#+TITLE: Plan de Edición

* Introducción

Este documento está hecho para gestionar la edición del libro del curso.

Considera una serie de pasos que nos harán transformar los documentos de cada
sesión que operan como guía a capítulos de libros.

Se considera el trabajo de realizar 3 tomos, uno por cada nivel.

* Estructura General

Cada /nivel/ contiene una serie de sesiones que tratan temáticas en particular
asociadas al nivel.

Cada /sesion/ contiene:

- resumen del capítulo
- contenidos a tratar
- ejercicios de ejemplo
- ejercicios a realizar
- tareas
- guía del profesor
- carpeta de imágenes
- carpeta de ejercicios ::
  - ejemplos
  - resueltos
  - planteados (resueltos y comentados en la página-no el libro)
- ejercicios resueltos con comentarios. 



* Migración

** Nivel 0

- Arrancar desde un pendrive LiveUSB (3000)
- La migración, cómo instalar GnuLinux
- Cómo instalar su propio sistema operativo y programas
- El paradigma de por qué existe
- Cómo usar emacs
- Org mode

** Nivel 1: GnuLinux, la terminal

Se compone de las siguientes sesiones a trabajar

| Capítulo                                      | Contenido  |
|                                               | <10>       |
|-----------------------------------------------+------------|
| Lógica Proposicional                          | l          |
| Teoría de Conjuntos                           | l          |
| Comandos básicos de la terminal               | c          |
| Elementos del Computador                      | c          |
| Control de Versiones con Git básico           | c          |
| Expresiones Regulares                         | c          |
| Strings                                       | c          |
| Datetime                                      | c          |
| Awk básico                                    | c          |
| Awk avanzado                                  | l          |
| Bash Avanzado                                 | c          |
| Scripts con argumentos                        | c          |
| Paralelización                                | l          |
| Gráficos con GnuPlot                          | c          |
| Aplicaciones con datos geográficos            | c          |
| Bases de Datos Relacionales SQL(introducción) | c          |
| Control de versiones con Git medio            | c          |

** Nivel 2: Python, dominando la serpiente

| Cappítulo                                                  | Contenido  |
|                                                            | <10>       |
|------------------------------------------------------------+------------|
| Ambiente virtual                                           | c          |
| Elementos de Python                                        | c          |
| Características especiales                                 | c          |
| Visualización de Datos                                     | c/l        |[*]
| Creación de Clases Básico                                  | c          |
| Creación de Clases Avanzado                                | l          |
| Comunicación entre procesos mediante socket                | c          |
| Iteradores                                                 | c          |
| Generadores                                                | c          |
| ORM: implementación de una estructura de tablas relacional | c          |
| Paralelización de Tareas                                   | l          |
| Entradas y salidas asíncronas                              | l          |

***  Ideas

Git, en grupo un ejercicio
[*] Mejorar explicación

** Nivel 3: Django, desarrollo web

| Capítulo                                  | Contenido  |
|                                           | <10>       |
|-------------------------------------------+------------|
| Introducción al HTML                      | c          |
| Introducción al CSS                       | c          |
| Introducción a Javascript                 | c          |
| Inicio de proyecto con Django             | c          |
| Primera aplicación con Django             | c          |
| Publicación en servidor                   | c          |
| Aplicación con Datos Dinámicos            | c          |
| Formulario de Contacto                    | c          |
| Formularios Compuestos                    | c          |
| Registro login grupos y niveles de acceso | c          |
| Aplicación con datos geográficos          | c          |
| Integración con SVG                       | l          |
| Javascript avanzado                       | c          |
| Integración con REST                      | l          |

* Participantes y roles

- David Pineda :: Director General de contenidos y editor
- Matias Meza :: Edición Técnica de Parte 1: GnuLinux
- Raquel Bracho :: Editora Técnica y Verificación de Contenidos
- Camilo Carrasco :: Edición Técnica de Parte 2: Python
- Alan Arelllano :: Director de Contenidos para Javascript
- Daniel Méndez :: Edición de Contenidos y Verificación Técnica
- Jose Castinneira :: Edición y Diagramación de Contenidos
- Alexandra Argüeles :: Edición y Diagramación de Contenidos
- Bernardita León :: Edición a Braille
- Seba Ghostman :: Edición técnica de contenidos
- Pablo Escuela :: Edición de Contenidos
- Caro da Silva :: Dirección Web

* Desarrollo 

Evaluar y trabajar lo siguiente:

Ilustraciones para cada libro y capítulo -> contactar un un ilustrador@ talentosx

- Un solo índice para cada tomo
- Un árbol de complejidad completo para cada tomo
- Un árbol de complejidad de todo el contenido (mapa afiche)
- Revisión de enlaces y referencias a libros -> a pie de página  (considerar que en textos impresos no se puede hacer click)
- Sección con glosario de conceptos a tratar -> Por tomo? Revisar esta ocion

[[file:./imagenes/etapa_edicion.png]]

Cada capítulo tiene una cantidad variable de páginas, por lo que el tiempo
estimado es que sea por cada 15 páginas, una semana de trabajo. De esta manera
si un capítulo contiene 30 páginas, serán dos semanas para Técnica, dos semanas
para Estilo, dos semanas para Diagramación. En total 6 semanas, cuando la
Técnica libera un capítulo, continúa con el siguiente mientras Estilo trabaja en
el anterior, y así sucesivamente.  

Estas etapas de trabajo, como propuesta, serán guiadas mediante el sistema de
control de versiones *git*, de la manera siguiente. 

[[file:./imagenes/esquema_git.png]]



