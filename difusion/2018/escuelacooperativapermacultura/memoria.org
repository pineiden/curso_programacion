#+TITLE: Memoria Proyecto Escuela Cooperativa Permacultura
#+AUTHOR: Ingeniero Civil Electricista David Pineda Osorio
#+EMAIL: dpineda@uchile.cl

* Descripcción de de obra

Procede:
Para Instalaciones clasificadas como locales de reunión de personas o
Instalaciones en ambientes explosivos, independiente de la potencia a
declarar.

En este apartado se indicará la finalidad de la instalación y su ubicación
geográfica. Se hará una descripción de su funcionamiento, destacando las
partes más importantes del proceso, indicando, además, los criterios de
diseño con el que fue elaborado el proyecto e informaciones previas del
proyecto.


* Cálculos Justificativos

Se deberán presentar en documentos denominados Memorias de Cálculo,
las justificaciones matemáticas de las soluciones para los diferentes
aspectos técnicos de las instalaciones, indicándose todos los factores
considerados en tales soluciones.

Los cálculos presentados en las Memorias de Cálculo se basarán en datos
fidedignos, en ellos deben incluir:
6.2.3.2.1 Análisis de cargas
6.2.3.2.2 Cálculos de intensidades de corrientes
6.2.3.2.3 Cálculos de alimentadores y conductores
6.2.3.2.4 Cálculos de caídas de tensión de alimentadores y circuitos finales
6.2.3.2.5 Análisis de distancias de seguridad.
6.2.3.2.6 Cálculos cortocircuito o certificado de niveles de cortocircuito
emitidos por la empresa eléctrica de distribución.
6.2.3.2.7 Para instalaciones cuya potencia declarada sea superior a 20kW
o para instalaciones conectadas a través de un empalme en
media tensión deberá incluir adicionalmente lo siguiente:
a)
b)
c)
6.2.4
6.2.5
Estudio de coordinación y selectividad de protecciones.
Calculo y diseño de sistema de puesta a tierra.
Cálculos de iluminación.




* Especificaciones Técnicas


Especificaciones Técnicas
6.2.4.1 Las especificaciones técnicas contendrán las características de
funcionamiento, designación de tipo, características de instalación,
dimensionales, constructivas y de materiales, además de toda otra
indicación que haga claramente identificable a los distintos componentes de
la instalación.
6.2.4.2 Las características y designaciones establecidas en 6.2.4.1 serán las fijadas
por los reglamentos y las normas técnicas nacionales correspondientes. En
ausencia de éstas, se aceptará la mención de normas extranjeras o, de otra
manera, la mención de alguna marca comercial incluyendo identificación de
tipo o número de catálogo, como referencia de características.
* Cubicación Materiales
6.2.5.1 En la cubicación de materiales se detallará en forma clara cada uno de los
equipos, materiales o accesorios que serán componentes de la instalación
terminada o que se ha utilizado en su montaje, indicando las cantidades
totales empleadas.
6.2.5.2 Cuando se utilicen estructuras o montajes normalizados, o en casos
similares, cuya cubicación de materiales es conocida, se podrá obviar la
cubicación en detalle de ellos haciendo referencia a la norma que los fija e
indicando sólo la cantidad global de estructuras, montajes u otros, utilizados
en el proyecto.
