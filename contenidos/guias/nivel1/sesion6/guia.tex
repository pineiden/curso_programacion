% Created 2019-11-25 lun 22:45
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\author{David Pineda}
\date{\today}
\title{Creación de Scripts con Opciones o Argumentos}
\hypersetup{
 pdfauthor={David Pineda},
 pdftitle={Creación de Scripts con Opciones o Argumentos},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 25.1.1 (Org mode 9.1.9)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Introducción}
\label{sec:org6a1325e}

Este capítulo considera que ya tienes la capacidad de utilizar
los principales comandos de \textbf{bash}, crear \textbf{scripts} y \textbf{funciones}.

Como ya hemos visto, muchos de los comandos permiten ser ejecutados 
con distintas opciones, estas opciones activan ciertas estructuras de control 
para ejecutar los bloques de código que correspondan.

Es decir, podemos crear un \textbf{script bash} que responda frente a
distintas entradas y obtener resultados satisfactorios.

Veremos entonces algunas formas de crear scripts que sean susceptibles
a las opciones que les ingresemos, a estas se le llaman argumentos 
o, depende el caso, \textbf{keywords}: llaves con argumentos.

\section{¿Cómo funciona?}
\label{sec:org791ace0}

Debemos fijarnos que existe una estructura general para establecer un comando
con sus opciones.

\subsection{Parámetros posicionales}
\label{sec:orgb08c117}

En primer lugar, el intérprete de \textbf{bash} recibe el comando con sus opciones 
de manera ordenada o posicional, a cada parte le asigna un índice partiendo
desde cero (\textbf{0}).

\begin{center}
\includegraphics[width=.9\linewidth]{./imagenes/script_arg_pos.png}
\end{center} 

Con esto podemos crear un script que nos muestre el \emph{texto} de cada argumento
posicional.

Recordando que, al ejecutar un script con sus opciones se entrega la acción
a \textbf{bash} para ejecutarlas.

\begin{minted}[]{bash}
#!/bin/bash

echo Posición 0 vale:
echo $0

echo Posición 1 vale:
echo $1

echo Posición 2 vale:
echo $2

echo Posición 3 vale:
echo $3
\end{minted}

Luego, si ejecutamos el siguiente comando.

\begin{minted}[]{bash}
bash mi_script_pos_args.sh hola mono feo
\end{minted}

Nos entrega lo siguiente.

\begin{minted}[]{bash}
Posición 0 vale:
mi_script_pos_args.sh
Posición 1 vale:
hola
Posición 2 vale:
mono
Posición 3 vale:
feo
\end{minted}

Con esto podemos desarrollar una primera intuición de como 
tomar las posiciones de un comando y definir ciertos parámetros
de nuestro programa.

\subsection{Un número indeterminado de parámetros}
\label{sec:orgd357872}

En caso de que no conozcamos la cantidad de parámetros de entrada que
el usuario utilizará cada vez, se puede usar el parámetro \textbf{\$@} y leer
cada elemento con un \textbf{for}.

\begin{minted}[]{bash}
posicion=1
for elemento in "$@"
do
    echo La posicion $posicion tiene valor:
    echo $elemento
    posicion=$[posicion+1]
done
\end{minted}

Lo que nos entrega todas las posiciones a partir del primer parámetro opcional.
Es decir, no incluye el nombre del archivo.

Con este script, la acción resultante sería.

\begin{minted}[]{bash}
# se ejecuta
bash mi_script_pos_args_@.sh hola mono feo
# el resultado es:
La posicion 1 tiene valor:
hola
La posicion 2 tiene valor:
mono
La posicion 3 tiene valor:
feo
\end{minted}

\subsection{Mostrar todo el comando ingresado}
\label{sec:org80b0065}

Para mostrar todo el comando ingresado se utiliza \textbf{\$*} como
parámetro, que resulta ser el string de toda la línea de parámetros
ingresados.

\begin{minted}[]{bash}
#!/bin/bash

echo Comando:
echo $*
\end{minted}

Tiene como resultado.

\begin{minted}[]{bash}
# comando:
bash mi_script_comando_ingresado.sh hola mono feo
# resultado:
Comando:
hola mono feo
\end{minted}

\subsection{Contar la cantidad de argumentos}
\label{sec:org92a16c9}

Para contar la cantidad de argumentos ingresados se puede
utilizar el parámetro \textbf{\$\#} que nos entrega la cantidad.

\begin{minted}[]{bash}
#!/bin/bash

echo Cantidad:
echo $#
\end{minted}

Y resulta

\begin{minted}[]{bash}
bash mi_script_cantidad_args.sh hola mono feo
Cantidad:
3
\end{minted}

\subsection{Una opción o argumento con espacio}
\label{sec:org1a7ddc1}

Para que el intérprete no lo cuente como un valor adicional, habrá
que marcar entre comillas el argumento que contenga espacios en
el \emph{string}.

\begin{minted}[]{bash}
bash mi_script_pos_args.sh 'hola mono' feo
# resultado:
Posición 0 vale:
mi_script_pos_args.sh
Posición 1 vale:
hola mono
Posición 2 vale:
feo
\end{minted}

O, también, para \$@

\begin{minted}[]{bash}
bash mi_script_pos_args_@.sh 'hola mono' feo
La posicion 1 tiene valor:
hola mono
La posicion 2 tiene valor:
feo
\end{minted}

\section{Argumentos con banderas -> uso de getopts}
\label{sec:org7ce190c}

Hemos visto que algunos comandos se accionan con opciones que
contienen el símbolo \textbf{menos} o \textbf{-} precedido por alguna letra.

Esta letra específica o bandera (\textbf{flag}), activará o desactivará
alguna acción del programa.

También es posible juntar varias letras a continuación del \textbf{-} y el sistema 
las leerá por serparado.

La funcionalidad que permite realizar el reconocimiento de estas \textbf{banderas} es
\textbf{getopts}, e investigaremos como se utiliza.

Recordemos el uso de \textbf{ls} que lista archivos y directorios en dónde
nos encontremos.

Si estoy en la carpeta de \textbf{CursoProgramacion}

\begin{minted}[]{bash}
ls
# entrega
imagenes  mis_datos.txt  nivel2  prologo.org
imgs.zip  nivel1	 nivel3  README.md
\end{minted}

En consecuencia, si queremos:

\begin{description}
\item[{a}] visualizar los archivos ocultos

\item[{F}] mostrar los directorios con "/"
\end{description}

Podemos realizar el comando utilizando las banderas \textbf{-aF}

\begin{minted}[]{bash}
ls -aFC
# entrega:
./   .git/	 imagenes/  mis_datos.txt  nivel2/  prologo.org
../  .gitignore  imgs.zip   nivel1/	   nivel3/  README.md
\end{minted}

Vemos la utilidad, ¿no? Entonces ahora vamos a desentrañar
los misterios de \textbf{getopts} para poder utilizarlo en nuestros scripts.

También, si necesitamos definir una opción o bandera con un argumento, es
posible hacerlo.

Por ejemplo si necesitamos comprimir una carpeta y el archivo comprimido tenga
un nombre ya definido.

\begin{itemize}
\item nombre archivo comprimido: nivel1.tar
\end{itemize}

\begin{minted}[]{bash}
tar -c -f nivel1.tar nivel1
\end{minted}

\subsection{Comando \emph{getopts}}
\label{sec:org9f415e7}

El comando \textbf{getopts} (con s como último carácter) considera que nosotros
declaramos una serie de letras que acepta como argumento.

\begin{center}
\includegraphics[width=.9\linewidth]{./imagenes/getopts_arg_pos.png}
\end{center}

Cada parámetro de entrada o argumento, entra en una iteración en la que se le
asigna el valor \textbf{OPTIND} por defecto. Contiene el número de opciones de la
entrada. Es necesario que esta variable vuelva a su valor original una vez
utilizada en la lectura de las opciones, con.

\begin{minted}[]{bash}
shift $(($OPTIND-1))
\end{minted}

Entre las condiciones expresadas por el manual de este comando, se utiliza
el carácter \textbf{":"} (dos puntos, en \emph{inglés} colon) para declarar que esa opción
o bandera específica trae un argumento adicional que debe ir separado por
espacio.

Cuando una opción tiene argumento, este es definido como la variable \textbf{OPTARG}
de la iteración correspondiente a la opción. Es decir '-a color', la variable
\textbf{OPTARG} tomara el valor \textbf{color}.

Para controlar cualquier error en el ingreso de las opciones, se debe colocar
el signo \textbf{:} \emph{antes de todas las letras}, de esta manera es posible controlar
cuando suceda y entregar un mensaje adecuado. De tal manera que, si hay error,
direcciona la opción al caso \textbf{?)}.

La construcción de \textbf{getopts} se realiza como el generador dentro de un iterador
como \texttt{while}, de la siguiente manera:

\begin{minted}[]{example}
getopts OPTS_STRING VARNAME
\end{minted}

Con el fin de que estás opciones se reflejen en nuestro script, deberemos usar
la estructura de control \textbf{while} y \textbf{case} combinadas.

En concreto, realizaremos un script de ejemplo que considere todas estas
variantes.

Recordar qué, en caso de error deberemos direccionar el mensaje a la
salida de error con:

\begin{itemize}
\item 1>\&2 \# Redirects stdout to stderr
\end{itemize}

\subsubsection{Seleccionar un color y número.}
\label{sec:orga48d74f}

Tenemos a disposición tres colores \{rojo, amarillo, azul\} y debemos
entregar un número que será la posición en una lista en la que irá el color.

Se debe guardar el valor de (número, color) en un archivo \textbf{fila\(_{\text{color.csv}}\)} si es que el
número no existe, si ya existe y es de distinto color retornar un mensaje que diga que esa posición ya
está tomada.

\begin{minted}[]{bash}
csv_file=fila_color.csv

color=""
numero=0

while getopts :ryan: opt
do
    case "${opt}"
    in
        r) color=rojo
           ;;
        y) color=amarillo
           ;;
        a) color=azul
           ;;
        n) numero=$OPTARG
           ;;
        \?) echo "Opcion Inválida: $OPTARG" 1>&2
            ;;
        :) echo "Opción Inválida: $OPTARG requiere un argumento" 1>&2
           ;;
    esac
done

shift $(($OPTIND-1))

# chequear que número es solo un entero
echo "Numero es -> $numero"

#$(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne 0

if [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    echo "Solo números enteros por favor" 1>&2
fi

if [[ "$color" =~ "" ]]
then
    echo "Color tiene que tener un valor" 1>&2
fi

if ! [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    # buscar valor en archivo
    existe=$(grep "^$numero;" $csv_file)
    entrada="$numero;$color"
    echo "Existe -> $existe"
    echo "To file-> $entrada"
    if [[ "$existe" == "" ]]
    then
        echo $entrada>>$csv_file
        echo "Se almacena combinación $entrada en archivo $csv_file"
    elif [[ "$existe" == "$entrada" ]]; then
        echo "Ya existe esa combinación" 1>&2
    else
        echo "Esa posición está tomada" 1>&2
    fi
fi
\end{minted}

Con esto, el comando se puede correr desde la terminal como:

\begin{minted}[]{bash}
bash fila_color.sh -r -n 56
bash fila_color.sh -y -n 2
\end{minted}

\subsection{Opciones largas o más descriptivas}
\label{sec:orga1740eb}

Lo primera diferencia a tener en cuenta es que existen opciones \emph{cortas} 
que son de la forma \textbf{-o}, y las opciones \emph{largas} que son de la forma
\textbf{--opcion}.

En los manuales de distintos comandos hemos visto que en muchos casos
las opciones posibles son \{-o, --opcion\} para realizar la misma acción.

Con esto en mente, para acercarnos a una mayor legibilidad a veces es mejor
usar las opciones \emph{largas}.

Para esto será necesario utilizar la combinación "-:" en la declaración. Así 
el intérprete ingresará en este caso y revisará las opciones \emph{largas}
utilizadas.

De esta manera, incluyendo opciones \emph{largas} ofreceremos una mayor flexibilidad.

\subsubsection{Seleccionar color y número con opciones largas}
\label{sec:orgd02f9f4}

Se modifica un poco el script anterior para considerar las opciones largas.

\begin{minted}[]{bash}
csv_file=fila_color.csv

color=""
numero=0

while getopts :ryan:-: opt
do
    case "${opt}"
    in
        -) case ${OPTARG} in
               red) color=rojo ;;
               yellow) color=amarillo;;
               blue) color=azul;;
               this_color=*) color=${OPTARG#*=};; 
           esac;;
        r) color=rojo
           ;;
        y) color=amarillo
           ;;
        a) color=azul
           ;;
        n) numero=$OPTARG
           ;;
        \?) echo "Opcion Inválida: $OPTARG" 1>&2
            ;;
        :) echo "Opción Inválida: $OPTARG requiere un argumento" 1>&2
           ;;
    esac
done

# chequear que número es solo un entero
echo "Numero es -> $numero"

#$(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne 0

if [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    echo "Solo números enteros por favor" 1>&2
fi

if [[ "$color" =~ "" ]]
then
    echo "Color tiene que tener un valor" 1>&2
fi

if ! [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    # buscar valor en archivo
    existe=$(grep "^$numero;" $csv_file)
    entrada="$numero;$color"
    echo "Existe -> $existe"
    echo "To file-> $entrada"
    if [[ "$existe" == "" ]]
    then
        echo $entrada>>$csv_file
        echo "Se almacena combinación $entrada en archivo $csv_file"
    elif [[ "$existe" == "$entrada" ]]; then
        echo "Ya existe esa combinación" 1>&2
    else
        echo "Esa posición está tomada" 1>&2
    fi
fi
\end{minted}

\section{Ejemplo: Descargar Libros de la Unam}
\label{sec:org74c0e6e}

Una de las Universidades más reconocidas de latinoamérica se encuentra en
México, esta es la UNAM.

Dentro de sus iniciativas bibliográficas está la digitalizar sus textos, los
cuales están disponibles para descarga abierta al público.

Realizaremos un script que, dada una \textbf{url} descarga todos los archivos \textbf{pdf} de
esa página.

Se realiza una descarga con \textbf{curl} y se hace una inspección, buscando patrones
para descartar aquello que no corresponda a lo buscado.

Puedes hacer la prueba con los libros disponibles en
\href{http://www.historicas.unam.mx/publicaciones/publicadigital/publicadigital.html}{Biblioteac UNAM}

\begin{minted}[]{bash}
while [[ $# -gt 1 ]]
do
key="$1"
case $key in
    -b|--book)
    PAGE_BOOK="$2"
    shift # past argument
    ;;
    --default)
    PAGE_BOOK="Nada"
    ;;
    *)
            # unknown option
    ;;
esac
done
shift $(($OPTIND-1))

echo "Descargando "$PAGE_BOOK

BOOK_PATH=$(echo $PAGE_BOOK|awk -F'/' '{OFS="/";$(NF--)=""; print}')
BOOK_NAME=$(echo $PAGE_BOOK|awk -F'/' '{OFS="/";$(NF--)=""; print $(NF-1)}')
echo $BOOK_PATH
mkdir $BOOK_NAME

curl $PAGE_BOOK | grep pdf| 
awk -F'<a href="' '{print $2}'|
awk -v path=$BOOK_PATH -F'" target="' '{print path$1}'| 
xargs wget -P $BOOK_NAME
\end{minted}

Observa el código, ¿Qué técnica se usa para obtener los argumentos?

¿Cómo sería el mismo script utilizando \textbf{getopts}? Escríbela.

\section{Uso de del parámetro \$@ con opciones o banderas}
\label{sec:orgb87f92e}

El parámetros \textbf{\$@} combinado con \textbf{for} nos permitirá recorrer toda la lista 
de elementos ingresados como argumentos.

En este caso podemos trabajar con opciones \textbf{cortas} o \textbf{largas}, pero esta vez
teniendo cuidado en la posición. Ya que si una posición tiene un valor
configurable habrá que especificar y rescatar esa posición.


\begin{itemize}
\item Creamos una lista a partir de \textbf{\$@}

\item Generamos un contador en \textbf{for}

\item Rescatamos valor correspondiente
\end{itemize}

Ejemplo en archivo \textbf{mi\(_{\text{script}}\)\(_{\text{for}}\)\(_{\text{args}}\)@.sh}

\begin{minted}[]{bash}
#!/bin/bash

args=("$@") 
count=1
for i in "$@"
do
    echo $i
    next=$(echo $count+1|bc)
    case $i in
        -l|--local)
            file_collect=local/run.py
            ;;
        -r|--remote)
            file_collect=remote/run.py
            ;;
        -p|--project)
            name=${args[$next]}
            echo "Pname:>"$name
            ;;
    esac

done
shift $(($OPTIND-1))


echo "El archivo escogido es:>${file_collect}"
echo "El nombre de proyecto es:>${name}"
\end{minted}

Y se usa de la manera siguiente:

\begin{minted}[]{bash}
bash mi_script_for_args\@.sh -l -p prueba_name
\end{minted}

\subsection{Ejemplo: Evitar un posible bug}
\label{sec:org0be24e2}

¿Qué pasa sin el valor dado asociado a un parámetro coincide con
alguna otra de las banderas?

\begin{itemize}
\item Deberíamos tener una bandera de control que nos permita omitir este caso
\end{itemize}

¿Qué tal el siguiente código?

\begin{minted}[]{bash}
#!/bin/bash

args=("$@") 
count=1
flag=0
for i in "$@"
do
    echo "Paso -> ${i}"
    next=$(echo $count+1|bc)
    case $flag in
        0)
          case $i in
              -l|--local)
                  file_collect=local/run.py
                  flag=0
                  ;;
              -r|--remote)
                  file_collect=remote/run.py
                  flag=0
                  ;;
              -p|--project)
                  name=${args[$next]}
                  echo "Pname:>"$name
                  flag=1
                  ;;
          esac
          ;;
        1)
          flag=0;;
        esac
done

echo "El archivo escogido es:>${file_collect}"
echo "El nombre de proyecto es:>${name}"
\end{minted}

\begin{itemize}
\item Tal vez sea posible saltar un espacio si entra a una opcion en que activemos
alguna acción de control de for, como \textbf{continue}.
\end{itemize}

\begin{minted}[]{bash}
#!/bin/bash
args=("$@") 
count=1
flag=0
for i in "$@"
do
    echo "Paso -> ${i}"
    next=$(echo $count+1|bc)
    if [[ "$flag" -eq "1" ]]
    then
        flag=0
        continue
    fi
    case $i in
        -l|--local)
            file_collect=local/run.py
            ;;
        -r|--remote)
            file_collect=remote/run.py
            ;;
        -p|--project)
            name=${args[$next]}
            echo "Pname:>"$name
            flag=1
            ;;
    esac
done

echo "El archivo escogido es:>${file_collect}"
echo "El nombre de proyecto es:>${name}"
\end{minted}

Tener en cuenta que \textbf{continue} opera de tal manera que, si entra en esa
iteración, se salta la ejecución del resto del código de ese bloque.

\section{Ejemplo: Uso de getopts para crear aplicación para Django}
\label{sec:org01ce36c}

Django es un framework de desarrollo web basado en el lenguaje \textbf{python},
crear una app requiere ciertos pasos rutinarios. Por lo que cuando necesitemos
crear alguna podríamos tener listo un script que realice lo que requerimos.

Para mayor comprensión de las acciones revisar el \textbf{nivel3} en los primeros
capítulos.

\begin{minted}[]{bash}
# OPTIONS >
# 
# -n :: app name, obligatory
# -t :: create templates folder {0,1}
# -s :: create static folder {0,1}
# -m :: create media folder {0,1}
# -u :: create urls.py file {0,1}
# -i :: define project name o main folder
#
# how to run?
#
# ./startapp.sh -n NAME -t 1 -s 0 -i mi_web
#

project_name=$(pwd|awk -F'/' '{print $NF}')
templates=1
static=1
media=0
urls=1
DEBUG=1

while getopts n:t:s:m:u:i:d: option
do
case "${option}"
in
  n) app=${OPTARG};;
  t) templates=${OPTARG};;
  s) static=${OPTARG};;
  m) media=${OPTARG};;
  u) urls=${OPTARG};;
  i) project_name=${OPTARG};;
  d) DEBUG=${OPTARG};
esac
done


echo "DEBUG FLAG : <${DEBUG}>"
echo "Creando apps en ${project_name}"
install_file="./${project_name}/settings/installed.py"

echo "Creating <"$app"> folder"

if [ $DEBUG -eq 0 ]
then
    mkdir ./apps/$app
fi

echo "Creating app inside the folder <./apps/"$app">"

if [ $DEBUG -eq 0 ]
then
    python manage.py startapp $app ./apps/$app
fi


echo "Template value <${templates}>"
if [ $templates -eq 1 ]
then
    echo "Creating templates"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/templates
        mkdir ./apps/$app/templates/$app
    fi
fi

echo "Static creation value <${static}>"
if [ $static -eq 1 ]
then
    echo "Creating static folder"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/static
        mkdir ./apps/$app/static/$app
    fi
fi

echo "Media creation value <${media}>"
if [ $media -eq 1 ];
then
    echo "Creating media folder"
    if [ $DEBUG -eq 0 ]
    then
        mkdir ./apps/$app/media
    fi
fi


echo "URLS creation value <${urls}>"
if [ $urls -eq 1 ];
then
    echo "Creating urls file"
    if [ $DEBUG -eq 0 ]
    then
        touch ./apps/$app/urls.py
    fi
fi

echo "Agregando app a INSTALLED_APPS -> apps.${app}"


if [ $DEBUG -eq 0 ]
then
    sed -i "/]/i\    \"apps."$app"\"," $install_file
fi

\end{minted}

\section{Referencias}
\label{sec:orgb5a7c3d}

\begin{description}
\item[{Manual getopts}] \url{https://ss64.com/bash/getopts.html}

\item[{Uso getopts}] \url{https://sookocheff.com/post/bash/parsing-bash-script-arguments-with-shopts/}

\item[{Comparativa getopt vs getopts}] \url{http://abhipandey.com/2016/03/getopt-vs-getopts/}

\item[{Uso de for y continue}] \url{https://www.tldp.org/LDP/abs/html/loopcontrol.html}

\item[{Manual getopts}] \url{https://wiki.bash-hackers.org/howto/getopts\_tutorial}
\end{description}
\end{document}