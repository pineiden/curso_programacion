#!/bin/bash

args=("$@") 
count=1
flag=0
for i in "$@"
do
    echo "Paso -> ${i}"
    next=$(echo $count+1|bc)
    case $flag in
        0)
          case $i in
              -l|--local)
                  file_collect=local/run.py
                  flag=0
                  ;;
              -r|--remote)
                  file_collect=remote/run.py
                  flag=0
                  ;;
              -p|--project)
                  name=${args[$next]}
                  echo "Pname:>"$name
                  flag=1
                  ;;
          esac
          ;;
        1)
          flag=0;;
        esac
done

echo "El archivo escogido es:>${file_collect}"
echo "El nombre de proyecto es:>${name}"
