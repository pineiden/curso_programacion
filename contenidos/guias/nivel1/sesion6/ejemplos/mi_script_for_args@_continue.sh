#!/bin/bash

args=("$@") 
count=1
flag=0
for i in "$@"
do
    echo "Paso -> ${i}"
    next=$(echo $count+1|bc)
    if [[ "$flag" -eq "1" ]]
    then
        flag=0
        continue
    fi
    case $i in
        -l|--local)
            file_collect=local/run.py
            ;;
        -r|--remote)
            file_collect=remote/run.py
            ;;
        -p|--project)
            name=${args[$next]}
            echo "Pname:>"$name
            flag=1
            ;;
    esac
done

echo "El archivo escogido es:>${file_collect}"
echo "El nombre de proyecto es:>${name}"
