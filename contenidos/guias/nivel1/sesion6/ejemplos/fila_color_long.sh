#!/bin/bash

csv_file=fila_color.csv

color=""
numero=0

while getopts :ryan:-: opt
do
    case "${opt}"
    in
        -) case ${OPTARG} in
               red) color=rojo ;;
               yellow) color=amarillo;;
               blue) color=azul;;
               this_color=*) color=${OPTARG#*=};; 
           esac;;
        r) color=rojo
           ;;
        y) color=amarillo
           ;;
        a) color=azul
           ;;
        n) numero=$OPTARG
           ;;
        \?) echo "Opcion Inválida: $OPTARG" 1>&2
            ;;
        :) echo "Opción Inválida: $OPTARG requiere un argumento" 1>&2
           ;;
    esac
done

# chequear que número es solo un entero
echo "Numero es -> $numero"

#$(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne 0

if [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    echo "Solo números enteros por favor" 1>&2
fi

if [[ "$color" =~ "" ]]
then
    echo "Color tiene que tener un valor" 1>&2
fi

if ! [[ $(echo "$numero" | grep -qE '^[0-9]+$'; echo $?) -ne "0" ]];then
    # buscar valor en archivo
    existe=$(grep "^$numero;" $csv_file)
    entrada="$numero;$color"
    echo "Existe -> $existe"
    echo "To file-> $entrada"
    if [[ "$existe" == "" ]]
    then
        echo $entrada>>$csv_file
        echo "Se almacena combinación $entrada en archivo $csv_file"
    elif [[ "$existe" == "$entrada" ]]; then
        echo "Ya existe esa combinación" 1>&2
    else
        echo "Esa posición está tomada" 1>&2
    fi
fi
