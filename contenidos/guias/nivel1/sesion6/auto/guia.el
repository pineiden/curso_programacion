(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org6a1325e"
    "sec:org791ace0"
    "sec:orgb08c117"
    "sec:orgd357872"
    "sec:org80b0065"
    "sec:org92a16c9"
    "sec:org1a7ddc1"
    "sec:org7ce190c"
    "sec:org9f415e7"
    "sec:orga48d74f"
    "sec:orga1740eb"
    "sec:orgd02f9f4"
    "sec:org74c0e6e"
    "sec:orgb87f92e"
    "sec:org0be24e2"
    "sec:org01ce36c"
    "sec:orgb5a7c3d"))
 :latex)

