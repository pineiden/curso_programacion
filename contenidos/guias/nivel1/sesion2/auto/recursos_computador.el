(TeX-add-style-hook
 "recursos_computador"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org1502ea0"
    "sec:orgf13965e"
    "fig:org4764400"
    "sec:org4ae4eaf"
    "fig:orgdcc6d8e"
    "sec:org49c8252"
    "fig:org7e1d073"
    "sec:org77d7eed"
    "fig:org2357583"
    "sec:org7319b95"
    "sec:org2718cc0"
    "sec:orgdb920ca"
    "sec:org6bbaefb"
    "sec:org8848c4d"
    "sec:orgb7b8527"
    "sec:org7373b46"
    "sec:org81beba7"
    "sec:org3723e59"
    "sec:org5ffbf6d"
    "sec:orgb0c7d25"))
 :latex)

