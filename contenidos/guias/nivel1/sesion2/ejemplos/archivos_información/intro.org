* Una lectura de /El Capital/

Esta serie de textos son apuntes tomados en torno a la lectura del renombrado
texto de economía, filosofía y política de Karl Marx. En ello trataré de
enlazar y clarificar, desde mi perspectiva, las ideas que en el libro se 
desarrollan.

A tener en cuenta que ya va casi un siglo y medio desde la publicación, período
en el cual ha sucedido mucho, la humanidad se ha desangrado en dos grandes guerras
y otras más pequeñas. No ha habido momento de paz. Países como el nuestro ha 
sufrido una dolora dictadura con consecuencias terribles que aún se viven.

Comprender las distintas perspectivas y escalas de como vivimos en el mundo ha
pasado a ser un aspecto de importancia en mi desarrollo como persona,  haciendo
lo posible por formarme en las materias que me vayan construyendo como un ser
humano pleno.

Hay ciertas ideas en este libro que se desprenden como poderosas herramientas
de trabajo para comprender que existimos no solo como individuos sino como
un colectivo que responde a ciertas características particulares, también
que existen conflictos entre unos y otros. 

De los antecedentes que he logrado recolectar de manera muy orgánica, tanto
por propia experiencia como por el trabajo en la organización social, es que
existen ciertos grupos de humanos que han explotado sistemáticamente a otros.

Ahora bien, siendo consciente de tal situación, que existimos como individuos
y somos dentro de uno de estos colectivos es que se hace necesario buscar 
las herramientas y trabajar para mejorar las condiciones de vida de los nuestros.

Pero el problema no es tan simple, las interacciones, la biodiversidad, la historia,
la cultura, la economía, las formas de organizarse, son algunos factores a considerar
en el problema, por lo que debemos declararlo muy complejo.

¿Nos ayudará entonces el estudio de este libro? Creo que sí, por la calidad del 
texto, su contenido e intencionalidad, además del peso de su propia historia. 
Dará pie, al menos, para comprender un poco más el problema y, tal vez, aportar
en el proceso de vivir con los otros.
 
La editorial que publica esta edición en castellano es Akal, numeros 48 a 55
de la colección /Básica de Bolsillo/. Este texto ha sido escrito previamente 
a comenzar la lectura del libro. Se compone de 8 tomos y un folleto de presentación. Este último es 
un ensayo introductorio que contextualiza y da luz sobre el libro publicado, 
llamado /El capital a casi siglo y medio de distancia, de Enrique Palazuelos/.

Sin duda hay un sin número de detalles sobre mi persona, que debería ahondar. Tal 
vez una de las motivaciones más profundas que tengo para comenzar este libro
es porque forma parte de un eje central del trabajo intelectual para transformar
el mundo a favor de las clases explotadas y desposeídas, encontrar los primeros
hilos de tales elementos conforma un reto intelectual que me dispongo a realizar.



** Disgresiones transgresoras

He pensado que lo anteriormente escrito llega a ser excesivamente somero en 
lo que describe sobre las líneas motivacionales que me llevan a leer este libro.
Tal vez dar rienda suelta a las palabras sería un buen ejercicio de preparación
para conocer tanto el origen como el destino del objeto de estudio.

Comenzaré hablando sobre mi persona. Estoy a punto de cumplir los 33 años, casado 
recientemente. Trabajando en el centro de sismología, que es donde monitorean
la actividad sísmica de mi país. 

Crecí en el seno de una familia en la cual mis padres, cuando tenía unos seis años,
se transformaron en creyentes de una secta protestante. Pese a que era pequeño
me percataba de la tamaña insensatez de las doctrinas que se imparten en esa
religión. Sin embargo,lograron entregarme un educación de calidad dentro de los
estándares de mi país aunque nada motivante.

Tengo una característica que aprecio mucho de mi mismo, es la capacidad de observación
y reflexión sobre los que estudio. Disfruto enormemente de la naturaleza y experimentar
en ella. Así como también abstraerme en la lectura de distinta clase de textos. 
Mi mente crea lo que lee y lo hace vivir, lo mezcla con todo lo que ya hay y pone
en marcha las fuerzas vitales que me motivan a vivir o sobrevivir en donde esté.

Siento que, para estar feliz, necesito siempre una buena compañia. Han sido
dolorosas las épocas en que he sentido el abandono de mi familia o de supuestos
amigos. Cada vez mas he aprendido a andar con cautela, a compartir mis ideas
y sentimientos de una forma más selecta.

¿Por qué es tan necesario el éxito? Ha sido una pregunta constante. Muchos
se validan frente a los otros bajo la fachada de ser una persona exitosa. 
Sin embargo, no considero que sea importante, considero que la felicidad
es de mayor relevancia que el éxito. Que las mínimas cosas deben ser suficientes
para tener una vida satisfactoria. 

Una búsqueda constante en mis reflexiones es poder encontrar los  recursos mínimos
para realizar una proyecto o idea, la elegancia está en la sencillez. Valoro más
que algo se realice como un ejercicio autonomo que bajo la venia de un poderoso. 
¿Por qué? Porque se vive más, da una mayor experiencia del vivir y convivir.


 
