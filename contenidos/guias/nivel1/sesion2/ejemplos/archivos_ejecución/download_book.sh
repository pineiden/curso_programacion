while [[ $# -gt 1 ]]
do
key="$1"
case $key in
    -b|--book)
    PAGE_BOOK="$2"
    shift # past argument
    ;;
    --default)
    PAGE_BOOK="Nada"
    ;;
    *)
            # unknown option
    ;;
esac
done

echo "Descargando "$PAGE_BOOK

BOOK_PATH=$(echo $PAGE_BOOK|awk -F'/' '{OFS="/";$(NF--)=""; print}')
BOOK_NAME=$(echo $PAGE_BOOK|awk -F'/' '{OFS="/";$(NF--)=""; print $(NF-1)}')
echo $BOOK_PATH
mkdir $BOOK_NAME

curl $PAGE_BOOK | grep pdf| awk -F'<a href="' '{print $2}'|awk -v path=$BOOK_PATH -F'" target="' '{print path$1}'| xargs wget -P $BOOK_NAME
