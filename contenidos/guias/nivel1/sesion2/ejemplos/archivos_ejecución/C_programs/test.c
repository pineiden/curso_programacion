#include <stdio.h>
#include <math.h>
#include <complex.h>

int main()
{
float t = 0.3;
float s = 5.6;
printf("Flotante %.2f %.2f \n",t,s);
__complex__ double z = 2.0 +3.5*I;
printf("La real %f, parte imaginaria %f \n", crealf (z), cimagf (z));
__complex__ float z2 = 5 +7i;
printf("La real %f, parte imaginaria %f \n", __real__ z2, __imag__ z2);
enum nombres {primero, segundo, tercero, cuarto} ;
enum nombres mi_nombre = primero;
printf("Nombre %d\n",mi_nombre);
if (mi_nombre == primero) {
	printf("%s","David");
}
return 0;
}


