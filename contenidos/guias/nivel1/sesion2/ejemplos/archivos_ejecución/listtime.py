from datetime import date, datetime, timedelta

def get_time_list(bday, duration):
    t0=bday.start_time
    t1=bday.end_time
    tl0=bday.lunch_start
    tl1=bday.lunch_end
    timelist=[]
    launch_limit_0=(datetime.combine(date.today(), tl0) - duration).time()
    launch_limit_1=tl1
    nxt=t0
    while nxt<=t1:
        print(nxt)
        if nxt<=launch_limit_0 or nxt>=launch_limit_1:
            print("Adding", nxt)
            yield nxt
        
        nxt=(datetime.combine(date.today(), nxt) + duration).time()



"""
Get a list of dates from today to last appointment+1 day
"""

def get_list_days(today, last_appointment_day, avalaible_days_list):
    nxt=today
    last_day=(last_appointment_day+timedelta(days=1)).date()
    while nxt<=last_day:
        #check if today is in av day list
        weekday=nxt.strftime("%w")
        if weekday in avalaible_days_list:
            yield nxt
        nxt=(nxt+timedelta(days=1)).date()


"""
So, for every day on day_list, check avalaible appointment for this doctor and agenda
"""
day_list=get_list_days(today, last_appointment_day, avalaible_days_list)
for this_day in day_list:
# for this day, obtain the get_time_list
    # get today bday
    bday=bdays.filter(day=weekday).latest('id')
    time_list=get_time_list(bday, duration)
    for this_time in time_list:
        # for every hour, check if there are an appointment
        # compose: Date+Time
        appointment_date_time=datetime.combine(this_date, this_time)
        # the field is named 'date' and is DateTime
        result_appointment=self.appointments.filter(date=appointment_date_time)
        if not result_appointment:
            return appointment_date_time
    # if there are a free space, and the time is after now, show
    # return this_date, next_time
