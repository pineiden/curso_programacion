#+TITLE: Guía de Sesión 1
#+SUBTITLE: Conjuntos, Lógica y GIT
#+AUTHOR: Daniel Méndez, David Pineda
#+LANGUAGE:es


* Tareas

** TODO ::

- ejemplos de cada operación

- ejercicio de git con poemas y cuentos

- revisar faltas ortográficas, sintaxis

* Teoría de conjuntos

La teoría de conjuntos, que no es más que el estudio de los conjuntos, se considera dentro de las bases que construyen la 
matemática. Desarrollada por el matemático alemán /Georg Cantor/[fn:1], quien definió tres axiomas
 
-proposiciones, afirmaciones o negaciones; que no se pueden demostrar, definir su valor de verdad; y se aceptan
 como verdaderas

-, los supuestos sobre los que se basa la teoría. 

Partiendo de a idea de que podremos "encapsular" muchos elementos y tratarlos como si fuera una sola entidad 
compuesta por los elementos, los axiomas son los siguientes:

1.- Un conjunto es una reunión de objetos que cumplen con cierta propiedad (llamados los elementos de ese conjunto) y que, por tanto, queda definido por tal propiedad.

2.- Un conjunto es una sola entidad matemática, de modo que puede a su vez ser contenido por otro conjunto.

3.- Dos conjuntos que tengan los mismos elementos son iguales. Así, puede decirse que un conjunto está determinado por sus elementos.

#+CAPTION: Conjunto A (verde): vocales del abecedario latino. Conjunto B (naranja): enteros positivos menores o iguales a 6.
#+NAME: fig:conjunto
[[./conjunto.png]]

En la figura [[fig:conjunto]] podemos ver una representación gráfica de los conjuntos. Otra forma de representarlo es usando la notación
matemática $A = \{ a, e, i, o, u\}$ y $B = \{1, 2, 3, 4, 5, 6\}$.
 
Para entendernos, y como regla general, se identifican los conjuntos a través de letras mayúsculas y los elementos de un conjunto con letras minúsculas.

Ahora que hemos definido qué es un conjunto, pasaremos a detallar algunas operaciones básicas que se pueden realizar entre ellos.

** Universo
Nuestro universo será el mundo en el que habitan los elementos que están dentro de un conjunto. Por lo general se denota 
con la letra $U$.

Del ejemplo anterior, el universo del conjunto B

** Pertenencia
Se dice que un elemento $e$ cualquiera pertenece a un conjunto $C$ si y si solo si el conjunto $C$ contiene a $e$.

Lo denotaremos de la siguiente forma $e \in C$. La negación de esta relación sería $e \notin C$.


** Inclusión
Se dice que un conjunto $S$ está incluido en $C$ si y solo si, todos los elementos de $S$ pertenecen también a $C$.

Lo denotaremos $S \subsetq C$. Se lee $S$ subconjunto de $C$. La negación sería $S \nsubsetq C$.

#+CAPTION: El conjunto A (verde) está incluido en el conjunto B (naranja).
#+NAME: fig:inclu
[[./inclusion.png]]

** Unión
Las operaciones anteriores daban como resultado un valor de verdad, es decir verdadero o falso. Ahora veremos algunas 
operaciones en que el resultado de ellas es otro conjunto.

Sean $A$ y $B$ dos conjuntos cualesquiera. La unión de $A$ y $B$, $A \cup B$, es el conjunto de todos los elementos
que pertenencen a $A$ o a $B$.

#+CAPTION: Conjuntos A y B.
#+NAME: fig:forunion
[[./forunion.png]]

#+CAPTION: Conjunto Unión.
#+NAME: fig:union
[[./union.png]]

** Intersección
Sean $A$ y $B$ dos conjuntos cualesquiera. Se define la intersección de estos, $A \cap B$, como el conjunto de 
elementos que pertenecen a $A$ y a $B$.

#+CAPTION: Conjunto intersección.
#+NAME: fig:intersección
[[./inter.png]]

** Diferencia

Nuevamente, sean $A$ y $B$ dos conjuntos arbitrarios. Definimos al conjunto diferencia, $A \setminus B$ que se lee $A$ menos $B$,
como el conjunto todos los elementos que pertenencen a $A$ y no pertenencen a $B$.

Pregunta: ¿$A \setminus B = B \setminus A$?

#+CAPTION: Conjunto diferencia.
#+NAME: fig:dif
[[./dif.png]]

** Complemento

Sea $A$ un conjunto cualquiera, dentro de un universo $U$, de dice complemento, $A^C$, a los elementos que no 
pertenencen a $A$.

Evalúe la veracidad de la siguientes proposiciones: $A \cup A^C = U$, $A \cap A^C = \emptyset$.

  
** Algebra de conjuntos
Vamos a ver ahora algunas propiedades útiles para hacer operaciones con conjuntos. La idea de estos ejercicios es que vamos 
aplicando los conceptos que aprendimos recién para obtener el resultado de las siguientes operaciones.

*** $(A^C)^C = A$
*** $A \cap U = A$
*** $A \cup U = U$
*** $(A \cup B) \cup C = A \cup (B \cup C)$
*** $(A \cap B) \cap C = A \cap (B \cap C)$
*** $A \cup B = B \cup A$
*** $A \cap B = B \cap A$
*** $(A \cup B)^C = ?$
*** $(A \cap B)^C = ?$

** Introducción concepto de objeto.



* Lógica proposicional

En computación, al basar la representación de la información en bit {0,1} y combinaciones de estos, se usa necesariamente
en toda la estructura de sus sistemas la lógica preposicional. Es decir combinaciones y operaciones de /Verdadero/ y /Falso/
que, también pueden representarse como /1/ y /0/.

Si lo vemos desde la teoría de conjuntos, a un conjunto que tiene elementos se le asigna /1/, a un conjunto vacío (que no
tiene elementos se le asigna un /0/.

Cada preposición o sentencia que tiene un valor lógico {0,1} puede ser operado con otra sentencia. Veremos entonces los operadores
usuales.

* Operadores Lógicos

** Negación {no, ~, -,¬}

Básicamente es lo contrario al valor de la sentencia. Ejemplos: 

1.- Si a=1, no a=0

2.- Si a=Verdadero, no a=Falso

3.- Si a=está lloviendo, no a=no está lloviendo

+-----+----+
|p    |¬p  |
+-----+----+
|V    |F   |
+-----+----+
|F    |V   |
+-----+----+


** Conjunción { y, "*", &, intersección}

Es la operación que permite comparar ocurrencias similares dentro de cada sentencia. Si hay ocurrencia 
el resultado es /1/, si no hay ocurrencia el resultado es /0/. Ejemplos:

1.- Está lloviendo /y/ está nublado

2.- El platano tiene cáscara amarilla /y/ El color de la piel de la pera es amarilla

3.- El agua es de todos /y/ las chilenas y los chilenos tenemos derecho al agua

+-----+-----+-----+
|p    |q    |p y q|
+-----+-----+-----+
|V    |V    |V    |
+-----+-----+-----+
|V    |F    |F    |
+-----+-----+-----+
|F    |V    |F    |
+-----+-----+-----+
|F    |F    |F    |
+-----+-----+-----+


** Disyunción { O, or, ||}

Es la operación que revisa la ocurrencia de una u la otra, en que no es necesario que una ocurra en la otra
y viceversa. Ejemplos:

1.- Está lloviendo /o/ está soleado

2.- Hace frío /o/ hace calor

3.- La manzana es dulce /o/ La manzana está verde

+-----+-----+--------+
|p    |q    |p or q  |
+-----+-----+--------+
|V    |V    |V       |
+-----+-----+--------+
|V    |F    |V       |
+-----+-----+--------+
|F    |F    |F       |
+-----+-----+--------+
|F    |V    |V       |
+-----+-----+--------+


** Disyunción exclusiva { o exclusivo, xor}

Es la operación que revisa la ocurrencia de algo en una sentencia mientras no ocurra en la otra sentencia, y viceversa. Ejemplos

1.- Hace frío XOR hace calor

3.- El disco está lleno XOR el disco está vacío

4.- Tengo hambre XOR estoy satisfecho


+-----+-----+--------+
|p    |q    |p xor q |
+-----+-----+--------+
|V    |V    |F       |
+-----+-----+--------+
|V    |F    |V       |
+-----+-----+--------+
|F    |F    |F       |
+-----+-----+--------+
|F    |V    |V       |
+-----+-----+--------+

** Condicion

Es la operación que revisa la ocurrencia de una sentencia o una operación de sentencias y actúa en consecuencia 
de estas. Ejemplos:

1.- /Si/ Hace frio /entonces/ nos abrigamos

2.- /Si/ Juan tiene hambre *o* Daniel tiene hambra /entonces/ hacemos el almuerzo

3.- /Si/ Se usa la lógica preposicional en computación /entonces/ es necesario comprenderla para aprender a programar.

En programación esta operación lógica se observa masivamente en el uso de las estructuras de control {if, while, switch case, etc}

+-----+-----+---------+
|p    |q     | p => q |
+-----+-----+---------+
|V    |F     |F       |
+-----+-----+---------+
|V    |V     |V       |
+-----+-----+---------+
|F    |F     |V       |
+-----+-----+---------+
|F    |V     |V       |
+-----+-----+---------+


** Condicional con múltiples casos { si {A,B,C o D} entonces {Ra, Rb, Rc, Rd}}

Es la operación condicional que revisa la ocurrencia en algunos de los casos disponibles. Si se tienen los
casos {A, B, C o D} entonces se actuará en consecuencia con resultados {Ra, Rb, Rc, Rd}. Ejemplos.

1.- /Si/ la palabra que me das está en las siguientes palabras :{hola, chao} /entonces/ te saludo como {bienvenida, despedida}

2.- /Si/ el número que obtendremos es {bajo -2, superior a 2} /entonces/ la ecuación tendrá pendiente {negativa, positiva}


** Bicondicionalidad o equivalencia { <=>, ==, eq}

Es la operación que verifica que las sentencias operadas tengan equivalencia en el dominio lógico del plano en que se comparan.

1.- La cáscara del plátano es amarilla /==/ La piel de la pera es amarilla [Color de la fruta]

2.- Un modelo de la tierra [geoide]  /==/ geoide

3.- Es sabor de la sandía [dulce] /==/ dulce

4.- Los humanos nunca mueren /==/ la luna es más grande que el sol

+-----+-----+---------+
|p    |q    |p==q     |
+-----+-----+---------+
|V    |V    |V        |
+-----+-----+---------+
|V    |F    |F        |
+-----+-----+---------+
|F    |V    |F        |
+-----+-----+---------+
|F    |F    |V        |
+-----+-----+---------+


* Leyes de Morgan

Son las leyes que permiten combinar operaciones lógicas entre sentencias.

- La negación de la conjunción es la disyunción de las negaciones.
- La negación de la disyunción es la conjunción de las negaciones.

Es decir.

/no/ (A /y/ B) <=> (/no/ A) /o/ (/no/ B)

/no/ (A /o/ B) <=> (/no/ A) /y/ (/no/ B)


* Ejercicios

1.- En grupos de a 2, realizar 1 ejemplo de cada caso.

2.- Convertir a lógica preposicional las siguientes frases:


#+BEGIN_SRC txt
No vi la película, pero leí la novela = ~p ^q
Ni vi la película ni leí la novela = ~p ^ ~q
no es cierto que viese la película y leyese la novela = ~ (p ^ q)
vi la película aunque no leí la novela = p ^ ~q
no me gustra transnochar ni madrigar = ~p ^ ~q


p = pablo atiende en clase
q = pablo estudia en casa
r = pablo fracasa en los exámenes
s = pablo es aplaudido

#+END_SRC

* Referencias

Wikipedia https://es.wikipedia.org/wiki/L%C3%B3gica_proposicional

Monografías http://www.monografias.com/trabajos-pdf5/logica-proposicional-ejercicios-resueltos

Ejercicios http://www.unipamplona.edu.co/unipamplona/portalIG/home_74/recursos/programacion-estructurada/06092016/ejerciciosresueltos.pdf

Ejemplos https://www.cs.buap.mx/~iolmos/propeLogica/1_LP1.pdf


* Desrrollo de proyectos con GIT

** Versionamiento
Una preocupación importante cuando estamos trabajando con proyectos que involucran gran cantidad de archivos, es
llevar control de las versiones que voy obteniendo de mi software.

Por lo general, al organizar el desarrollo de un software, se dividen las funcionalidades que 
definen mi proyecto. El objetivo de esto es ir construyendo etapas de desarrollo, las que voy cumpliendo una a una. Cada
vez que una de estas últimas es alcanzada, se termina una versión de mi software. Esto también se utiliza cuando corrigos errores (bugs)
en el comportamiento del sistema.

Para llevar control de cada etapa usamos una herramientoe de versionamiento, que nos permitirá respaldar cada 
cambio que efectuemos en los archivos involucrados.

Existen varias de estas herramientas. Para el objetivo del curso usaremos */GIT/*[fn:2], que es una herramienta Open
Source para llevar control de las versiones.
** Iniciar un proyecto

Antes de poder iniciar un proyecto, necesitamos crear una cuenta en algun sitio que nos de acceso a un servidor remoto
donde respaldar nuestra información. Le confiaremos esta tarea a /GitLab/[fn:3], que es otro proyecto Open Source 
desarrollado por Dmitriy Zaporozhets y Valery Sizov y los aporte del equipo de desarrollo.


1. Primero instalar Git revisando https://git-scm.com/download/linux.
2. Luego entrar a https://gitlab.com, registrar una cuenta y crear nuevo proyecto.
3. Seguir las instrucciones que entrega GitLab.

** Instrucciones básicas
Antes de revisar en detalle cada uno de los comandos, es preciso comprender que nuestro proyecto exisitirá en dos computadores
a la vez. Llamaremos repositorio local a los archivos que manejo en mi computador y repositorio remoto al respaldo de 
los archivos en el servidor que nos facilita GitLab.
*** CLONE
Ese comando nos permite clonar un repositorio remoto existente. Es decir, copiarlo exactamente como está en nuestro
equipo.
#+BEGIN_SRC bash
git clone [url_repositorio]
#+END_SRC
*** ADD
Este comando nos permite agregar al índice, los archivos que serán versionados. Nos permite agregar modificaciones, creación
o eliminación de un archivo o directorio.
#+BEGIN_SRC bash
git add .
git add [nombre_archivo]
#+END_SRC
*** COMMIT
Esta instrucción graba en el repositorio local los cambios efectuados, los que deben haber sido debidamente 
agregados anteriormente. Agregaremos siempre un mensaje que describa los cambios recien hechos.
#+BEGIN_SRC bash
git commit -m [mensaje]
#+END_SRC
*** PUSH
Para actualizar el repositorio remoto con los cambios que he cometido (commit), uso
#+BEGIN_SRC bash
git push origin master
#+END_SRC
*** PULL
Analogo al anterior, si quiero descargar lo que hay en mi remoto al local, en vez de ``pushear'', ``pulleo''.
*** STATUS
Este comando es uno que vale la pena realmente recordar. Cuando estemos perdidos porque no sabemos en que estado
está nuestro repositorio, este comando nos mostrará la información que necesitamos y una guía de como seguir. Es 
el perfecto ayuda memoria.
#+BEGIN_SRC bash
git status
#+END_SRC

** Branches
En ocasiones, nuestro proyecto puede tener muchos colaboradores, o quizás más de una persona necesita hacer cambios
sobre el mismo grupo de archivos que otra, creando diferentes funcionalidades. Para mantener la información de manera
consistente y sin sobre escribir datos, usamos las ramas.

Una rama nos permite crear un desvío a partir de un punto en la línea original de desarrollo, arreglar un problema o 
implementar una función, para luego volver a mezclar con la rama original. 

#+BEGIN_SRC bash
git branch
#+END_SRC

Este comando desplegará una lista con las diferentes ramas de nuestro repositorio.
*** CHECKOUT
Este comando nos permite cambiarnos de branch, en caso que exista, o crear una cuando aún no está creada.
#+BEGIN_SRC bash
git checkout [nombre_rama]
git checkout -b [nombre_rama]
#+END_SRC
*** MERGE
Cuando llega el momento de volver a mezclar las ramas, usamos ``merge''. Este comando mezclará automáticamente 
las ramas que no produzcan conflicto, es decir, cuando no se esté intentando cambiar código que está escrito
en la rama original. En caso que ocurra la sobre escritura en algún archivo, autómaticamente se hará un registro
de ambas modificaciones para que se decida cual es la que debe permanecer.

#+BEGIN_SRC bash
git merge [rama_mezclada]
#+END_SRC
* Footnotes

[fn:1] Pueden leer más historia en Wikipedia: https://es.wikipedia.org/wiki/Axiomas_de_Zermelo-Fraenkel
[fn:2] Sitio oficial de GIT: https://git-scm.com/about
[fn:3] Sitio oficial de GitLab: https://about.gitlab.com/about/

* TODO Ejemplos

** Los humanos


Ricos / Pobres
Colonizadores / Colonizados
Explotados / Explotadores
Pueblo Originario / Europeo
Hombre/Mujer/LGBT
