(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org8e7aecd"
    "sec:org3ab7c5b"
    "fig:orga982732"
    "sec:org04b9858"
    "sec:orgd740289"
    "sec:orgd67929d"
    "fig:orgfbce9cd"
    "sec:org4b5b9ad"
    "fig:org69493de"
    "fig:orgfc79b66"
    "sec:orgefedb04"
    "fig:org5bf3333"
    "sec:orga305efe"
    "fig:org5e27bc7"
    "sec:orga905bf8"
    "sec:orgb38497e"
    "sec:org1bf4f9c"
    "sec:orgfde5fe9"
    "sec:org4fa94f2"
    "sec:orgb667fe0"
    "sec:org021bcf6"
    "sec:org5abad1a"
    "sec:org27e8cee"
    "sec:org61e535c"
    "sec:org06f7cb4"
    "sec:orgc465374"
    "sec:org501f48c"
    "sec:orgee81c32"
    "sec:org8fec3c1"
    "sec:org63c3d46"
    "sec:orgf79cd56"
    "sec:orgf8f92f4"
    "sec:orgf21fa7c"
    "sec:org285c278"
    "sec:org1173b27"
    "sec:orgfb1defc"
    "sec:org3f88860"
    "sec:org4ceeb20"
    "sec:org5afbb72"
    "sec:orgf052ab2"
    "sec:org5d89eec"
    "sec:org7caf33e"
    "sec:orgd3e26f6"
    "sec:orgb2851a4"
    "sec:orgfc016c4"
    "sec:org685aa2e"
    "sec:orgada7713"
    "sec:org581cd24"
    "sec:org842e146"
    "sec:orgd839990"
    "sec:orgeff564a"))
 :latex)

