#include <stdio.h>
#include <math.h>
#include <complex.h>

//Clases Especificadores de almacenaje : auto, extern, register and static

//auto: variables locales en funcion
//register: casi como auto, si la variable es muy usada, no se puede usar operador de direccion, solo sizeof
//static, lo opuesto a funcion, se retiene el valor inclusoo fuera de la definicion
//va con return usualmente



int main()
{

    struct line {
        int length;
        char contents[0];
    };
    //punteros, en flotante
    float valor = 6.5;
    float * puntero = & valor;

    printf("Valor puntero %p\n", (void *) puntero);
    printf("Valor direccion a puntero %p\n", (void *) &puntero);
    printf("Direccion a valor %p\n", (void *) &valor);
    
    union numbers {
        int i;
        float f;
    };
    
    union numbers foo = {4};
    union numbers *number_ptr = &foo;
    
    number_ptr -> i = 450;
    
    printf("%d \n", foo.i);
    
    __complex__ int x = 5 + 17i;
    printf ("Mult compleja %f \n", creal(x * ~x));
    
    return 0;    
}
