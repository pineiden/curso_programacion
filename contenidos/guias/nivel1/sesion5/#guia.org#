#+TITLE: BASH avanzado: herramientas útiles 
#+AUTHOR: David Pineda, Daniel Méndez
#+EMAIL: dpineda@uchile.cl - daniel.mendezf@mail.udp.cl

* Introducción

Hemos visto ya varias herramientas que nos permiten sentar una base para el manejo de 
datos y el uso de la terminal. Esta base que incluye desde los conceptos de lógica y 
conjuntos vistos al comienzo, hasta el desarrollo de un programa utilizando AWK como 
lenguaje deben ser internalizadas y entendidas a cabalidad para poder llevarlas a la
práctica. Pero ojo, no necesito saber usar un crochet para entender que sirve para hacer
bufandas. Para dominar el uso de la herramienta es necesaria la práctica, para saber 
cuando usarla es preciso entender su función.

En esta sesión veremos algunos conceptos que nos ayudarán a comprender mejor las 
herramientas que hemos presentado y terminaremos el primer nivel del curso con 
conceptos que introducirán los próximos niveles.

* Funciones

Uno de los conceptos vistos durante el curso, y que además es la base del modelamiento,
es el concepto de función. Existen muchas definiciones, podemos decir que es una relación
entre dos conjuntos, si queremos hablar en conceptos de teoría de conjuntos. Podemos
decir también que una función es una transformación de algún elemento. En palabras 
simples, una función es como una caja negra a la que yo le ingreso algún elemento y 
arroja uno de salida, que puede ser el mismo u otro diferente.

#+CAPTION: Ejemplo de funciones
#+NAME: fig:func
[[./funcion3.png]]
[[./funcion1.png]]
[[./funcion4.png]]
[[./funcion2.png]]

Si analizamos matemáticamente una función, encontraremos algunos elementos que la 
componene. El dominio de la función es el conjunto de los elementos que le doy como
entrada a la función. También es llamado conjunto pre-imagen o conjunto de entrada. Son
todos los valores aceptados por nuestra función. Análogamente tenemos el recorrido, o 
conjunto de llegada. Es la agrupación de los posibles valores que puede entregar nuestra
función. Se le llama también conjunto imagen o de salida.

En programación, las funciones son útiles cuando debemos aplicar la misma serie de
instrucciones repetidas veces en diferentes entornos o contextos. Por ejemplo, luego de 
instalar linux en un computador, es recomendable realizar una serie de tareas para 
mejorar la experiencia del usuario. Estas tareas puedes ser a elección del usuario que
instala, en este caso realizaremos las siguientes:

1. Actualizar el sistema: ~sudo apt-get update && sudp apt-get upgrade~
2. Instalar Gnome Tweak Tool: ~sudo apt-get install gnome-tweak-tool~
3. Instalar programas para (des)comprimir:  ~sudo apt-get install p7zip-full p7zip-rar rar unrar~
4. Agregar algunos aliases a nuestro sistema: ~cd ~ && echo "alias ll='ls -l -sort'">>.bashrc && echo "alias ..='cd ..'">>.bashrc~

** Ejercicio

Crear en un script de bash una función que ejecute las tareas recién mencionadas.

** Para ejercitar
 Revise en la carpeta de la sesión el archivo =encontrar.sh=. Identifique lo que hace
y como lo hace. No tiene que escribir nada, es un código hecho por Pedro Ruiz Hidalgo
que usted debe entender como funciona. Si tiene alguna duda de un comando, preguntar por
el grupo. Mejor aún si busca la solución en internet y la comparte por telegram.

En ocasiones necesitamos construir funciones que reciban un parametro de entrada. Muy 
similar a =AWK=, la sintaxis para usar argumentos en una función es la siguiente.

#+NAME: param
#+BEGIN_SRC bash
#!/bin/bash

function parametros(){
  echo $0 $1;
};
parametros primer_param;
#+END_SRC

* Recuerdo de  Expresiones aritméticas
#+NAME: exp_ari
#+BEGIN_SRC bash
$((expression))
$[expression]
$(( n1+n2 ))
$[ n1+n2 ]
$(( n1/n2 ))
$[ n1/n2 ]
$(( n1-n2 ))
$[ n1-n2 ]
#+END_SRC

#+NAME: ejem
#+CAPTION: Ejemplo de expresiones aritméticas
#+BEGIN_SRC bash
#!/bin/bash
x=5
y=10
ans=$[ x + y ]
echo "$x + $y = $ans"
#+END_SRC

Las expresiones aritméticas que se pueden usar en bash las pueden encontrar [[https://bash.cyberciti.biz/guide/Perform_arithmetic_operations][aquí]].
* Arreglos, arrays o listas

Este es otro concepto que hemos mencionado varias veces durante el transcurso de este
nivel. Como hemos dicho también, existen varios tipos de variables y los arreglos son 
uno más dentro de aquellos.

Recordando, una variable es el espacio en memoria (RAM) necesario para guardar un dato. Hacemos
referencia al dato guardado a través del nombre de la palabra, el cual es definido por 
quien programa. Dentro de los tipos de datos que hemos visto, recordamos =char=, =strings=,
=int= para los enteros, =float= para los reales, =bool= para verdadero o falso y hay más.

El arreglo o lista, será una agrupación de variables. Idealmente del mismo tipo, pero no
en todos los lenguajes esto es un requisito. De todas formas, es una buena práctica 
hacer arreglos que contengan datos del mismo tipo. A bajo nivel, un arreglo es el
espacio en memoria para guardar una cantidad n de datos de algún tipo, donde n es la 
cantidad de elementos que tendrá nuestro arreglo. Básicamente, son como una vaina.

#+attr_html: :width 300px
[[./vaina.jpg]]
#+attr_html: :width 300px
[[./vainaint.jpg]]
#+attr_html: :width 300px
[[./vainvaina.jpg]]

Ahora, como podemos crear +vainas+ arreglos en =BASH=. La sintaxis es la siguiente:

#+NAME: arreglos
#+BEGIN_SRC bash
arr=(Hola Mundo)
echo ${arr[0]}
echo ${arr[1]}
#+END_SRC

Los corchetes son necesarios para evitar conflictos con rutas a archivos o directorios. 
Sumado a estas instrucciones, tenemos las siguientes que nos permiten manejar el arreglo.

#+NAME: masarray
#+BEGIN_SRC bash
${arr[*]}         # Todos los items del arreglo
${!arr[*]}        # Todos los indices del arreglo
${#arr[*]}        # Cantidad de arvejas, osea, elementos del arreglo
${#arr[0]}        # Largo del primer elemento del arreglo
#+END_SRC

Veamos un ejemplo más complejo, pueden encontrar este script dentro de la carpeta 
de ejemplos de la sesión como =ejemplo_uno.sh=.

#+NAME: arraycom
#+BEGIN_SRC bash
#!/bin/bash

array=(one two three four [5]=five)

echo "Array size: ${#array[*]}"

echo "Array items:"
for item in ${array[*]}
do
    printf "   %s\n" $item
done

echo "Array indexes:"
for index in ${!array[*]}
do
    printf "   %d\n" $index
done

echo "Array items and indexes:"
for index in ${!array[*]}
do
    printf "%4d: %s\n" $index ${array[$index]}
done
#+END_SRC

Para poder manejar string, sin que produzca errores con las comillas, usamos =@=, tal 
como si fuera el =*= en el ejemplo anterior. Veamos el comportamiento en otro script.
También lo pueden encontrar en la carpeta ejemplos (=ejemplo_dos.sh=).
#+NAME: arraystring
#+BEGIN_SRC bash
#!/bin/bash

array=("first item" "second item" "third" "item")

echo "Number of items in original array: ${#array[*]}"
for ix in ${!array[*]}
do
    printf "   %s\n" "${array[$ix]}"
done
echo

arr=(${array[*]})
echo "After unquoted expansion: ${#arr[*]}"
for ix in ${!arr[*]}
do
    printf "   %s\n" "${arr[$ix]}"
done
echo

arr=("${array[*]}")
echo "After * quoted expansion: ${#arr[*]}"
for ix in ${!arr[*]}
do
    printf "   %s\n" "${arr[$ix]}"
done
echo

arr=("${array[@]}")
echo "After @ quoted expansion: ${#arr[*]}"
for ix in ${!arr[*]}
do
    printf "   %s\n" "${arr[$ix]}"
done
#+END_SRC

Los ejemplos fueron sacados de [[https://www.linuxjournal.com/content/bash-arrays][aquí]].

* Recursión
Antes de repasar un poco las estructuras de control, es recomendable mencionar este concepto.
Si bien su aplicación para la resolución de ejercicios requiere de mayor práctica y tiempo
de maduración, es importante al menos revisarlo para tenerlo presente.

#+attr_html: :width 300px
[[./google-recursion.jpg]]

La recursión nace por la necesidad de resolver problemas de gran complejidad, pero que se
pueden dividi en pequeñas versiones de sí mismo. Por lo tanto, si hacemos una función que
resuelva el problema original, podremos usar la misma función para resolver los pequeños
problemas que lo solucionan.

Esto se ve reflejado en el código cuando en la definición de una función, hago uso de la misma.

#+attr_html: :width 800px
[[./recursion_flow.png]]

Veamos un par de ejemplos para comprender mejor el concepto.

** Problema 1
Existe una operación matemática llamada Factorial, que se identifica con el signo =!=. Es
decir, el factorial de 3 o =3!=, se calcula multiplicando =1 x 2 x 3=. El factorial de 5
sería entonces ~5! = 1 x 2 x 3 x 4 x 5 = 120~. De manera general, podemos decir que 
~n! = 1 x 2 x ... x n~. o lo que equivale a decir que el factorial de n es el resultado
de la multiplicación de 1 hasta n (Al hecho de multiplicar números consecutivos se le llama
pitatoria o productorio, que viene del número Pi. Más info [[https://es.wikipedia.org/wiki/Productorio][aca]].)

Pregunta para la reflexión. ¿Cuánto es =0!=?

A continuación la función que soluciona este problema de manera recursiva.

#+NAME: fact
#+BEGIN_SRC bash
#!/bin/bash

factorial()
{
    if [[ $1 -eq 0 ]]
    then
        echo 1
    else
        last=$(factorial $[$1-1])
        echo $(($1 * last))
    fi
}
factorial 5
#+END_SRC

** Problema 2
Veamos un ejemplo sin matemática. Necesitamos escribir una función en =BASH= que reciba 
un string y retorne la misma palabra pero en orden inverso.


* Repaso de estructuras de Control
Estas instrucciones son las que nos permiten modificar el flujo de ejecución en 
las instrucciones del programa.

** Bifurcación o condición

#+CAPTION: Diagrama de flujo de una bifurcaciÃ³n
#+NAME: fig:bif
[[./condicion.jpg]]

#+NAME: ifthen
#+BEGIN_SRC bash
#!/bin/bash

echo 'Ingresa un 1 o un 2'
read A

if [ $A = 1 ];
then
printf 'Ingresaste un uno'
else
printf 'Ingresaste un dos'
fi

exit
#+END_SRC

** Ciclos
#+CAPTION: Diagrama de flujo de un ciclo
#+NAME: fig:ciclo
[[./ciclo.png]]

#+NAME: ciclo
#+BEGIN_SRC bash
#!/bin/bash
limite=5
i=0;

while [ $limite -gt $i ]
do
     echo -e "AcciÃ³n $i ejecutada.\n"
     let i=$i+1
done
#+END_
