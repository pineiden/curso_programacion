(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "latin1") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org9a9e0b8"
    "sec:orgc3d7e8a"
    "fig:org9a985ca"
    "sec:org416d074"
    "sec:org543b201"
    "sec:org918eea4"
    "org9d6f382"
    "sec:org61ed1da"
    "sec:orgf059708"
    "sec:orgd44cf26"
    "sec:org0bc3e33"
    "sec:org60dd6f8"
    "sec:orgcccc4de"
    "fig:org9c543c1"
    "sec:org5315336"
    "sec:org975f15d"
    "sec:org44fd70d"
    "sec:orgd1b1695"
    "sec:orgd05471c"
    "tab:org5accbed"
    "sec:org53909bc"
    "tab:orgf7137e9"
    "sec:org7b3c6d3"
    "tab:org6464256"
    "sec:org38085d0"
    "fig:org5444abb"
    "sec:org77ab60e"
    "sec:org39d5114"
    "sec:org620e5a0"
    "sec:org371c42f"
    "sec:org34bfa66"
    "sec:org0f96b66"
    "sec:org87484f6"))
 :latex)

