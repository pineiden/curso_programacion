#! /bin/bash

# Función suma a+b
suma(){
	resultado=$(echo $1+$2|bc -l)
	echo $resultado
}

# Función resta a-b
resta(){
	resultado=$(echo $1-$2|bc -l)
	echo $resultado
}

# Función divide a/b
divide(){
	resultado=$(echo "$1/$2"|bc -l)
	echo $resultado
}

# Función multiplica

multiplica(){
	resultado=$(echo $1*$2|bc -l)
	echo $resultado
}

# Función Exponencial

exponencial(){
	resultado=$(echo $1^$2|bc -l)
	echo $resultado
}
