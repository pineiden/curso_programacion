#! /bin/bash

factorial(){
	if [[ $1 -eq 0 ]]
	then
		echo 1
	else
		ultimo=$(factorial $[$1-1])
		echo $(($1*ultimo))
	fi

}
