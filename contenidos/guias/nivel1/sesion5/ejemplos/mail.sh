#!/bin/bash

email="daniel.mendezf@gmail.com"

if [[ "$email" =~ ^([A-Za-z]+[A-Za-z0-9]*((\.|\-|\_)?[A-Za-z]+[A-Za-z0-9]*){1,})@(([A-Za-z]+[A-Za-z0-9]*)+((\.|\-|\_)?([A-Za-z]+[A-Za-z0-9]*)+){1,})+\.([A-Za-z]{2,})+ ]]
then
    echo "La direccion $email es valida."
else
    echo "la direccion $email es invalida."
fi
