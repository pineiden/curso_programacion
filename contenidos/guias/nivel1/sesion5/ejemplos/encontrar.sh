#!/usr/bin/env bash
 
# ~/.bin/encontrar
# encuentra archivos a partir de la descripci�n de su nombre en un directorio espec�fico
#
# Por Pedro Ruiz Hidalgo
# version 1.0.0
# Copyright � enero 2017
#
#
 
EXIT_OK=0
EXIT_BAD=66
 
PATRON=$1
DIRECTORIO=$2
 
autor()
{
 echo -e "\nPedro Ruiz Hidalgo @petrorum. Copyright � 2017\n"
}
 
ayuda()
{
 echo -e "\nencontrar [PATRON] [DIRECTORIO]\n"
} 
 
noparams()
{
 echo -e "\nSon necesarios dos par�metros\nencontrar -h para ayuda\n"
 read -p "�Quieres ver la ayuda? (S|s)" -n 1 -r
 if [[ $REPLY =~ ^[Ss]$ ]];
    then
       echo ""
       ayuda
 fi
}
 
nodir()
{
 echo -e "\nDirectorio no Existe\n"
}

if [[ $PATRON == "-h" ]];
then 
 ayuda
 exit $EXIT_OK
fi
 
if [[ $PATRON == "-a" ]];
then 
 autor
 exit $EXIT_OK
fi
 
if [ $# -lt 2 ];
then
 noparams
else
 if [ -d $DIRECTORIO ];
 then
 echo ""
 find $DIRECTORIO -name $PATRON*
 echo ""
 exit $?
 else 
 nodir 
 exit EXIT_BAD
 fi
fi
