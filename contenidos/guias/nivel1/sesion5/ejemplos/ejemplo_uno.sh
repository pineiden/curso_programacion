#!/bin/bash

arreglo=(uno dos tres cuatro [5]=cinco)

echo "Tamaño de arreglo: ${#arreglo[*]}"

echo "Ítems en arreglo:"
for item in ${arreglo[*]}
do
    printf "   %s\n" $item
done

echo "Índices en arreglo:"
for index in ${!arreglo[*]}
do
    printf "   %d\n" $index
done

echo "Índices y valores en arreglo:"
for index in ${!arreglo[*]}
do
    printf "%4d: %s\n" $index ${arreglo[$index]}
done
