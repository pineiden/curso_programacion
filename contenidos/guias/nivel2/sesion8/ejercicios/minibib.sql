%%Create a tablespace
CREATE SCHEMA  IF NOT EXISTS libros AUTHORIZATION escritor;
\dt libros.*
#DEfault:
SET search_path = libros;

CREATE TABLE IF NOT EXISTS libros.libro(
    id bigserial PRIMARY KEY ,
    nombre varchar(50),
    autor varchar(50),
    serie varchar(100) UNIQUE)
WITH (OIDS = TRUE);


CREATE TABLE IF NOT EXISTS libros.usuario(
    id bigserial PRIMARY KEY ,
    nombre varchar(50),
    fono varchar(50),
    rut varchar(100) UNIQUE,
    email varchar(100))
WITH (OIDS = TRUE);

CREATE TABLE IF NOT EXISTS libros.prestamo(
    id bigserial PRIMARY KEY,
    id_user integer references libros.usuario (id) ON UPDATE CASCADE ON DELETE CASCADE,
    id_libro integer references libros.libro (id) ON UPDATE CASCADE ON DELETE CASCADE,
    fecha timestamp default now())
WITH (OIDS = TRUE);
