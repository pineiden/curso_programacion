import asyncio
from corrutinas import holacoro


# Obtener el loop asyncio
loop = asyncio.get_event_loop()

loop.run_until_complete(holacoro())

loop.close()
