from datetime import datetime
import asyncio
import random

async def ahora_fecha():
    return datetime.utcnow()

async def imprimir():
    for i in range(3):
        ahora = await ahora_fecha()
        print("La fecha es %s" %ahora)
        num = random.randrange(0,10)      
        await asyncio.sleep(num)


loop = asyncio.get_event_loop()

loop.run_until_complete(imprimir())

loop.close()
