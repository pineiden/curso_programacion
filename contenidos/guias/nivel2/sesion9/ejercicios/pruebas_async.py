import asyncio
from corrutinas import holacoro


# Obtener el loop asyncio
loop = asyncio.get_event_loop()

# Ejecutar la corrutina en el loop
loop.run_until_complete(holacoro())

# Cerrar el loop
loop.close()
