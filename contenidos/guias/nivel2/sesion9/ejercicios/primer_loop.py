import asyncio

async def holacoro():
    print("Hola %d" % 1)
    await asyncio.sleep(1)
    
def renew(*args):
    task = asyncio.ensure_future(holacoro())
    task.add_done_callback(renew)


task=asyncio.ensure_future(holacoro())
task.add_done_callback(renew)

loop=asyncio.get_event_loop()

try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')
