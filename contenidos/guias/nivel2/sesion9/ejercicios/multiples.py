# 2 corrutinas llamadas de manera independiente 'a la vez'
import asyncio
import random

async def holacoro():
	for i in range(3):
		num = random.randrange(0,10)      
		await asyncio.sleep(num)
		print("Hola %d" % i)

async def chaocoro():
	for i in range(3):
		num = random.randrange(0,10)      
		await asyncio.sleep(num)
		print("Chao %d" % i)

if __name__ == "__main__":
	tasks=[
	asyncio.ensure_future(holacoro()),
	asyncio.ensure_future(chaocoro())
	]
	loop = asyncio.get_event_loop()
	#creamos tarea y la asociamos al loop, ejecutandola
	loop.run_until_complete(
		asyncio.gather(*tasks)	
	)
