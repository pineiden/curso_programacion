import asyncio
from corrutinas import holacoro


# Obtener el loop asyncio
loop = asyncio.get_event_loop()

# Quiero ejecutar holacoro
# Debo crear una tarea
# Asignarla al "trabajador(procesador)"
# Encargado del loop

# Reviso si la funcion es corrutina

task = None
if asyncio.iscoroutinefunction(holacoro):
    print("holacoro es corrutina, se crea tarea")
    # se crea la corrutina
    print("¿Qué es la función holacoro?")
    print(holacoro)
    coro = holacoro()
    # se agenda para el loop
    print("¿Qué es coro?")
    print(coro)
    task = asyncio.ensure_future(coro)
else:
    print("holacoro no es corrutina, no se asigna")

if task:
    loop.run_until_complete(task)

loop.close()
