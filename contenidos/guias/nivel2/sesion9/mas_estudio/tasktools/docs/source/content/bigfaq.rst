What is an async function?
==========================

An **async function** or **coroutine** is an special kind of function which allows
you to work with *events* that happend without your control but you need to access
to some of the information generated on that.

For example, if you realize a query to a server you can't control how much time last
the work of the computer searching the data and returning to you, so you
have to *await* for the data. But, what can you do with this *awaited time*. In the classic
paradigm you have to wait for the returning data, but with the asynchronous paradigm
you can do something else.

With that in mind you can use this tools to message exchange in any case, socket
communications (to databases, other servers, etc) and any system with multiple events
happening at same time.

In **Python** the library **async** provides an implementation for that purpose, and you
have to consider some new concepts and stages to enable the execution of this coroutines.


The classic way
---------------

The classic way or *synchronous programming* way, you have a number of actions that
the computer runs one after other, you can't attend other events.

.. image:: img/lineal.png

In python the GIL(Global Interpreter Locking) take charge of that and blocks any other
action while the code is running.

The asynchronous way
--------------------

With that programming style we can say the we are *near* reality, on our programs we
depends from things that can or can't happend without our control, but we can give some confidence
to some systems or the systems that we know how works (in general), and then give or send, and get or
receive data from them.

.. image:: img/async_progm.png

So, when we detect that something is ready to proceed, we continue with that. The order whem that
happen doesn't matter.

Better explained:

.. image:: img/eventloop.png

Recommended Knowledge
---------------------

The DavidBeaz_ site offer you a wide variety of examples, videos and material about the most interesting
subjects on python and computer science. Put in clear a lot of concepts of complex things like the *asyncio*
module.

The WillysCave_ site explains the main differences betweeon *classic* and *asynchronous* programming ways.

The MichaelKuty_ presentation to explain the evolution of this subjects.

The DavidPineda_, yes me, in *español*  with different and useful examples

The Modules_ consider an interesting list of different uses of asyncio library.

.. _DavidBeaz: http://www.dabeaz.com/

.. _WillysCave: https://blogs.msdn.microsoft.com/willy-peter_schaub/2012/06/27/willys-cave-dwelling-notes-5-c-and-asynchronous-programming/

.. _MichaelKuty: http://michaelkuty.github.io/vertx-gdg/

.. _DavidPineda: https://gitlab.com/pineiden/async_coro

.. _Modules: https://github.com/python/asyncio/wiki/ThirdParty

How to Create an Asyncio Coroutine
----------------------------------

Look at this definition:

 
.. code-block:: python
   :linenos:

    import asyncio

    async def holacoro():
      for i in range(3):
        await asyncio.sleep(1)
        print("Hola %d" % i)


#. Import the module
#. Create a new function with async
#. Use the **wait**


How use the coroutine in a simple way
-------------------------------------

Look at this code:

.. code-block:: python

  loop = asyncio.get_event_loop()
  #creamos tarea y la asociamos al loop, ejecutandola
  loop.run_until_complete(holacoro())


The **loop** is an object that manage the exuction of the coroutines


Wait a moment, how it's work?
-----------------------------

Well, step by step:

- Define the coroutine

- Create a Future Task in what te coroutine vill be run

.. code-block:: python

   future_task = holacoro()


- Collect other different tasks. Other coroutines will be run.

.. code-block:: python

   future_task_2 = othercoro()

- Create a group, set or list

.. code-block:: python

   task_list = [future_task, future_task_2]

- Gather the tasks

The tasks are the arguments from the unpacked list of pack to create
the gather object.

.. code-block:: python

   tasks_gathered = asyncio.gather(*tasks)


- Finally, run the gathered tasks.

.. code-block:: python

   loop.run_until_complete(tasks_gathered)


Do you want more?
-----------------

Bored, but check the PEP's

#. PEP 492 https://www.python.org/dev/peps/pep-0492/

#. PEP 3156 https://www.python.org/dev/peps/pep-3156/

#. PEP 3153 https://www.python.org/dev/peps/pep-3153/

#. PEP 525 https://www.python.org/dev/peps/pep-0525/

#. PEP 492 https://www.python.org/dev/peps/pep-0492/

#. PEP 530 https://www.python.org/dev/peps/pep-0530/


Main features
-------------

#. The **eventloop** with many different implementations, the standar and others (like quamash for qt)

#. Abstractions for layers Transport and Protocol, (OSI model?)

#. Support for networking uses: TCP, UDP, UnixSocket, SSL, subprocesses, pipes, etc.

#. Sintaxis based on (async, await) pair.

#. Manage Futures and coroutines

#. Interface to work with threads and multiprocessing


Consider this objects when work with coroutines
-----------------------------------------------

#. Eventloop: the loop manager

#. Coroutines: the special function

#. Futures, Tasks: when you say you will run some coroutine

#. Streams: comunication elements

#. Subprecesses: activate different workes

#. Queues: to share data among processes



