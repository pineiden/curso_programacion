.. Async Task Tools documentation master file, created by
   sphinx-quickstart on Thu Aug 16 15:45:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Async Task Tools's documentation!
============================================

The Tasktools module allows you to work with the asyncio_ library from the programming
language *Python* on the versions above the *3.5*.

The features which this module provides is the ability to create *async functions* that runs
in an **async while** in first case. The problem is when you have a lot of Task to run
at *same time* (a lot or many) you have to order your mind and design a *system* or *machine*
using more resources.

To do that, you have to activate multiple processes and create the tasks for every active process. In
that stage the **TaskScheduler** and **TaskAssignator** are useful. Like its names says are
specific machines to *schedule* tasks on the procesess and activate with the *assignator*.

Hence, when you have a system that needs to run permanently, check new data, convert and send
to other place is convenient to study this module and implement your functions and classes
using the tools from this module.

¡Enjoy!

.. _asyncio: https://docs.python.org/3/library/asyncio.html

.. toctree::
   :caption: Table of Contents:
   :maxdepth: 2

   content/bigfaq
   content/install
   content/async_while
   content/scheduler
   content/assignator
   content/codedoc
   content/todo



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
