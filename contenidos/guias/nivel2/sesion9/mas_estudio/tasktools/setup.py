from setuptools import setup

setup(name='tasktools',
      version='0.7.0',
      description='Some useful tools for asycnio Tasks',
      url='https://tasktools.readthedocs.io/en/latest/',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='MIT',
      packages=['tasktools'],
      zip_safe=False)
