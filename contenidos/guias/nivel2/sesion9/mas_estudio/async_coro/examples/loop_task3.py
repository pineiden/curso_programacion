import asyncio
import functools

async def holacoro(v):
    print("Hola %d" % v)
    await asyncio.sleep(1)
    return v+1
    
async def sumacoro(*args):
    c=sum(args)
    print("La suma es %d" %c)
    await asyncio.sleep(3)
    return c
    
#special for every coroutine
async def coromask(coro, args):
     result=await coro(*args)
     return [result]

#special for every coroutine
async def coromask_suma(coro, args):
     _in=[args[-1]]
     result=await coro(*args)
     _in.append(result)
     return _in
    
def renew(task, mask, coro, *args):
    result=task.result()
    task=loop.create_task(mask(coro,result))
    task.add_done_callback(functools.partial(renew, task, mask, coro))

loop=asyncio.get_event_loop()

args=[1]
task1=loop.create_task(coromask(holacoro,args))
task1.add_done_callback(functools.partial(renew, task1, coromask, holacoro))

args2=[1,2]
task2=loop.create_task(coromask_suma(sumacoro,args2))
task2.add_done_callback(functools.partial(renew, task2, coromask_suma, sumacoro))


try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')

