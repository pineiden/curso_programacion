# 2 corrutinas llamadas secuencialmente
import asyncio

async def holacoro():
	for i in range(3):
		await asyncio.sleep(1)
		print("Hola %d" % i)

async def chaocoro():
	for i in range(3):
		await asyncio.sleep(2)
		print("Chao %d" % i)

async def doscoro():
	await holacoro()
	await chaocoro()


if __name__ == "__main__":
	loop = asyncio.get_event_loop()
	#creamos tarea y la asociamos al loop, ejecutandola
	loop.run_until_complete(doscoro())
