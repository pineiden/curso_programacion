# idea:
# Create an engine instance with local socket.
import asyncio
import sys

import functools
from taskloop import coromask, renew, simple_fargs

from termcolor import colored, cprint
import concurrent.futures
import functools


def gprint(text):
    msg = colored(text, 'green', attrs=['reverse', 'blink'])
    print(msg)

def bprint(text):
    msg = colored(text, 'blue', attrs=['reverse', 'blink'])
    print(msg)

def rprint(text):
    msg = colored(text, 'red', attrs=['reverse', 'blink'])
    print(msg)

import multiprocessing

def hola_fargs(_in, obtained):
    return [obtained]

async def holacoro(v):
    print("Hola %d" % v)
    v+=1
    await asyncio.sleep(2)
    return v

def hola_task(v):
    loop=asyncio.get_event_loop()
    args=[v]
    bprint("Task %s" % 1)
    try:
        task=loop.create_task(
            coromask(
                holacoro,
                args,
                hola_fargs)
        )
        task.add_done_callback(
            functools.partial(
                renew,
                task,
                holacoro,
                hola_fargs)
        )
    except Exception as exec:
        print(exec)
    if not loop.is_running():
        loop.run_forever()

def chao_fargs(_in, obtained):
    return [obtained]

async def chaocoro(b):
    print("Chao %d "  % b)
    b+=1
    await asyncio.sleep(3)
    return b

def chao_task(v):
    loop=asyncio.get_event_loop()
    args=[v]
    bprint("Task %s" % 1)
    try:
        task=loop.create_task(
            coromask(
                chaocoro,
                args,
                chao_fargs)
        )
        task.add_done_callback(
            functools.partial(
                renew,
                task,
                chaocoro,
                chao_fargs)
        )
    except Exception as exec:
        print(exec)
    if not loop.is_running():
        loop.run_forever()


## For simple test:
import time

def hola():
    while True:
        print("hola")
        time.sleep(1)
############################


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    executor=concurrent.futures.ProcessPoolExecutor()
    v=1
    b=1
    gprint("Entrando a loop event")
    future1=loop.run_in_executor(
        executor,
        functools.partial(hola_task,v))
    future2=loop.run_in_executor(
        executor,
        functools.partial(chao_task,b))
    tasks=[future1,future2]
    loop.run_until_complete(asyncio.gather(*tasks))
