import asyncio
import functools

#all coroutines
async def coromask(coro, args, fargs):
     _in=args
     obtained=await coro(*args)
     result=fargs(_in, obtained)
     return result
   
def renew(task, coro, fargs, *args):
    result=task.result()
    loop=asyncio.get_event_loop()
    task=loop.create_task(coromask(coro, result, fargs))
    task.add_done_callback(functools.partial(renew, task, coro, fargs))


def simple_fargs(_in, obtained):
    return _in

def simple_fargs2(_in, obtained):
    return obtained

def none_fargs(_in):
    return None
