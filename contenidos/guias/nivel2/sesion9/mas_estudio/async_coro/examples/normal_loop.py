import asyncio
import functools

async def holacoro(v):
    print("Hola %d" % v)
    await asyncio.sleep(1)
    return v+1
    
async def sumacoro(*args):
    c=sum(args)
    print("La suma es %d" %c)
    await asyncio.sleep(3)
    return c
    
    
async def normal():
    v=1
    sumar=[1,2]
    while True:
        await holacoro(v)
        c=await sumacoro(*sumar)
        v+=1
        sumar=[sumar[-1]].append(c)

loop=asyncio.get_event_loop()
task=loop.create_task(normal())


try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')
