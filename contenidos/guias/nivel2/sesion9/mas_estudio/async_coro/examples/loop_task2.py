import asyncio
import functools

async def holacoro(v):
    print("Hola %d" % v)
    await asyncio.sleep(1)
    return v+1
    
#special for every coroutine
async def coromask(coro, args):
     result=await coro(*args)
     return [result]
    
def renew(task, coromask, coro, *args):
    print(task.result())
    task=loop.create_task(coromask(coro,task.result()))
    task.add_done_callback(functools.partial(renew, task, coromask, coro))

args=[1]
task=loop.create_task(coromask(holacoro,args))
task.add_done_callback(functools.partial(renew, task, coromask, holacoro))

loop=asyncio.get_event_loop()

try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')
