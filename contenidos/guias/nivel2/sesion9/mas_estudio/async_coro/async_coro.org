#+TITLE: Python Asyncio Coroutines
#+LANGUAGE: es
#+LATEX_HEADER:\usepackage[spanish]{babel}
#+LATEX_HEADER:\selectlanguage{spanish}
#+INDEX: asyncio

* Async Coroutines (Corrutinas asíncronas).

** Introducción.

Una corrutina asíncrona es una forma muy especial de desarrollar una funcionalidad que
está pensada para coexistir con la operación simultánea de otras corrutinas.

En general su uso se centraliza en el desarrollo del intercambio de mensajes en red,
sockets y usos similares; en que el sistema debe esperar a que ocurra algo para
continuar, si embargo la corrutina permite mientras tanto seguir operando otras 
corrutinas.

Este taller está pensado para aprender lo básico del módulo /asyncio/ de python versión 3.5
en adelante. Sin embargo será necesario considerar un repaso de un tema muy importante para
comprender los alcances de esta herramienta, es el GIL.

** GIL: Global Interpreter Locking.

Es un administrador general que posee el lenguaje Python por diseño.  Es posible
analizar su funcionamiento inspeccionando el código C que lo define, según
como lo hizo David Baezly (recomendado ver sus charlas) [[http://www.dabeaz.com]].

Existen dos versiones del GIL, a partir de la versión Python 3.2 se ocupa una versión
mejorada por Antonie Pitrou.

Para la versión antigua, se observa que, para casos de multi hilos (Threading) el GIL ejecuta una rutina
pero bloquea el restos de los hilos, luego bloquea el hilo que ejecutaba y continúa
con otro hilo, bloqueando los otros.

#+CAPTION: Old GIL.  From Understanding GIL (David Baezly)
#+NAME: oldGIL
[[./img/gil_threading.png]]

Por diseño, periódicamente GIL revisa si es que hay solicitudes de otros hilos. En general es 
semáforo binario que cambia de hilo frente a la variación de una condición. Al cambiar de un 
hilo a otro ocurre una serie de señalización que permite controlar la coherencia
de la ejecución general.

#+CAPTION: New GIL model.  From Understanding GIL (David Baezly)
#+NAME: newGIL
[[./img/switch_thread.png]]

En la nueva versión de GIL, desarrollada para version(/py/) >= 3.2, el hilo bloqueado espera
a que el hilo que opera  GIL lo libere voluntariamente. Bajo un parámetro TIMEOUT, el 
hilo bloqueado avisa cada cierto tiempo que espera por su activación.

#+CAPTION: Nuevo GIL.  From Understanding GIL (David Baezly)
#+NAME: New GIL
[[./img/new_gil.png]]

Esto se traduce directamente en un impacto en un mejor rendimiento  (en general) del sistema.
Sin embargo, aún existe GIL gestionando la operación de las rutinas.

** Generadores

Para comprender el uso de las corrutinas asíncronas, sugiero que demos un repaso al concepto de generadores en python.
Seguimos con el material de David Baezly [[http://www.dabeaz.com/generators-uk/GeneratorsUK.pdf][Generators]]. Los generadores son también corrutinas (un subconjunto), tienen una forma de definición
bastante sencilla, utilizando el par /(@coroutine, yield)/ o, en las últimas versiones solo con /*yield*/

Es una de las herramientas más poderosas de Python. A grandes rasgos sirve para uso racional de la 
memoria. Por ejemplo, en vez de cargar un archivo completo a memoria, un generador va cargando solo lo solicitado (la línea).
O bien para generar un /for/ con una lista de 1000000 de números, no es necesario crearlos todos a la vez e 
inspeccionar, sino crear un generador que los haga aparecer a medida que se ocupan.

Podemos abordar al generador desde dos lados: crear un generador, usar un generador. Según la [[https://docs.python.org/3.6/howto/functional.html][documentación oficial]] de
Python.

*** Crear un generador

Como ya se mencionó, para crear un generador se debe tener en cuenta un diseño de un algoritmo que trabaja
con una cierta forma de datos secuenciales, lo que en principio podrías generar una lista y entregarla con
la sentencia *return*. Ahora bien, se  reemplaza con */yield/*.

#+NAME: Creación de un Generador
#+begin_src python
def get_records(self):
    while len(self.msg_bytes) > 0:
        record_type, record_length = unpack('>2B', self.msg_bytes[0:2])
        self.msg_bytes = self.msg_bytes[2:]
        try:
            yield self.select_record(record_type, record_length)
        except Exception as exec:
            print("No hay mensaje que traducir")
            print(exec)
#+end_src

*** Usar un generador
Una vez que se llama a la generador, el interprete de Python creará un objeto
/generator/, para acceder a la información que provee solo basta con operarlo con  *next()* o bien llamarlo
desde un controlador *for*. A la vez que se pueden encadenar varias generadores.

#+NAME: Llamar a un generador y usarlo, dentro de una corrutina asíncrona
#+begin_src python
async def run_test(loop):
    gsof = Gsof()
    await gsof.connect(IP, PORT)
    while True:
        try:
            await gsof.get_message_header()
            for m in gsof.get_records():
                print(m)
        except Exception as exec:
            loop.close()
            print(exec)
        except KeyboardInterrupt as ke:
            loop.close()
            print(ke)
#+end_src

** Corrutinas

El uso de *yield* se generaliza. Hace algo más que solo generar valores, también pueden /consumir/. En
una corrutina es posible utilizar el método *send()* para que consuma un objeto. La respuesta
se obtiene con *next()*. Para cerrar se utiliza *close()* generando la salida del generador.


#+NAME: Llamar a un generador y usarlo, dentro de una corrutina asíncrona
#+begin_src python
from coroutine import coroutine

@coroutine
def grep(pattern):
    print "Looking for %s" % pattern
    try:
        while True:
            line = (yield)
            if pattern in line:
                print line,
    except GeneratorExit:
        print "Going away. Goodbye"
#+end_src

Se diferencian de los Generadores en que:

- Generador :: produce valores => pull data
- Corrutina :: consume valores => push data

No es necesario que una corrutina tenga un valor de retorno con /yield/, aunque pueda tenerlo. Una 
corrutina puede estar enlazada en cascada con varias otras corrutinas, utilizando */pipes/*.

Para ensamblar varias corrutinas es necesario definir en un diseño un método inicial (que no es corrutina) 
que envíe el primer mensaje a la siguiente. Encadenando el mensaje hasta un punto final.

*** TODO Crear corrutina
*** TODO Usar corrutina

#+NAME: Encadenamiento de Corrutinas, @David Baezly
#+begin_src python

from coroutine import coroutine

# A data source.  This is not a coroutine, but it sends
# data into one (target)

import time
def follow(thefile, target):
    thefile.seek(0,2)      # Go to the end of the file
    while True:
         line = thefile.readline()
         if not line:
             time.sleep(0.1)    # Sleep briefly
             continue
         target.send(line)

# A sink.  A coroutine that receives data

@coroutine
def printer():
    while True:
         line = (yield)
         print line,

# Example use
if __name__ == '__main__':
    f = open("access-log")
    follow(f,printer())
#+end_src

En la muy  interesante charla de @dbaez de  [[http://www.dabeaz.com/coroutines/Coroutines.pdf][Coroutines]], se pueden ver casos de uso bastante atractivos de
esta herramienta.

La expresión */(yield)/* se utiliza para recibir los valores de entrada, en el ejemplo line es la variable
que recibe lo que se le envía a /printer/


** Corrutinas asíncronas

La necesidad de optimizar el uso de los computadores ha llevado a desarrollar distintas herramientas
que nos permiten crear mejor código. En ello vemos tanto a los generadores, como las corrutinas. Ahora
bien existe un concepto llamado *concurrencia* que consiste en la ocurrencia /"simultánea"/ de distintos
procesos en que a la vez existe interacción entre ellos. Lo que hace GIL al trabajar con diversos hilos sería concurrencia.

Para resolver la correcta operación de la concurrencia se crean los procesos asíncronos, desde
 v(py)=3.4 viene implementada la operación de corrutinas asíncronas en la /librería estandar/. Utilizando un solo hilo /single-threaded/
permite realizar intercambio de tareas en los procesos. Se centra en un *event loop* que ejecuta
las /async coro/. Administra los eventos I/O, de sistema, cambios de contexto, etc. 

En la [[https://docs.python.org/3/library/asyncio.html][documentación]] es posible ver el despliegue completo de la funcionalidad, que ofrece un buen rendimiento para el trabajo,
comunicación entre corrutinas y operación en red, entre otras características. 

Como se observa en la [[http://2014.es.pycon.org/static/talks/Concurrencia%20-%20Aitor%20Guevara.pdf][presentación]] de Aitor Guevara, hay una diferencia sustancial en los tiempos de ejecución 
entre el procesamiento normal y un procesamiento con asyncio.


#+CAPTION: Como ocurren los procesos normalmente, sin concurrencia
#+NAME: fig:normal
#+LABEL: fig:normal
[[./img/proceso_a.png]]

En la figura [[fig:normal]] se observa una secuencialidad, primero debe cargar uno para luego el otro.

#+CAPTION: Como ocurren los procesos utilizando asyncio
#+NAME: fig:asyncio
#+LABEL: fig:asyncio
[[./img/proceso_asyncio.png]]

En cambio, en la figura [[fig:asyncio]] se observa que a la vez cargan todos los procesos.

** Corrutinas en tareas [Tasks]

En el event loop para la ejecución de las corrutinas, se crea un administrador de ellas que permite monitorear
el estado de ejecución. Este administrador lleva por nombre Tasks, que heredan de la clase Future. Lo que permite, al definir
la ejecución de la corrutina, la creación de una tarea [Task]. Con esto se abre la posibilidad de ejecutar alguna acción
al terminarla o bien obtener su resultado.

Para la ejecución independiente de tareas [multi-Tasks] se debe llevar a cabo una definición de un algoritmo que permita
crear una nueva tarea (la misma, con algunas modificaciones) una vez la primera tarea finalice. Para eso se propone
una solución óptima que da lugar a la posibilidad de ejecutar varias tareas de manera independiente en un ciclo.

[[./img/looptask.png]]

Un ejemplo para tal caso se puede observar en el código siguiente, en que se ejecuta un "Hola" y un reposo cada 1sg, mientras
que cada 2 seg se ejecuta una suma de los dos últimos elementos de una lista de números.

#+BEGIN_SRC python
import asyncio
import functools

async def holacoro(v):
    print("Hola %d" % v)
    await asyncio.sleep(1)
    return v+1
    
async def sumacoro(*args):
    c=sum(args)
    print("La suma es %d" %c)
    await asyncio.sleep(3)
    return c
    
def fargs_holacoro(args, obtained):
    return [obtained]
    
def fargs_sumacoro(args, obtained):
    result= [args[-1], obtained]
    return result
    
#every coroutine
async def coromask(coro, args, fargs):
     _in=args
     obtained=await coro(*args)
     result=fargs(_in, obtained)
     return result
   
def renew(task, coro, fargs, *args):
    result=task.result()
    task=loop.create_task(coromask(coro, result, fargs))
    task.add_done_callback(functools.partial(renew, task, coro, fargs))

loop=asyncio.get_event_loop()

args=[1]
task1=loop.create_task(coromask(holacoro,args,fargs_holacoro))
task1.add_done_callback(functools.partial(renew, task1, holacoro, fargs_holacoro))

args2=[1,2]
task2=loop.create_task(coromask(sumacoro,args2,fargs_sumacoro))
task2.add_done_callback(functools.partial(renew, task2, sumacoro, fargs_sumacoro))


try:
     loop.run_forever()
except KeyboardInterrupt:
     print('Loop stopped')
#+END_SRC
