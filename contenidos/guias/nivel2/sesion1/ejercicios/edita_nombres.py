import csv

nombre = "nombres.csv"
contador = 0
arreglo = lambda x: x.strip().capitalize()

with open(nombre, "r") as f:
	reader = csv.reader(f, 
		delimiter=",",
		quotechar='"')
	for row in reader:
		print(row)
		print(type(row))
		if contador>=1:
			nuevafila=[arreglo(elem) for elem in row]
			print(nuevafila)
		else:
			contador += 1
