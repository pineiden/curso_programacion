import csv

nombres_archivos = ['geo_cilindro.csv',
                    'geo_cuadrilateros.csv',
                    'geo_paralelepipedos.csv',
                    'geo_triangulos.csv']

area_cilindro = lambda r,h: (2*3.14*r)*(r+h)
area_cuadrilatero = lambda x,y: x*y
area_triangulo = lambda b,h: (b*h)/2
area_paralele = lambda a,b,c: area_cuadrilatero(a,b)+area_cuadrilatero(b,c)+area_cuadrilatero(c,a)
volumen_cilindro = lambda r,h: (3.14*r*r)*h
volumen_paralele = lambda a,b,c: a*b*c
perimetro_cuadrilatero = lambda x,y: 2*(x+y)
perimetro_triangulo = lambda a,b,c: a+b+c

reporte = 'geo_reporte.csv'
i = 1
with open(reporte, 'w') as rep:
    rep.write('id;tipo_figura;perimetro;area;volumen\n')
    
    for archivo in nombres_archivos:
        with open(archivo, 'r') as fig:
            reader = csv.DictReader(fig, delimiter=';')
            for row in reader:
                if archivo == 'geo_cilindro.csv':
                    rep.write('%d-cili-%s;cilindro;None;%s;%s\n' %(i,
                                                                 row['id'],
                                                                 area_cilindro(float(row['radio']),
                                                                               float(row['altura'])),
                                                                 volumen_cilindro(float(row['radio']),
                                                                                  float(row['altura'])))
                    )
                if archivo == 'geo_cuadrilateros.csv':
                    rep.write('%d-c-%s;cuadrilatero;%s;%s;None\n' %(i,
                                                                  row['id'],
                                                                  perimetro_cuadrilatero(
                                                                      float(row['base']),
                                                                      float(row['altura'])),
                                                                  area_cuadrilatero(
                                                                      float(row['base']),
                                                                      float(row['altura'])))
                              )
                if archivo == 'geo_paralelepipedos.csv':
                    rep.write('%d-p-%s;paralelepipedo;None;%s;%s\n' %(i,
                                                                    row['id'],
                                                                    area_paralele(
                                                                        float(row['ancho']),
                                                                        float(row['altura']),
                                                                        float(row['largo'])),
                                                                    volumen_paralele(
                                                                        float(row['ancho']),
                                                                        float(row['altura']),
                                                                        float(row['largo'])))
                              )
                if archivo == 'geo_triangulos.csv':
                    rep.write('%d,t-%s;triangulo;%s;%s;None\n' %(i,
                                                                 row['id'],
                                                                 perimetro_triangulo(
                                                                     float(row['lado1']),
                                                                     float(row['lado2']),
                                                                     float(row['base'])),
                                                                 area_triangulo(
                                                                     float(row['base']),
                                                                     float(row['altura'])))
                              )
                i+=1                                                 
                
        
                              
                
                    
            
