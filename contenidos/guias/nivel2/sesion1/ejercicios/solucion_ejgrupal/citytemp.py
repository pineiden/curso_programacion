import csv

# Se define variable con nombre de archivo de datos
archivo ="ciudad_temp.csv"
# Leemos archivo csv con la informacion
# por cada campo de fila -> td
# por cada fila -> tr

#========
# Se crea lista vacia
# En esta lista se pondrá cada fila leída desde archivo
contenido_tabla = []

with open(archivo, 'r') as f:
	reader = csv.DictReader(f)
	for row in reader:
		# Se crea un nuevo diccionario por cada fila leida
		# El nuevo diccionario corresponde a las columnas de una fila
		# en una tabla del formato HTML
		# La forma en que se crea diccionario es usando comprehensión
		new_dict = { key: "<td>%s</td>" %value for key, value in row.items() }
		# Creamos un string que es la fila de la tabla en html
		# con la etiqueta tr, poniendo en medio todas las columnas 
		# correspondientes
		fila_tabla ="<tr>%s</tr>" % "".join(new_dict.values())
		# agregamos a la lista
		contenido_tabla.append(fila_tabla)
		
# imprimimos usando una lista comprehensiva
[print(x) for x in contenido_tabla]

# Definimos el nombre del archivo template html
template = "template.html"
# unimos todas las filas y creamos un string 
# que será insertado en el template reemplazando una etiqueta
contenido_tabla_string = "".join(contenido_tabla)
print(contenido_tabla_string)
# Leemos el archivo template.html
# cada etiqueta será cambiada despues por la info
# de los archivos csv

# creamos lista que contrendrá el texto del template
text_html = []
with open(template,"r") as temp_file:
	for line in temp_file:
		text_html.append(line)
		
# Pasamos toda la lista a un solo string
texto_template = "".join(text_html)
	
# definimos titulo de la pagina html
titulo = "Temperatura de Ciudades"

# reemplazamos la etiqueta por titulo
# nos entrega un nuevo string
txt_contitulo = texto_template.replace("[TITULO]", titulo)
# reemplazamos elementos_fila por el contenido
txt_contenido = txt_contitulo.replace("[ELEMENTOS_FILA]",contenido_tabla_string)
print(txt_contenido)
 

# guardamos el string conseguido en nuevo archivo html 
final ="ciudades_temp.html"

with open(final, "a+") as f:
	f.write(txt_contenido)

# abrir con firefox
