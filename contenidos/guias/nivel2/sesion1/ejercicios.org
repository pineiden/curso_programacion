#+TITLE: Ejercicios de Práctica

* Introducción

Los siguientes ejercicios están pensados para practicar con los conocimientos de
*python* adquiridos en la guía 1 de este nivel. De esta manera será posible
fijar los conocimientos básicos.


* Buscar palíndromos

Los palíndromas son aquellas frases o palabras que si inviertes los caracteres, o si las
lees de derecha a izquierda, dice lo mismo que si la leyeras de manera normal.

En el archivo *buscar_palind.txt* hay una lista de palabras o frases. La misión
es escribir un *script* de python que lea el archivo y muestre solamente
aquellas líneas palíndromas, además colocar el número de línea de la
coincidencia.

Tener en consideración, antes de comprobar o verificar la condición será
necesario:

- buscar el conjunto de números que corresponden a las letras ASCII

- eliminar los espacios, puedes usar *replace()*

- eliminar los caracteres no letras

- cambiar letras con tilde a letras sin tilde

- cambiar mayúsculas a minúsculas

- Ver este enlace, en que se ocupa una técnica bien interesante para cambiar las
  tildes, cuidando de que la *ñ* no cambie
  [[https://es.stackoverflow.com/questions/135707/c%C3%B3mo-puedo-reemplazar-las-letras-con-tildes-por-las-mismas-sin-tilde-pero-no-l][Cambio de tilde a sin tilde]]

- Revisar la documentación https://docs.python.org/3.7/library/unicodedata.html
 
Desde un sentido técnico, lo que estarás escribiendo es un filtro que deja pasar
ciertas cosas y otras las retiene porque no cumplen las condiciones.

Datos a considerar en la técnica:

- Biblioteca unicodedata y el método normalize

- Crear un diccionario con valores iniciales, partiendo de una secuencia
*dictionary.fromkeys(sequence[, value])*

- el método *translate()* de string para cambiar carácteres.

- la lista de caracteres unicode la puedes encontrar en este [[https://www.compart.com/en/unicode][sitio Unicode]] o [[https://unicode-table.com/es/#basic-latin][Acá]]

El siguiente es un ejemplo de cómo limpiar de acentos un texto.

#+BEGIN_SRC python
from unicodedata import normalize

s = 'Pingüino: Málaga es una ciudad fantástica y en Logroño me pica el... moño'
# Crea un diccionario cuyas llaves son los enteros ordinales correspondientes
# al carácter unicode 301 y 308 (dieresis y tilde), y cuyos valores
# son None, lo que haría pasar estos valores a None
trans_tab = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
s = normalize('NFKC', normalize('NFKD', s).translate(trans_tab))
s
'Pinguino: Malaga es una ciudad fantastica y en Logroño me pica el... moño'
#+END_SRC


* Mensaje Secreto

Un científico loco de un continente del sur ha desarrollado un método
para codificar (encriptar) mensajes de manera efectiva y, durante años, ha
logrado comunicarse  con su equipo de trabajo efectivamente, sin sufrir
filtraciones.

He conseguido los planos del modelo y he conseguido replicar los rudimentos del
sistema de codificación. Solo se sabe que se tienen dos archivos y se debe
realizar una operación de conjuntos entre cada línea de ambos para obtener el
mensaje. 

Su tarea será entonces leer ambos archivos y encontrar el mensaje, utilizando el
lenguaje *Python* y todas las herramientas que ya conoce.

¡Usted puede!
