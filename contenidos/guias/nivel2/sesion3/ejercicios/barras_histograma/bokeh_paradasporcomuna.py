import random
import math
import sys
sys.path.append('../datos')
from transantiago import paradasXcomuna_dict


counterXcomuna = {}

for key, value in paradasXcomuna_dict.items():
    if '' in value:
        value.remove('')
    counterXcomuna.update({key: len(value)})
    print("%s --> %s"%(key,value))

lista_comunas = list(counterXcomuna.keys())
for key, value in counterXcomuna.items():
    print("%s --> %s"%(key,value))

# seleccionamos una lista de 10 comunas al azar

cantidad = 30
seleccion = set()

def randomcomuna(lista, seleccion):
    comuna = random.choice(lista)
    while True:
        if not comuna in seleccion:
            seleccion.add(comuna)
            break
        else:
            comuna = random.choice(lista)
    return comuna

lista = [randomcomuna(lista_comunas, seleccion) for x in range(cantidad)]

print("Selección de comunas")
[print(comuna, counterXcomuna.get(comuna,0)) for comuna in lista]

valores = [counterXcomuna.get(comuna,0) for comuna in lista]


from bokeh.io import show, output_file
from bokeh.plotting import figure

output_file("paradasXcomuna.html")

TOOLTIPS = [
    ("index", "$index"),
]

p = figure(x_range=lista,
           plot_height=500,
           title="Cantidad de Paradas por Comuna",
)
p.xaxis.major_label_orientation = math.pi/2
p.vbar(x=lista, top=valores, width=0.9)

p.xgrid.grid_line_color = None
p.y_range.start = 0

show(p)
