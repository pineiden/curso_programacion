import random
import sys
sys.path.append('../datos')
from transantiago import paradasXcomuna_dict


counterXcomuna = {}

for key, value in paradasXcomuna_dict.items():
    if '' in value:
        value.remove('')
    counterXcomuna.update({key: len(value)})
    print("%s --> %s"%(key,value))

lista_comunas = list(counterXcomuna.keys())
for key, value in counterXcomuna.items():
    print("%s --> %s"%(key,value))

# seleccionamos una lista de 10 comunas al azar

cantidad = 30
seleccion = set()

def randomcomuna(lista, seleccion):
    comuna = random.choice(lista)
    while True:
        if not comuna in seleccion:
            seleccion.add(comuna)
            break
        else:
            comuna = random.choice(lista)
    return comuna

lista = [randomcomuna(lista_comunas, seleccion) for x in range(cantidad)]

print("Selección de comunas")
[print(comuna, counterXcomuna.get(comuna,0)) for comuna in lista]

valores = [counterXcomuna.get(comuna,0) for comuna in lista]

import pyqtgraph as pg
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget

win = pg.plot()
win.setWindowTitle('Paraderos por Comuna')

# create bar chart
barItem= lambda comuna, valor: pg.BarGraphItem(
    x=comuna,
    height=valor,
    width=0.6,
    brush='r')

indexes = [i for i, value in enumerate(lista)]
bars = barItem(indexes, valores)
print(bars)

win.addItem(bars)

## Start Qt event loop unless running in interactive mode or using 
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QApplication.instance().exec_()
