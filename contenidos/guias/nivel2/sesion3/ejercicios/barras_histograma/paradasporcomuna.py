import random
import sys
sys.path.append('../datos')
from transantiago import paradasXcomuna_dict


counterXcomuna = {}

for key, value in paradasXcomuna_dict.items():
    if '' in value:
        value.remove('')
    counterXcomuna.update({key: len(value)})
    print("%s --> %s"%(key,value))

lista_comunas = list(counterXcomuna.keys())
for key, value in counterXcomuna.items():
    print("%s --> %s"%(key,value))

# seleccionamos una lista de 10 comunas al azar

cantidad = 30
seleccion = set()

def randomcomuna(lista, seleccion):
    comuna = random.choice(lista)
    while True:
        if not comuna in seleccion:
            seleccion.add(comuna)
            break
        else:
            comuna = random.choice(lista)
    return comuna

lista = [randomcomuna(lista_comunas, seleccion) for x in range(cantidad)]

print("Selección de comunas")
[print(comuna, counterXcomuna.get(comuna,0)) for comuna in lista]

valores = [counterXcomuna.get(comuna,0) for comuna in lista]

import matplotlib.pyplot as plt
import numpy as np


# se tiene lista y valores
# se crea figura y se asigna a plot

fig, ax = plt.subplots()
# se carga figura previamente y luego
# se añaden los datos

"""
Documentación:
https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html

bar(x, height, *, align='center', **kwargs)
bar(x, height, width, *, align='center', **kwargs)
bar(x, height, width, bottom, *, align='center', **kwargs)
"""
bars=plt.bar(lista, valores, .50, align='center')
ax.set_xticklabels(lista, rotation='vertical')

plt.show()
