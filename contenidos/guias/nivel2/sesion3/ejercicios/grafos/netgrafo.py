# Este grafo representa distintos caminos
# con trazos en común

import math
import networkx as nx
import matplotlib.pyplot as plt


g =  nx.Graph()

nodes = ["A","A1","A2","B","C","D","D1","E","E1"]
color_id = [0,1,2,0,0,0,1,0,2]
cdic = dict(zip(nodes, color_id))
color_dict = {0:"red",1:"green",2:"blue"}
g.add_nodes_from(nodes)
list_colors = [color_dict[cdic[llave]] for llave in g.nodes()]
print(color_id)
print(list_colors)



g.add_edge("A", "B")
g.add_edge("B", "C")
g.add_edge("C", "D")
g.add_edge("D", "E")

g.add_edge("A1", "B")
g.add_edge("C", "D1")

g.add_edge("A2", "B")
g.add_edge("D", "E1")

print(g)
print(g.nodes())
print(g.edges())

plt.subplot()
nc=nx.draw(g,
           with_labels=True,
           node_color=list_colors)


plt.show()
