import csv
import datetime
from datetime import timedelta, datetime

# Lectura de archivos a visualizar

stations = ['VALN','TRPD']
data={}

for station in stations:
    timestamp=[]
    N=[]
    E=[]
    U=[]
    file_name = station + '.csv'
    print(file_name)
    with open(file_name) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            timestamp.append(float(row['timestamp']))
            N.append(row['N'])
            E.append(row['E'])
            U.append(row['U'])
    ts=[datetime.utcfromtimestamp(ts) for ts in timestamp ]
    data.update({station:{'timestamp':ts,
                          'N':list(map(float,N)),
                          'E':list(map(float,E)),
                          'U':list(map(float,U))}})

print("="*20)
print("Lectura de archivos correcta")
print(data.keys())
print("="*20)

from bokeh.plotting import figure, output_file, show, reset_output
from bokeh.layouts import gridplot

for station in stations:
    # un nuevo archivo por estacion
    f=output_file("%s.html" %station)
    # create a new plot with a datetime axis type
    p1 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p1.line(data[station]['timestamp'], data[station]['N'], color='red', alpha=0.5)
    p2 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p2.line(data[station]['timestamp'], data[station]['E'], color='green', alpha=0.5)
    p3 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p3.line(data[station]['timestamp'], data[station]['U'], color='navy', alpha=0.5)
    show(gridplot([[p1],[p2],[p3]]))
    reset_output()
