import csv
import datetime
from datetime import timedelta, datetime

# Lectura de archivos a visualizar

stations = ['VALN','TRPD']
data={}

for station in stations:
    timestamp=[]
    N=[]
    E=[]
    U=[]
    file_name = station + '.csv'
    print(file_name)
    with open(file_name) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            timestamp.append(float(row['timestamp']))
            N.append(row['N'])
            E.append(row['E'])
            U.append(row['U'])
    ts=[datetime.utcfromtimestamp(ts) for ts in timestamp ]
    data.update({station:{'timestamp':ts,
                          'N':list(map(float,N)),
                          'E':list(map(float,E)),
                          'U':list(map(float,U))}})

print("="*20)
print("Lectura de archivos correcta")
print(data.keys())
print("="*20)

"""
Código bash para verificar fechas límite

awk -F, '(NR==2){
print $1
}{
ender=$1
}END{
print ender}' VALN.csv | xargs -I input date -d @input
"""

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
#from my_toolbox.load_save import load_columns
from io import BytesIO
import numpy as np

im=plt.imread('logo.png')

def watermark_with(img, mark):
    img.seek(0)
    tmp = BytesIO()
    with Image.open(img) as base, Image.open(mark) as overlay:
        # It seems an alpha blend of .3 makes sense.
        marked = Image.blend(base, overlay, 0.3)
    marked.save(tmp, "PNG")
    return tmp


def plot_traces(dataset,
                colors=('r', 'b', 'k'),
                title="Estación"):
    # Three subplots sharing both x/y axes
    print(dataset)
    fig, (ax1, ax2, ax3) = plt.subplots(
        3,
        sharex=True)
    if title is not None:
      fig.suptitle(title, fontsize=22)
    ax3.set_xlabel(r'$t\, [HH:MM]\, utc$', fontsize=16)
    ax1.set_ylabel(r'$E\,[m]$', fontsize=16)
    ax2.set_ylabel(r'$N\,[m]$', fontsize=16)
    ax3.set_ylabel(r'$U\,[m]$', fontsize=16)
    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax1.plot(dataset['timestamp'], dataset['N'], color=colors[0])
    ax2.plot(dataset['timestamp'], dataset['E'], color=colors[1])
    ax3.plot(dataset['timestamp'], dataset['U'], color=colors[2])
    return fig, (ax1, ax2, ax3)


def jump_index(values, threshold):
    x=values
    #for k in range(x.size):
    #    if abs(x[k+1] - x[k]) > threshold:
    #        return k+1


for st in stations:
    print(data[st])
    fig, (ax1, ax2, ax3) = plot_traces(data[st], title="%s 24 abril 2017"%st)
    #fig.autofmt_xdate()
    #plt.savefig(st)
    plt.show()
