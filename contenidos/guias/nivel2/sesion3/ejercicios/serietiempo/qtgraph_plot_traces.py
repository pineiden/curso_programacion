import csv
import datetime
from datetime import timedelta, datetime

# Lectura de archivos a visualizar

stations = ['VALN','TRPD']
data={}

for station in stations:
    timestamp=[]
    N=[]
    E=[]
    U=[]
    file_name = station + '.csv'
    print(file_name)
    with open(file_name) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            timestamp.append(float(row['timestamp']))
            N.append(row['N'])
            E.append(row['E'])
            U.append(row['U'])
    ts=[ts for ts in timestamp ]
    data.update({station:{'timestamp':ts,
                          'N':list(map(float,N)),
                          'E':list(map(float,E)),
                          'U':list(map(float,U))}})

print("="*20)
print("Lectura de archivos correcta")
print(data.keys())
print("="*20)

# se llama al gestor de pyqtgraph
import pyqtgraph as pg

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtCore import QTime, QTimer

app = QApplication(sys.argv)


def int2dt(ts):
    if not ts:
        return datetime.utcfromtimestamp(ts) # workaround fromtimestamp bug (1)
    return(datetime.fromtimestamp(ts))

class TimeAxisItem(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        return [int2dt(value).strftime("%Hh%Mmn%Ss") for value in values]
windows = {station:pg.GraphicsWindow() for station in stations }

for station in stations:
    # creando plot items
    win = windows[station]
    p1=win.addPlot(
        row=0, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p1.plot(data[station]['timestamp'], data[station]['N'], pen='r',)
    p2=win.addPlot(
        row=1, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p2.plot(data[station]['timestamp'], data[station]['E'], pen='y')
    p3=win.addPlot(
        row=2, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p3.plot(data[station]['timestamp'], data[station]['U'], pen='b')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***
