# Se hace un cambio de tabas por ;
# el separador es mas visible de esta manera
# se guarda la salida en em2
sed -e 's/\t/;/g' ensayos_mecanicos.txt | sed 's/ //g' > em2.csv
# se lee em2.csv y se define una bandera de partida (una variable de control)
# si el comienzo del primer campo es index la bandera es 1
# si el comienzo del primer campo es Control, la bander cambia a 0
# Esto se define en base a los patrones encontrados en el archivo fuente
awk -v flag=0 '{
	if( $1 ~ /^index/){flag=1}; 
	if( $1 ~ /^Control/){flag=0}; 
	if (flag==1){print $0} 
}' em2.csv  > em3.csv
# se cambian los valores numéricos con ,  a .
# a groso modo, en otros casos será necesaria
# una verificacion campo a campo
sed -i 's/,/\./g' em3.csv
