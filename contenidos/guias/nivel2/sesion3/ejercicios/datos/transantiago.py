import math
import csv
import chardet

#filename = 'paradas_transantiago.csv'

filename = '../datos/pt.csv'

paradasXcomuna_dict = {}
# inicializa set
lineasXcomuna_dict = {}
# inicializa set 
comunasXlinea_dict = {}
# inicializa set
rutaXlinea_dict = {}
# inicializa lista de tuplas

llaves = {'comuna':'Comuna',
              'x':'x',
              'y':'y',
              'linea':'Servicio Usuario',
              'paradero':'Código  paradero Usuario'}

def is_number(s):
    try:
        float(s) # for int, long and float
    except ValueError:
        try:
            complex(s) # for complex
        except ValueError:
            return False
    return True

def data2dict(row, llave, agregar, diccionario,
              objeto=set,
              conversion=None):
    """
    Esta funcion toma un dato de la línea leída
    que es un diccionario
    row :: diccionario de datos origen csv
    llave :: la llave que estamos controlando en diccionario
    diccionario :: el diccionario en que se coloca nuevo dato
    objeto :: puede ser set o list, según el caso necesario
    """
    if not row[llave] in diccionario:
        diccionario.update({row[llave]:objeto()})
    if objeto == set:
        if row[llave] in diccionario:
            valor = row[llaves[agregar]]
            if conversion:
                valor = conversion(valor)
            diccionario[row[llave]].add(valor)
    if objeto == list:
        # comprehension de tupla:
        # *(x for x in range(10)),
        if conversion:
            tupla = *(conversion(row[llaves[x]]) for x in agregar 
									if is_number(row[llaves[x]])),
        else:
            tupla = *(row[llaves[x]] for x in agregar),
        if row[llave] in diccionario:
            diccionario[row[llave]].append(tupla)
# obteniendo la codificación desde bash
encoding='ISO-8859-1'
#with open(filename, 'r', newline='', encoding='utf-8', ) as file:
#    encoding = chardet.detect(file.read())

print("Encoding de tipo %s" %encoding)

try:
    with open(filename, 'r') as file:
        # llaves de interes
        separator = ';'
        reader = csv.DictReader(file,
                                delimiter=separator)
        for row in reader:
            # jugar con paradas comunas
            llave = llaves['comuna']
            data2dict(row,
                      llave,
                      'paradero',
                      paradasXcomuna_dict)
            data2dict(row,
                      llave,
                      'linea',
                      lineasXcomuna_dict)
            # jugar con comunas linea
            llave = llaves['linea']
            data2dict(row,
                      llave,
                      'comuna',
                      comunasXlinea_dict)
            data2dict(row,
                      llave,
                      ('x','y'),
                      rutaXlinea_dict,
                      list,
                      float)
except Exception as ex:
    print("Error en lectura de archivo, error -> %s" %ex)
    raise ex
        
        
for key, value in paradasXcomuna_dict.items():
    print("%s --> %s"%(key,value))

            
for key, value in rutaXlinea_dict.items():
    print("%s --> %s"%(key,value))
