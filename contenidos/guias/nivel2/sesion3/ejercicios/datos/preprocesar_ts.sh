awk -F';' 'BEGIN{
OFS=";";
print "comuna","linea","paradero","lat","lon";
}{
if(NR==1){
  for (i; i<NF;i++)
      print $i
      if($i ~ /^Comuna/){
	    ncomuna=i
      }
      if($i ~ /^Servicio Usuario/){
	    nlinea=i
      }
      if($i ~ /^Código  paradero Usuario/){
	    nparadero=i
      }
      if($i ~ /^x/){
	    nlat=i
      }
      if($i ~ /^y/){
	    nlon=i
      }
  print ncomuna, nlinea, nparadero, nlat, nlon
}
else if(NR>1){
  #print $ncomuna, $nlinea, $nparadero, $nlat, $nlon
}
}' paradas_transantiago.csv


