import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget
app = QApplication(sys.argv) 

# se llama al gestor de pyqtgraph
import pyqtgraph as pg
pg.setConfigOption('background', 'w')
# Genera una figura con un primer gráfico
pw = pg.plot(points, seno, pen='r')
pw.plot(points, coseno, pen='b')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***
