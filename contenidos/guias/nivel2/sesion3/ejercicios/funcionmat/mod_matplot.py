import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]


# Se importa el sistema de gráficos
import matplotlib.pyplot as plt

# se crea figura o marco de dibujo

figura = plt.figure()

# se define título de figura

figura.suptitle("Funciones trigonometricas")

plt.plot(points, seno, label='seno')
plt.plot(points, coseno, label='coseno')

plt.xlabel('x label')
plt.ylabel('y label')

plt.title("Gráfico Simple")

plt.legend()

plt.show()
