import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]

from bokeh.plotting import figure, output_file, show

output_file("trigonometria.html")

# Crea un nuevo plot con título y ejes definidos.
figura = figure(title="Gráfico Trigonometría",
           x_axis_label='x',
           y_axis_label='y')

# Añadir una serie de puntos
# renderizados en lineas

figura.line(points, seno,
            legend="Seno",
            line_width=2,
            color='red')
figura.line(points, coseno,
            legend="Coseno",
            line_width=2,
            color='green')

# Mostrar gráfico

show(figura)
