import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
ypoints = np.linspace(-1, 1, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]


# Se importa el sistema de gráficos
import matplotlib.pyplot as plt

# se crea figura o marco de dibujo
# se definen 4 subplots


figura, subplots = plt.subplots(2, 2)

# se define título de figura

figura.suptitle("Funciones trigonometricas")

subplots[0][0].plot(points, seno, label='seno')
subplots[0][1].plot(points, coseno, label='coseno')
subplots[1][0].plot(ypoints, arcseno, label='arcseno')
subplots[1][1].plot(ypoints, arccoseno, label='arccoseno')

subplots[0][0].set_xlabel('x label')
subplots[0][0].set_ylabel('y label')
subplots[0][0].set_title("Gráfico Seno")
subplots[0][0].legend()

subplots[0][1].set_xlabel('x label')
subplots[0][1].set_ylabel('y label')
subplots[0][1].set_title("Gráfico Coseno")
subplots[0][1].legend()

subplots[1][0].set_xlabel('x label')
subplots[1][0].set_ylabel('y label')
subplots[1][0].set_title("Gráfico ArcSeno")
subplots[1][0].legend()

subplots[1][1].set_xlabel('x label')
subplots[1][0].set_ylabel('y label')
subplots[1][1].set_title("Gráfico ArcCoseno")
subplots[1][1].legend()

figura.savefig('trigonometricas_2.png') 

plt.show()
