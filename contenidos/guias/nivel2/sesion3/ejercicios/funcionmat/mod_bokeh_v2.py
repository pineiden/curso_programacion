import numpy as np
import math

points = np.linspace(0, 2*math.pi, 30)
ypoints = np.linspace(-1, 1, 30)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]


# Se importa el sistema de gráficos
from bokeh.io import output_file, show
from bokeh.layouts import gridplot
from bokeh.palettes import Viridis3
from bokeh.plotting import figure

output_file("trigonometria_v2.html")

# se crea figura o marco de dibujo
# se definen 4 subplots

#Las figuras son 'glifos'

p1 = figure(plot_width=250, plot_height=250, title=None)
p1.circle(points, seno, size=10, color="red")

p2 = figure(plot_width=250, plot_height=250, title=None)
p2.triangle(points, coseno, size=10, color="green")

p3 = figure(plot_width=250, plot_height=250, title=None)
p3.square(ypoints, arcseno, size=10, color="yellow")

p4 = figure(plot_width=250, plot_height=250, title=None)
p4.cross(ypoints, arccoseno, size=10, color="blue")

show(gridplot([[p1, p2],
               [p3, p4]]))
