import numpy as np
import math

points = np.linspace(0, 2*math.pi, 30)
ypoints = np.linspace(-1, 1, 30)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]

# se llama al gestor de pyqtgraph
import pyqtgraph as pg

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget

app = QApplication(sys.argv)

win = pg.GraphicsWindow()
# Automaticamente genera una grilla para los ítems

# creando plot items
p1=win.addPlot( row=0, col=0)
p1.plot(points, seno, pen='r')
p2=win.addPlot( row=0, col=1)
p2.plot(points, seno, pen='y')
p3=win.addPlot( row=1, col=0)
p3.plot(ypoints, arcseno, pen='b')
p4=win.addPlot( row=1, col=1)
p4.plot(ypoints, arccoseno, pen='g')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***
