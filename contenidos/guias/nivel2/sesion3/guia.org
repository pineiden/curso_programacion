#+TITLE: Visualización de Datos
#+AUTHOR: David Pineda, Daniel Mendez
#+DATE:<2018-07-28 sáb>

* Introducción

Esta sesión consiste en el uso diferentes módulos para la visualización de datos. Estos
datos pueden tener diferentes orígenes pero deben ser tratados de tal manera que
obtengamos estructuras como listas o diccionarios que nos permitan generar la visualización
que deseemos.

Con las herramientas de python entregadas hasta el momento ya es posible crear visualizaciones
de datos, por lo que nos dedicaremos a estudiar el uso de algunos de los módulos más
importantes o aquellos que nos proveen las mejores herramientas para lo que necesitemos.

Cada uno de estos módulos provee la posibilidad de realizar un grupo de tipos de gráficos,
la documentación provista en cada página oficial les entregará toda la información necesaria
para tales efectos.

Por último, realizaremos el trabajo inverso, en que desde una imagen mapa de bits extraeremos
información que podemos utilizar para distintos motivos.

* Módulos de herramientas matemáticas y científicas

Lo primero que debemos tener en cuenta es el uso de las herramientas para el
procesamiento de los datos cuyo origen puede ser tanto un dispositivo, una
base de datos o un archivo, entre los más conocidos.

Cada    *-network UNCLAIMEDuna de las siguientes herramientas provee un conjunto de funcionalidades
suficiente para realizar cálculos de matemáticas de diferente índole. Veremos
a continuación.

Si hay funciones similares se diferencian entre ellos por la implementación, 
tiempo de cálculo, entre otros factores.

Instala en ambiente los siguientes módulos.

#+BEGIN_SRC bash 
python pip install numpy scipy matplotlib sympy nose
#+END_SRC

** Math

Es el módulo de matemáticas de la *biblioteca estandar* de *python*, que provee
las funciones fundamentales para operar en nuestro código.

Para un análisis exhautivo de sus funciones revisa la [[https://docs.python.org/3/library/math.html][documentación]].

#+BEGIN_SRC python
# funciones con base y exponente; a^b
lista = [2,3,4,50,-20] 
pi = math.pi
yexponente = lambda x: math.exp(x)
lista_exp = [y(x) for x in lista]
yelevado = lambda x,y: math.pow(x,y)
yraiz = lambda x: math.swrt(x)
# uno dos listas
nlista = zip(lista, lista_exp)
lista_elevado = [yelevado(x,y) for x,y in nlista]
# la raiz de lo anterior
lista_raiz = [yraiz(x) for x in lista_elevado]
# algo de trigonometria
lista_cos = [math.cos(2*pi*x) for x in lista_raiz]
lista_sin = [math.sin(2*pi*x) for x in lista_raiz]
# radianes a  polares
lista_polar = [math.polar(x) for x in lista]
#+END_SRC


** CMath

Es el módulo de matemáticas que permite operar con números complejos (imaginarios),
de la forma a $z=a+bi$

Para un análisis exhautivo de sus funciones revisa la [[https://docs.python.org/3/library/cmath.html#module-cmath][documentación]].

#+BEGIN_SRC python
a=1
b=2
z = a+b*j
# el ángulo de apertura o fase
fase = cmath.phase(z)
# el número en exprsion polar
r, fase = cmath.polar(z)
#+END_SRC

** Numpy

[[http://www.numpy.org/][Numpy]] es uno de los más importantes módulos de *python*, ofrece en extenso 
con su implementación lo siguiente:

- Trabajo con vectores y matrices
- Funciones matemáticas sofisticadas
- Integración con código C y Fortran
- Herramientas de álgebra lineal, Fourier, etc

#+BEGIN_SRC python
# importar
import numpy as np
# Creación de un array
a=np.array([[1,2,3]])
# Creación de una matriz 
M=np.array([[3,4,5],[7,6,4],[4,9,8]])
# Operar matriz con array
M.dot(a)
# Bradcasting(diferente):
M*a
# crear matriz de 0
ceros = np.zeros((3,4))
print(ceros)
# crear matriz de 1
ones = np.ones((3,2))
print(ones)
#+END_SRC

** Scipy

[[https://www.scipy.org/][Scipy]] es el complemento a Numpy, que provee herramientas de mayor complejidad 
para problemas de índole científico y cálculos matemáticos.

** Pandas

Es otro módulo del mismo paquete que provee funcionalidades rápidas para acceder
a archivos, modificar y ordenar matrices, entre otras características.

#+BEGIN_SRC python
import pandas as pd
# IO tools: https://pandas.pydata.org/pandas-docs/stable/io.html
file_path = 'ejemplo_pandas.csv'
file_data = pd.read_csv(file_path)
#+END_SRC

* Módulos de gráficos

Los distintos módulos existentes y en desarrollo proveen características específicas
de su diseño para las que fueron pensadas. En nuestro caso *matplotlib* está pensado
para mostrar imágenes de calidad pero estáticas [[fig.anatomy]], en el caso de *bokeh* está pensado
para mostrar gráficos en entorno /web/ y *pyqtgraph* está pensado para gráficas en 
tiempo real, siendo más difícil de utilizar.

#+CAPTION: Anatomía general de un gráfico, fuente: matplotlib.org
#+NAME:   fig.anatomy
[[file:./anatomy.png]]

** Instalar los modulos necesarios

Primero, hay que instalar las dependencias como *sudo*

#+BEGIN_SRC bash
sudo apt build-dep python3-tk
sudo apt install python3-tk
sudo apt build-dep qt5-default
sudo apt install qt5-default
#+END_SRC

Luego, instalamos los módulos python en nuestro ambiente virtual.

#+BEGIN_SRC python
pip install matplotlib
pip install bokeh
pip install pyqt5
pip install pyqtgraph
#+END_SRC

** Procedimiento general

A modo general existe una serie de pasos que se recomiendan
para construir adecuadamente una gráfica de datos.

1.- Fuente de datos

2.- Preprocesar (bash, awk)

3.- Procesar (python)

4.- Gráficar o visualizar

** Una función matemática 

Se grafican una y dos funciones distintas en el mismo gráfico. También se 
visualizarán en gráficas separadas dentro de la misma ventana.

*** Matplotlib

Graficamos un par de funciones trigonométricas {seno, coseno}.

#+BEGIN_SRC python 
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]


# Se importa el sistema de gráficos
import matplotlib.pyplot as plt

# se crea figura o marco de dibujo

figura = plt.figure()

# se define título de figura

figura.suptitle("Funciones trigonometricas")

plt.plot(points, seno, label='seno')
plt.plot(points, coseno, label='coseno')

plt.xlabel('x label')
plt.ylabel('y label')

plt.title("Gráfico Simple")

# Activar leyenda
plt.legend()

# Guardar figura
fig.savefig('trigonometricas.png') 

# Mostrar 
plt.show()
#+END_SRC

Luego, una versión un poco más compleja, con varias
secciones en el mismo gráfico se puede obtener de la siguiente manera.


#+BEGIN_SRC python 
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
ypoints = np.linspace(-1, 1, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]


# Se importa el sistema de gráficos
import matplotlib.pyplot as plt

# se crea figura o marco de dibujo
# se definen 4 subplots


figura, subplots = plt.subplots(2, 2)

# se define título de figura

figura.suptitle("Funciones trigonometricas")

subplots[0][0].plot(points, seno, label='seno')
subplots[0][1].plot(points, coseno, label='coseno')
subplots[1][0].plot(ypoints, arcseno, label='arcseno')
subplots[1][1].plot(ypoints, arccoseno, label='arccoseno')

subplots[0][0].set_xlabel('x label')
subplots[0][0].set_ylabel('y label')
subplots[0][0].set_title("Gráfico Seno")
subplots[0][0].legend()

subplots[0][1].set_xlabel('x label')
subplots[0][1].set_ylabel('y label')
subplots[0][1].set_title("Gráfico Coseno")
subplots[0][1].legend()

subplots[1][0].set_xlabel('x label')
subplots[1][0].set_ylabel('y label')
subplots[1][0].set_title("Gráfico ArcSeno")
subplots[1][0].legend()

subplots[1][1].set_xlabel('x label')
subplots[1][0].set_ylabel('y label')
subplots[1][1].set_title("Gráfico ArcCoseno")
subplots[1][1].legend()

fig.savefig('trigonometricas_2.png') 

plt.show()
#+END_SRC

*** Bokeh

Veamos el mismo problema con *Bokeh*, la [[https://bokeh.pydata.org/en/latest/][Página Oficial]] puedes
consultar la documentación y otros ejemplos interesantes. Este 
módulo provee la generación de gráficos muy bellos y con enfoque
web.

#+BEGIN_SRC python
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]

from bokeh.plotting import figure, output_file, show

output_file("trigonom.html")

# Crea un nuevo plot con título y ejes definidos.
figura = figure(title="Gráfico Trigonometría",
           x_axis_label='x',
           y_axis_label='y')

# Añadir una serie de puntos
# renderizados en lineas

figura.line(points, seno,
            legend="Seno",
            line_width=2,
            color='red')
figura.line(points, coseno,
            legend="Coseno",
            line_width=2,
            color='green')

# Mostrar gráfico
show(figura)
#+END_SRC

De manera análoga, para graficar con subplots, se define una grilla
que reconoce la estructura de una matriz en la cual colocamos
cada figura.

#+BEGIN_SRC python
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 30)
ypoints = np.linspace(-1, 1, 30)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]


# Se importa el sistema de gráficos
from bokeh.io import output_file, show
from bokeh.layouts import gridplot
from bokeh.palettes import Viridis3
from bokeh.plotting import figure

output_file("trigonometria_v2.html")

# se crea figura o marco de dibujo
# se definen 4 subplots

#Las figuras son 'glifos'

p1 = figure(plot_width=250, plot_height=250, title=None)
p1.circle(points, seno, size=10, color="red")

p2 = figure(plot_width=250, plot_height=250, title=None)
p2.triangle(points, coseno, size=10, color="green")

p3 = figure(plot_width=250, plot_height=250, title=None)
p3.square(ypoints, arcseno, size=10, color="yellow")

p4 = figure(plot_width=250, plot_height=250, title=None)
p4.cross(ypoints, arccoseno, size=10, color="blue")

show(gridplot([[p1, p2],
               [p3, p4]]))
#+END_SRC


*** PyQtGraph

Este módulo necesita activar una ventana qt sobre
la que mantener la imagen generada, además requiere
que la version de *pyqt* instalada sea *5.10.1* debido
a que la última presenta algunos errores en el uso conjunto
con *pyqtgraph*.

#+BEGIN_SRC python
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 100)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
app = QtGui.QApplication(sys.argv) 

# se llama al gestor de pyqtgraph
import pyqtgraph as pg

# Genera una figura con un primer gráfico
pw = pg.plot(points, seno, pen='r')
pw.plot(points, coseno, pen='b')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***

#+END_SRC

Ahora, para profundizar la relación entre el sistema de ventantas QT 
y qtgraph, será necesario comprender el esquema de posiciones y clases
que se utiliza para diagramar un gráfico en una ventana[[fig.qtgraph]].

#+CAPTION: Estructura de un gráfico qtgraph con ventana QT
#+NAME:   fig.qtgraph
[[file:./plottingClasses.png]]


Ahora bien, para graficar en una grilla con varios plots, será necesario
utilizar otras funcionalidades.


#+BEGIN_SRC python
import numpy as np
import math

points = np.linspace(0, 2*math.pi, 30)
ypoints = np.linspace(-1, 1, 30)
seno = [math.sin(x) for x in points]
coseno = [math.cos(x) for x in points]
arcseno = [math.asin(x) for x in ypoints]
arccoseno = [math.acos(x) for x in ypoints]

# se llama al gestor de pyqtgraph
import pyqtgraph as pg

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
app = QtGui.QApplication(sys.argv) 

win = pg.GraphicsWindow()
# Automaticamente genera una grilla para los ítems

# creando plot items
p1=win.addPlot( row=0, col=0)
p1.plot(points, seno, pen='r')
p2=win.addPlot( row=0, col=1)
p2.plot(points, seno, pen='y')
p3=win.addPlot( row=1, col=0)
p3.plot(ypoints, arcseno, pen='b')
p4=win.addPlot( row=1, col=1)
p4.plot(ypoints, arccoseno, pen='g')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***


#+END_SRC

** Un histograma

Los histograma son gráficos que permiten visualizar conjuntos de valores
y comparar con otros conjuntos, es una de las formas más sencillas
y visuales que existen para analizar datos.

Desde aquí comenzamos a utilizar datos cuyas fuentes provienen
de algún archivo o database, tomar en cuenta la diferencia.

Consideramos el procedimiento recomendado, primero analizamos 
la fuente de datos y realizamos los filtros necesarios para 
que nuestro programa en python los lea adecuadamente.

*** Paradas por comuna del transantiago

El objetivo de esta visualización es obtener la cantidad de paradas
por comuna, por lo que podemos definir una estrategia previa
a la lectura del archivo que nos entregue el conjunto de comunas,
con eso podemos inicializar un conjunto un diccionario que inicalice
un contador.

Un problema que puede ser usual con datos generados en sistemas
operativos deficientes como '$Windows' es la codificación. Es 
lo que ocurre en este caso, se puede verificar de la siguiente manera,
reparando el desperfecto.

#+BEGIN_SRC bash
file paradas_transantiago.csv
iconv -f ISO-8859-1 -t UTF-8 paradas_transantiago.csv > pt.csv
#+END_SRC

**** Preprocesar datos.
El estudio de la fuente de datos indica que están correctamente formateados.

Por lo tanto se procede a la lectura del archivo csv desde python. 

¿Qué cosas interesantes se pueden obtener y visualizar?

1.-- Paradas por comunas
2.-- Cantidad de líneas distintas por comuna 
3.-- Cantidad de comunas que atraviesa
4.-- La ruta por línea (para ejemplo con mapa)
5.-- ¿Más ideas? 
6.-- Etcétera

Con python, creamos un script ubicado en *datos/transantiago.py*
que nos permitirá acceder a los datos que necesitemos para 
poder visualizarlos

**** Matplotlib

Graficar en *matplotlib* requerirá llamar el set de datos ya
procesados y construir la gráfica con una selección aleatoria(en este caso).

#+BEGIN_SRC python
import random
import sys
sys.path.append('../datos')
from transantiago import paradasXcomuna_dict


counterXcomuna = {}

for key, value in paradasXcomuna_dict.items():
    if '' in value:
        value.remove('')
    counterXcomuna.update({key: len(value)})
    print("%s --> %s"%(key,value))

lista_comunas = list(counterXcomuna.keys())
for key, value in counterXcomuna.items():
    print("%s --> %s"%(key,value))

# seleccionamos una lista de 10 comunas al azar

cantidad = 30
seleccion = set()

def randomcomuna(lista, seleccion):
    comuna = random.choice(lista)
    while True:
        if not comuna in seleccion:
            seleccion.add(comuna)
            break
        else:
            comuna = random.choice(lista)
    return comuna

lista = [randomcomuna(lista_comunas, seleccion) for x in range(cantidad)]

print("Selección de comunas")
[print(comuna, counterXcomuna.get(comuna,0)) for comuna in lista]

valores = [counterXcomuna.get(comuna,0) for comuna in lista]

import matplotlib.pyplot as plt
import numpy as np


# se tiene lista y valores
# se crea figura y se asigna a plot

fig, ax = plt.subplots()
# se carga figura previamente y luego
# se añaden los datos

"""
Documentación:
https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html

bar(x, height, *, align='center', **kwargs)
bar(x, height, width, *, align='center', **kwargs)
bar(x, height, width, bottom, *, align='center', **kwargs)
"""
bars=plt.bar(lista, valores, .50, align='center')
ax.set_xticklabels(lista, rotation='vertical')

plt.show()
#+END_SRC

**** Bokeh

De la misma manera, para *Bokeh* será necesario cambiar
las líneas que tienen relación con el anterior módulo
por las que correspondan.

#+BEGIN_SRC python

from bokeh.io import show, output_file
from bokeh.plotting import figure

output_file("paradasXcomuna.html")

TOOLTIPS = [
    ("index", "$index"),
]

p = figure(x_range=lista,
           plot_height=500,
           title="Cantidad de Paradas por Comuna",
)
p.xaxis.major_label_orientation = math.pi/2
p.vbar(x=lista, top=valores, width=0.9)

p.xgrid.grid_line_color = None
p.y_range.start = 0

show(p)
#+END_SRC

**** PyQtGraph


Con este módulo, se puede conseguir también un gráfico
de barras, aunque puede terminar siendo más difícil trabajar
en el posicionamiento de las etiquetas por barra.

Lo elemental de este sistema es que cada distinto elemento
gráfico es un ítem y se añade a la ventada activada.

#+BEGIN_SRC python
import pyqtgraph as pg
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget

win = pg.plot()
win.setWindowTitle('Paraderos por Comuna')

# create bar chart
barItem= lambda comuna, valor: pg.BarGraphItem(x=comuna, 
    height=valor, 
    width=0.6, 
    brush='r')

indexes = [i for i, value in enumerate(lista)]
bars = barItem(indexes, valores)
print(bars)

win.addItem(bars)

## Start Qt event loop unless running in interactive mode or using 
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QApplication.instance().exec_()
#+END_SRC


*** Pruebas de fuerza mecánica

**** Preprocesar datos.
Se escribe un script en bash que realice cambios necesarios 
para que la información a trabajar sea la estrictamente necesaria.

#+BEGIN_SRC bash
# Se hace un cambio de tabas por ;
# el separador es mas visible de esta manera
# se guarda la salida en em2
sed -e 's/\t/;/g' ensayos_mecanicos.txt | sed 's/ //g' > em2.csv
# se lee em2.csv y se define una bandera de partida (una variable de control)
# si el comienzo del primer campo es index la bandera es 1
# si el comienzo del primer campo es Control, la bander cambia a 0
# Esto se define en base a los patrones encontrados en el archivo fuente
awk -v flag=0 '{
	if( $1 ~ /^index/){flag=1}; 
	if( $1 ~ /^Control/){flag=0}; 
	if (flag==1){print $0} 
}' em2.csv  > em3.csv
# se cambian los valores numéricos con ,  a .
# a groso modo, en otros casos será necesaria
# una verificacion campo a campo
sed -i 's/,/\./g' em3.csv
#+END_SRC

** Un gráfico serie tiempo

Es usual la necesidad de crear gráficas de ciertos fenómenos 
medidos con sensores y equipos dataloggers. La visualización
más elemental es la llamada serie-tiempo, la cual muestra
el valor medido en relación al tiempo en que se registró el valor.

Como se observa en la figura [[fig.serie_tiempo]], se tienen tres
gráficas que representan el desplazamiento según tres ejes(norte, este, arriba)
en base a una posición de referencia.

#+CAPTION: Gráfica Serie Tiempo
#+NAME:   fig.serie_tiempo
[[file:./ejercicios/TRPD.png]]


*** Módulos de manipulación de tiempo

Se utiliza el módulo *datetime* de la biblioteca
estándar de *python*.

Crea objetos que permiten manipular fecha y tiempo,
bajo distintos formatos. 

Un valor usual a trabajar es la estampa de tiempo
o *timestamp*, que es una forma de representar 
el tiempo utilizando valores numéricos.

Para convertir este valor a un objeto *datetime*
será necesario hacer lo siguiente:

#+BEGIN_SRC python
ts=1493069030
mydatetime=datetime.utcfromtimestamp(ts)
print(mydatetime)
#+END_SRC

También, será necesario muchas veces tener un
string de la fecha que contenga la mayor
cantidad de información posible. En este
caso se recomienda utilizar *isoformat*.

#+BEGIN_SRC python
ts=1493069030
mydatetime=datetime.utcfromtimestamp(ts)
print(mydatetime.isoformat())
#+END_SRC


*** Matplotlib

Se realiza una lectura de dos estaciones, cada
estación tiene tres ejes. Se debe definir una figura
con tres subplots, a cada subplot se le asigna un
color particular.

Los datos de timestamp son transformados a *datetime*
y formateados con la herramienta *mdate* de matplotlib.

#+BEGIN_SRC python
import csv
import datetime
from datetime import timedelta, datetime

# Lectura de archivos a visualizar

stations = ['VALN','TRPD']
data={}

for station in stations:
    timestamp=[]
    N=[]
    E=[]
    U=[]
    file_name = station + '.csv'
    print(file_name)
    with open(file_name) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            timestamp.append(float(row['timestamp']))
            N.append(row['N'])
            E.append(row['E'])
            U.append(row['U'])
    ts=[datetime.utcfromtimestamp(ts) for ts in timestamp ]
    data.update({station:{'timestamp':ts,
                          'N':list(map(float,N)),
                          'E':list(map(float,E)),
                          'U':list(map(float,U))}})

print("="*20)
print("Lectura de archivos correcta")
print(data.keys())
print("="*20)

"""
Código bash para verificar fechas límite

awk -F, '(NR==2){
print $1
}{
ender=$1
}END{
print ender}' VALN.csv | xargs -I input date -d @input
"""

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
#from my_toolbox.load_save import load_columns
from io import BytesIO
import numpy as np

im=plt.imread('logo.png')

def watermark_with(img, mark):
    img.seek(0)
    tmp = BytesIO()
    with Image.open(img) as base, Image.open(mark) as overlay:
        # It seems an alpha blend of .3 makes sense.
        marked = Image.blend(base, overlay, 0.3)
    marked.save(tmp, "PNG")
    return tmp


def plot_traces(dataset,
                colors=('r', 'b', 'k'),
                title="Estación"):
    # Three subplots sharing both x/y axes
    print(dataset)
    fig, (ax1, ax2, ax3) = plt.subplots(
        3,
        sharex=True)
    if title is not None:
      fig.suptitle(title, fontsize=22)
    ax3.set_xlabel(r'$t\, [HH:MM]\, utc$', fontsize=16)
    ax1.set_ylabel(r'$E\,[m]$', fontsize=16)
    ax2.set_ylabel(r'$N\,[m]$', fontsize=16)
    ax3.set_ylabel(r'$U\,[m]$', fontsize=16)
    ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax3.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax1.plot(dataset['timestamp'], dataset['N'], color=colors[0])
    ax2.plot(dataset['timestamp'], dataset['E'], color=colors[1])
    ax3.plot(dataset['timestamp'], dataset['U'], color=colors[2])
    return fig, (ax1, ax2, ax3)


def jump_index(values, threshold):
    x=values
    #for k in range(x.size):
    #    if abs(x[k+1] - x[k]) > threshold:
    #        return k+1


for st in stations:
    print(data[st])
    fig, (ax1, ax2, ax3) = plot_traces(data[st], title="%s 24 abril 2017"%st)
    #fig.autofmt_xdate()
    #plt.savefig(st)
    plt.show()
#+END_SRC

*** Bokeh

En *bokeh* la generación de las gráficas, no deja de ser simple.

#+BEGIN_SRC python
# cambiar la parte de gráfica por:
from bokeh.plotting import figure, output_file, show, reset_output
from bokeh.layouts import gridplot

for station in stations:
    # un nuevo archivo por estacion
    f=output_file("%s.html" %station)
    # create a new plot with a datetime axis type
    p1 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p1.line(data[station]['timestamp'], data[station]['N'], color='red', alpha=0.5)
    p2 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p2.line(data[station]['timestamp'], data[station]['E'], color='green', alpha=0.5)
    p3 = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p3.line(data[station]['timestamp'], data[station]['U'], color='navy', alpha=0.5)
    show(gridplot([[p1],[p2],[p3]]))
    reset_output()

#+END_SRC

*** PyQtGraph

Y, con *pyqtgraph* será necesario hacer unos ajustes en la data, la lista
*timestamp* no se debe convertir a *datetime*, quedando solo números.

#+BEGIN_SRC python 
# la línea de ts:
# ts=[ts for ts in timestamp ]
#
# se llama al gestor de pyqtgraph
import pyqtgraph as pg

# Primero activamos una ventana QT
import sys
from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtCore import QTime, QTimer

app = QApplication(sys.argv)


def int2dt(ts):
    if not ts:
        return datetime.utcfromtimestamp(ts) # workaround fromtimestamp bug (1)
    return(datetime.fromtimestamp(ts))

class TimeAxisItem(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        return [int2dt(value).strftime("%Hh%Mmn%Ss") for value in values]
windows = {station:pg.GraphicsWindow() for station in stations }

for station in stations:
    # creando plot items
    win = windows[station]
    p1=win.addPlot(
        row=0, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p1.plot(data[station]['timestamp'], data[station]['N'], pen='r',)
    p2=win.addPlot(
        row=1, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p2.plot(data[station]['timestamp'], data[station]['E'], pen='y')
    p3=win.addPlot(
        row=2, col=0,
        axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    p3.plot(data[station]['timestamp'], data[station]['U'], pen='b')

if __name__ == '__main__':
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        app.exec_()  # Start QApplication event loop ***
#+END_SRC

** Grafos 

A veces será necesario mostrar máquinas de estado, relaciones entre
entidades, nodos, redes. La forma de visualización con grafos permite
comprender visualmente las relaciones entre cada elemento.

*** Graphviz
Es un módulo que hereda del lenguaje *dot* la forma de escribir 
grafos.

En python se puede tener algo del estilo:

#+BEGIN_SRC python
from graphviz import Digraph
g = Digraph('G', filename='hello.gv')
g.edge('Hello', 'World')
g.view()
#+END_SRC

Una máquina de estados como

#+BEGIN_SRC python
from graphviz import Digraph

f = Digraph('finite_state_machine', filename='fsm.gv')
f.attr(rankdir='LR', size='8,5')

f.attr('node', shape='doublecircle')
f.node('LR_0')
f.node('LR_3')
f.node('LR_4')
f.node('LR_8')

f.attr('node', shape='circle')
f.edge('LR_0', 'LR_2', label='SS(B)')
f.edge('LR_0', 'LR_1', label='SS(S)')
f.edge('LR_1', 'LR_3', label='S($end)')
f.edge('LR_2', 'LR_6', label='SS(b)')
f.edge('LR_2', 'LR_5', label='SS(a)')
f.edge('LR_2', 'LR_4', label='S(A)')
f.edge('LR_5', 'LR_7', label='S(b)')
f.edge('LR_5', 'LR_5', label='S(a)')
f.edge('LR_6', 'LR_6', label='S(b)')
f.edge('LR_6', 'LR_5', label='S(a)')
f.edge('LR_7', 'LR_8', label='S(b)')
f.edge('LR_7', 'LR_5', label='S(a)')
f.edge('LR_8', 'LR_6', label='S(b)')
f.edge('LR_8', 'LR_5', label='S(a)')

f.view()
#+END_SRC

Otros ejemplos se pueden estudiar en la [[https://graphviz.readthedocs.io/en/stable/examples.html][documentatión]]

*** Networkx

Es un módulo de grafos que puede utilizar matplotlib para visualizar
las estructuras definidas.

Se instala:

#+BEGIN_SRC python
pip install networkx
#+END_SRC

Un ejemplo sencillo.

Distintos caminos representados por un grafo.

1. El transporte pasa por {A,B,C,D,E}
2. Una persona parte por A1 y pasa por {B,C}, para llegar a D1
3. Otra persona parte por A2 y pasas por {B,C,D} para llegar a E1


#+BEGIN_SRC python
# Este grafo representa distintos caminos
# con trazos en común

import math
import networkx as nx
import matplotlib.pyplot as plt


g =  nx.Graph()

nodes = ["A","A1","A2","B","C","D","D1","E","E1"]
color_id = [0,1,2,0,0,0,1,0,2]
cdic = dict(zip(nodes, color_id))
color_dict = {0:"red",1:"green",2:"blue"}
g.add_nodes_from(nodes)
list_colors = [color_dict[cdic[llave]] for llave in g.nodes()]
print(color_id)
print(list_colors)



g.add_edge("A", "B")
g.add_edge("B", "C")
g.add_edge("C", "D")
g.add_edge("D", "E")

g.add_edge("A1", "B")
g.add_edge("C", "D1")

g.add_edge("A2", "B")
g.add_edge("D", "E1")

print(g)
print(g.nodes())
print(g.edges())

plt.subplot()
nc=nx.draw(g,
           with_labels=True,
           node_color=list_colors)


plt.show()
#+END_SRC

*** Graphtool 

Es otra herramienta para construir grafos, se basa en *python*
y *c++*.

Se puede clonar desde:

#+BEGIN_SRC bash
git clone https://git.skewed.de/count0/graph-tool.git
#+END_SRC

Previamente instalar las dependencias

#+BEGIN_SRC bash
 sudo apt install libodb-boost-dev
 sudo apt install libboost1.62-dev/
 sudo apt install libboost1.62-dev
 sudo apt install libboost-graph-dev
 sudo apt install libboost-graph-parallel-dev
 sudo apt install libboost-python1.62-dev
 sudo apt install libboost-iostreams-dev
 sudo apt install libboost-thread1.62-dev 
 sudo apt install libcgal
 sudo apt install libcgal-qt5-dev libcgal-dev
 sudo apt install libcairomm-1.0-dev
 sudo apt install libsparsehash-dev
#+END_SRC

Y luego compilar:

#+BEGIN_SRC bash
./configure
make
sudo make install
#+END_SRC

Necesita algunas bibliotecas de *c++* adicionales, como *boost*

** Tareas

1.- Para datos del transantiago, graficar los puntos 2 al 4, agrega un gráfico que te gustaría
realizar.

2.- Para datos de pruebas de fuerza mecánica graficar las serie tiempo en una figura con 
subplots, realizar los gráficos de barras agrupando intervalos.

3.- Realizar un grafo de las relaciones existentes entre las personas
de la *Escuela Cooperativa de Permacultura*, sus nodos, organizaciones
amigas, etc. Investigue.

* Manipulación de imágenes
** Modelando una imagen
Cómo último tema en el contexto de visualización de datos, veremos
algunos ejemplos y ejercicios con imágenes.

Las imágenes en programación se tratan como una matriz. Y una matriz
es una lista de listas.

Una imagen de 200 x 300 pixeles se modela como una matriz que tiene
200 columnas y 300 filas.

#+BEGIN_SRC python
fila = [x for x in range(10)]
matriz = [fila for x in range(15)]
print(matriz)
#+END_SRC

Cada valor en la matriz representa la intensidad de la luz en 
el pixel de la imagen asocida. Este valor puede ir desde el 0
al 255 o desde 0.0 a 1.0, dependiendo el tipo de dato.

Una imagen en blanco y negro se representa con una sola matriz. Si
la imagen está en colores, necesitamos 3 matrices. Una para rojo, 
una para verde y una para azul. 

** Representación de imágenes con Numpy

La forma más cómoda de manejar imágenes en python es usar los
arreglos del módulo numpy, los que están hechos para funcionar
mejor con otras librerias de Python, como matplotlib o scipy.

El siguiente código genera una imagen de 500 x 500 pixeles, en 
blanco y negro, en la que cada pixel tiene un valor random entre
0.0 y 1.0.

#+BEGIN_SRC python
import numpy as np
from matplotlib import pyplot as plt

random_image = np.random.random([500, 500])

plt.imshow(random_image, cmap='gray', interpolation='nearest')
plt.show()
#+END_SRC

*cmap* es el [[https://matplotlib.org/users/colormaps.html][mapa de colores]] con el que se mostrará la imagen. Algunas opciones son 'cool' o 'plasma'.
*interpolation* es una operación matemática que permite estimar el valor de un punto en la imagen en caso de no conocerse.

*** De listas a arreglos de numpy
Otra herramienta útil y que además es muy sencilla, es la 
que se usa para pasar de una lista a un arreglo de numpy.

#+BEGIN_SRC python
import numpy as np
list1 = [0,1,2,3,4]
arr1d = np.array(list1)
#+END_SRC

De manera equivalente, si hemos construido una matriz hecha
con listas, podemos pasarla rápidamente a un arreglo numpy.

#+BEGIN_SRC python
list2 = [[0,1,2], [3,4,5], [6,7,8]]
arr2d = np.array(list2, dtype='float')
print(arr2d)
#+END_SRC


** Un módulo para importar imágenes

Python provee ejemplos de imágenes para probar los algoritmos. 
El módulo *scikit-image* es una herramienta de la que veremos algunos ejemplos.

Debemos instalar el módulo usando pip

#+BEGIN_SRC bash
pip install scikit-image
#+END_SRC

Una vez instalado, podemos importar *skimage* para tener algunos ejemplos
de imágenes.

#+BEGIN_SRC python
from skimage import data
from matplotlib import pyplot as plt
#Extraer los datos de la imagen
coins = data.coins()
#Imprimir typo de dato de coins, tipo de dato de la imagen y tamaño
print(type(coins), coins.dtype, coins.shape)

#Mostrar la imagen
plt.imshow(coins, cmap='gray', interpolation='nearest')
plt.show()
#+END_SRC

Una imagen en colores, como habíamos dicho, sera un cubo o 
3 matrices una al lado de la otra. Una para cada color.

#+BEGIN_SRC python
cat = data.chelsea()
print("Tamaño:", cat.shape)
print("Valores  min/max:", cat.min(), cat.max())

plt.imshow(cat, interpolation='nearest')
#+END_SRC

** Ejercicios

1. Cree y muestre por pantalla una imagen en degradé 
horizontal de negro a blanco.

+ *BONUS*: Haga lo mismo pero con una imagen en colores. 

2. Tome una [[http://scikit-image.org/docs/api/skimage.data.html][imagen de muestra]] a su elección y dupliquela de tamaño repitiendo las columnas pares y las filas impares.

+ *BONUS*: Duplique el tamaño de la imagen, pero para cada pixel nuevo introducido su valor será el promedio de sus 8 vecinos.

