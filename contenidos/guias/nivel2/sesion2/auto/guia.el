(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:orgd895c97"
    "sec:org12f9c87"
    "sec:org1202380"
    "sec:org11d8be7"
    "sec:orgaabf1b8"
    "sec:orga083661"
    "sec:orgfaf05bb"
    "sec:org4967405"
    "sec:org748d21a"
    "sec:orge6aa10c"
    "sec:orgee50448"
    "sec:orga24f0e0"
    "sec:orgd931293"
    "sec:orgaee6ff3"
    "sec:org8f9e2f7"
    "sec:org2d747a2"
    "sec:orga3ce62f"
    "sec:org7a19122"))
 :latex)

