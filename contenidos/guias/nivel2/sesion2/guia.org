#+TITLE: Características Especiales de Python
#+AUTHOR: Daniel Mendez, David Pineda
#+EMAIL: dpineda@uchile.cl
#+DATE: <2018-07-20 vie>

* Introducción

En esta segunda sesión nos adentraremos en las características 
especiales del lenguaje *Pyhton* que nos permitirá generar 
código de calidad y legible.

Son funcionalidades que nos permiten ahorrar código y ser expresivos
a la hora de programar.

Las listas y diccionarios comprehensiv(a-o)s nos permiten ser muy
concisos  a la hora de crear estas estructuras de datos, de manera
tal que podemos ahorrar varias líneas de código si las usamos.

Luego pasaremos a aprender a crear funciones generales y extensibles, 
mucho más flexibles que funciones con parámetros definidos de antemano. 
Así como también a comprender el funcionamiento *lambda* y la creación
de *generadores*, aquellas funciones especiales que nos proveeen 
información a medida que la necesitemos.

También, dado que nuestro código siempre estará sujeto a errores 
y fallas de sistema es que aprenderemos algunas herramientas que nos
permiten asegurar la ejecución de nuestro código y descubrir sus fallas.

Por último aprenderemos a crear un módulo instalable que podamos utilizar 
en distintos proyectos.

* Listas comprehensivas

Las listas comprehensivas son una forma concisa de generar 
una serie de objetos dentro de una lista. La definen en la
[[https://www.python.org/dev/peps/pep-0202/][PEP 202]], y se implementa como se puede observar en la [[https://docs.python.org/3/tutorial/datastructures.html?highlight=comprehensive][documentación]]

Usualmente pahttp://librosweb.es/libro/algoritmos_python/capitulo_12/validaciones.htmlra crear una lista de elementos se hace lo
siguiente, con un ejemplo sencillo:

#+BEGIN_SRC python 
lista = []
for i in range(10):
    lista.append(i)
print(lista)
#+END_SRC

Ahora bien, es posible hacer lo mismo en una sola línea,
al estilo comprehensivo.

#+BEGIN_SRC python 
lista = [x for x in range(10)]
print(lista)
#+END_SRC

* Diccionarios comprehensivos

De manera análoga, para crear de manera concisa un diccionario
con una serie de elementos, es posible hacerlo al estilo comprehensivo.
Esta funcionalidad se definen en la [[https://www.python.org/dev/peps/pep-0274/][PEP 278]] 

La creación de un diccionario utilizando comprehensión es de
la siguiente manera:

#+BEGIN_SRC python
{x:x**2 for x in range(10)}
#+END_SRC

** Ejercicios

Estos son ejercicios sencillos en el uso y definición de diccionarios.

**** Carácter a hexadecimal

Crear un diccionario cuyas llaves sean las letras del alfabeto
y el valor correspondiente sea el código ASCII hexadecimal de la 
misma.

**** Carácter a MORSE

Crear un diccionario que entregue el *código morse* de cada letra y carácter posible.

* Funciones con parámetros args y kwargs.

Puede ocurrir que tengamos diferentes funciones y debamos
ocupar la misma estructura de datos como entrada, sea una
lista o bien un diccionario. La manera de poder realizar
funciones que, a modo general, puedan ocupar estos objetos
que continen una cantidad de elementos indeterminada es
el uso de *unpack* o expansión del objeto.

Se trata de poder utilizar funciones del tipo:

#+BEGIN_SRC python
def funcion(nombre, apellido, edad=30, *args, **kwargs):
    #Hacer algo
    texto = kwargs['texto']
    lista = [nombre.upper(), apellido.upper(), edad, texto, args[0]]
    return lista
#+END_SRC


** Unpacking de lista

Cuando no conocemos la cantidad de elementos con que debemos trabajar
pero necesitamos utilizarlos como entrada de nuestra función, o bien
utilizar los elementos de una lista en distintas funciones, es buena idea
utilizar expansión de listas al modo *\*args* en la definición de funciones.

Veamos un caso sencillo, la suma de los elementos cuadráticos de una lista.

#+BEGIN_SRC python
# Una función que usa todos los elementos de la lista
def sumcuad(*args):
    cuad = lambda x: x**2
    lista = [cuad(x) for x in args]
    return sum(*lista)
# Otra función que utiliza el tercer elemento 
def tercero(*args):
    te = None
    if len(args)>=3:
        print("El tercer elemento es %s" %args[2])
        te = args[2]
    else:
        print("No hay tercer elemento")    
    return te
lista = [2,34,1,435,23]
sumcuad(*lista)
tercero(*lista)
#+END_SRC

Otro uso que tiene es cuando tenemos algunas entradas bien definidas
y utilizamos solo expansión de lista al momento de llamar la función.

#+BEGIN_SRC python
def sumatres(a,b,*args):
    return a+b+args[0]
lista = [2,34,1,435,23]
sumatres(*lista)
#+END_SRC

** Unpacking de diccionario

De manera análoga a la expansión de listas, la expansión de diccionario
nos permite utilizar las *keywords* de manera genérica en distintas funciones
utilizando el mismo diccionario.

#+BEGIN_SRC python
def define_direccion(nombre, apellido, **direccion):
    calle = direccion.get('calle', "Sin nombre")
    numero = direccion.get('numero', 0)
    comuna = direccion.get('comuna', 'Santiago')
    texto = "%s %s vive en: %s %d, %s" %(nombre, apellido, calle, numero, comuna)
    return texto
direccion = {'calle':'Los Duraznos', 'numero':101, 'comuna': 'La Pintana'}
print(define_direccion("David", "Pineda", **direccion))
#+END_SRC

Así también, es posible utilizar expansión de diccionario para entregar
como argumentos algunas *keyword* ya definidas.

#+BEGIN_SRC python
def nombre_completo(nombre="David", apellido="Pineda", **kwargs):
    texto = "%s %s" %(nombre, apellido)
    return texto
direccion = {'nombre': "Daniel", 'apellido': 'Méndez'}
print(nombre_completo(**direccion))
#+END_SRC

* Usar funciones con callbacks

Esta es una técnica que se puede utilizar ya que las funciones también
son objetos que se almacenan en memoria listos para ser ejecutados 
con algún argumento aún sin conocer, la técnica del *callback* permite
definir funciones genéricas que operen bajo un algoritmo definido que 
utilice la función que entreguemos.

Haremos dos funciones diferentes que operen sobre una lista y otra
que necesite un *callback* y la lista de argumentos.

#+BEGIN_SRC python 
def sumcuad(*args):
    cuad = lambda x: x**2
    lista = [cuad(x) for x in args]
    return sum(*lista)

def imprimir(*args):
    print(args)

def operador(callback, lista):
    return callback(*lista)
   
lista = [2,34,1,435,23]
operador(sumcuad, lista)
operador(imprimir, lista)
#+END_SRC 

* Funciones Generadoras

Cuando trabajamos con ciertos trozos de código que estimamos podrían
generar el uso de mucha memoria de la manera tradicional, podemos
crear funciones generadoras que sean más eficientes (y rápidas) en
la realización de esta acción.

Se destaca el uso de la palabra *yield* (rendir, dar, ceder, producir)

#+BEGIN_SRC python
def mi_generador(lista):
    for palabra in lista:
        yield palabra.upper()
lista = ["david","pineda","programa","python"]
for palabra in mi_generador(lista):
    print(palabra)
#+END_SRC

Una buena guía práctica sobre este tema lo puedes ver en [[https://anandology.com/python-practice-book/iterators.html][Python Practice Book]]

También, de esta [[http://www.dabeaz.com/generators-uk/][guía de David Beazley]] veremos algunos otros ejemplos.
En la carpeta generadores puedes ver los archivos de ejemplo que se 
ofrecen en la guía de Beazley.

** Ejercicio

Este ejercicio trata de lo siguiente:

Para enviar información entre dos computadoras $A --> B$ es necesario
conocer las características del medio por el que se envía y sus puntos
de conexión.

En informática, el canal más básico es el *socket* y este tiene, lo
que se llama, una *capacidad de canal* en *bytes*. Es decir
pueden transmitirse a lo más *b bytes* por un canal. Además
existe un valor *timeout* que es el tiempo de espera para obtener
respuesta desde el otro lado.

Ahora bien, la información que se transmite es una serie de bytes
que parten de /A/ a /B/ y viceversa. Estos bytes son la *información
serializada*. Es decir, la representación en *byte* de los objetos
con que trabaja un lenguaje, algunos de estos permiten una conversión
directa y para otros se necesita definir una seria de parámetros *serializables*
que permitan reconstruir el objeto, en otros casos simplemente no se puede
(como objetos que son sesiones de conexión a bases de datos).

En esta ocasión trabajaremos la *primera etapa* de la construcción
de un sistema que permita enviar un *mensaje entre dos computadores*.
Consistirá en tomar un texto (string), convertirlo a bytes y seccionarlo.

Utilizaremos el concepto de *función generadora*.  

* Asserts

La estructura de control *assert* es sencilla de utilizar y permite controlar
el flujo del código frente a una operación lógica.

Por ejemplo, si defino una función cuya entrada requiera de una palabra mayor
a un largo 6, puedo hacer lo siguiente:

#+BEGIN_SRC python 
def user_login(name):
    assert len(name)>=6, "Debe tener un largo mayor a 6 caracteres"
    return name.lower()

name= "Hola"
user_login(name)

name2="Superduper"
user_login(name2)
#+END_SRC

Entonces, el funcionamiento de *assert* verifica que se cumpla algo, entonces permite
ejecutar el siguiente código, de otro modo entrega un mensaje de error y no ejecuta el 
código.

Es recomendable también revisar la [[http://librosweb.es/libro/algoritmos_python/capitulo_12/validaciones.html][Guía de Validaciones]] que revisa distintos casos
de uso de *assert*.

* Excepciones

Una herramienta adicional que existe para controlar posible fallos en la 
ejecución de un trozo de código es la estructura de *control de excepciones*.

Se utiliza la tupla de palabras *(try, except)* que permiten registrar desde
cualquier excepción o bien, granularmente, excepciones definidas y que se 
puedan reconocer.

En general, las distintas bibliotecas disponibles proveen sus propias
clases de *Excepciones*, esto nos permite ser muy granulares a la hora
de seleccionar un tipo de error que se gatille.

Una vez que sucede un error la ejecución del código pasa al contexto
que se abre con la estructura de control *except*, lo que incluye
cualquier código disponible en esa sección y la ejecución del *raise Exception*

Una guía recomendada para este tema la puedes encontrar en [[http://librosweb.es/libro/algoritmos_python/capitulo_12/excepciones.html][Guía Excepciones]]

** Ejemplo 

Es un ejemplo sencillo que muestra un procedimiento en que
se implementa un par *(try, except)* en el código.

*** Programa que solicita un número y es el divisor de otro.

Aquí pueden ocurrir dos excepciones: no se entrega un número o bien
se entrega un valor 0.

Tenemos un programa que solicita un número y lo divide a 100.

Es suficiente, opera bien si y solo si le entregamos números. ¿Probemos?

#+BEGIN_SRC python
# numerador
arriba = 100
# divisor
abajo = 1
while number != "X":
	abajo = input("Dame un número o termina con X")
	division = arriba / int(abajo)
	print("Division correcta")
	if abajo == "X":
		break	
#+END_SRC

No funciona tan bien, debemos también controlar cuando entregamos X.
Reordenando el código, tenemos lo siguiente:

#+BEGIN_SRC python
# numerador
arriba = 100
# denominador
abajo = 1
while abajo != "X":
	abajo = input("Dame un número o termina con X: ")
	if abajo == "X":
		break
	else:
		division = arriba / int(abajo)
		print("Division correcta %f" %division)
#+END_SRC

Realizamos distintas pruebas y si entregamos un 0 nos
retorna el siguiente error, deteniendo la ejecución.

#+BEGIN_SRC bash 
Traceback (most recent call last):
  File "<stdin>", line 6, in <module>
ZeroDivisionError: division by zero
#+END_SRC

Es hora de colocar un (try, except) al código para 
controlar los errores (división por cero) y describir
con mayor precisión que está pasando.

#+BEGIN_SRC python
# numerador
arriba = 100
# denominador
abajo = 1
while abajo != "X":
	abajo = input("Dame un número o termina con X: ")
	if abajo == "X":
		break
	else:
		try:
			division = arriba / int(abajo)
			print("Division correcta %f" %division)
		except ZeroDivisionError as ze:
			print("Hubo un error de división por 0, no debes entregar 0 como divisor")
			raise ze
#+END_SRC

La inclusión de *raise* permite levantar el error a través de los múltiples niveles
sobre los cuales se pueda ejecutar un código.

Ahora bien, si deseamos que el código se continúe ejecutando, pero registrando el error
podemos incluir la orden *continue*

#+BEGIN_SRC python
# numerador
arriba = 100
# denominador
abajo = 1
while abajo != "X":
	abajo = input("Dame un número o termina con X: ")
	if abajo == "X":
		break
	else:
		try:
			division = arriba / int(abajo)
			print("Division correcta %f" %division)
		except ZeroDivisionError as ze:
			print("Hubo un error de división por 0, no debes entregar 0 como divisor")
			continue
#+END_SRC

Por último, para solo permitir la operación de la división para números, podemos
usar la llamada del método *isdigit()* que verifica que un string sea numérico.

#+BEGIN_SRC python
# numerador
arriba = 100
# denominador
abajo = 1
while abajo != "X":
	abajo = input("Dame un número o termina con X: ")
	if abajo == "X":
		break
	elif abajo.isdigit():
		try:
			division = arriba / int(abajo)
			print("Division correcta %f" %division)
		except ZeroDivisionError as ze:
			print("Hubo un error de división por 0, no debes entregar 0 como divisor")
			continue
#+END_SRC


* Creación de un módulo python

En todo el proceso de desarrollo de software, la creación de un módulo nos permite llegar a un paso final
de publicación del código de manera tal que este sea instalable y utilizable como una llamada de biblioteca.

Básicamente, para crear un módulo se necesita:

- Archivos *.py* con variables, funciones y clases
- Una carpeta en que se almacenen estos archivos, con el nombre del módulo.
- Definir el proyecto y usar git para controlar versiones (de aquí en adelante)
- Utilizar la biblioteca *setuptools* para crear el instalador del módulo
- Hacer un set de pruebas unitarias (ideal)

** Ejercicio

Vamos a empaquetar nuestro primer módulo.

Tomamos las funciones que hemos escrito:

- Imprimir en color 
- String random de tamaño n
- Operaciones de geometría
- pygrep y pysed que realizamos de tarea

Creamos las siguientes carpetas:

- Herramientas
- Dentro de la primera "Herramientas" otra "herramientas"

Creamos el siguiente archivo:

- setup.py dentro de "Herramientas

Inicializamos proyecto git y lo publicamos en la plataforma

Lo probamos en otro computador.

#+BEGIN_SRC python

from setuptools import setup

setup(name='herramientas',
      version='0.1',
      description='Herramientas de python',
      url='http://gitlab.com/pineiden/herramientas',
      author='David Pineda Osorio',
      author_email='dpineda@uchile.cl',
      license='MIT',
      packages=['herramietnas'],
      zip_safe=False)

#+END_SRC

Así como también extraemos los requerimientos de nuestro módulo:

#+BEGIN_SRC bash
pip freeze > requerimientos.txt
#+END_SRC











#+END_SRC











