from ejemplo1 import Martillo

# Se usa el método de clase, sin crear una instancia

print("Las densidades de los materiales de un martillo son:")
print(Martillo.get_densidades())

Martillo.add_densidad('plástico', 30)

print("Las densidades de los materiales de un martillo son:")
print(Martillo.get_densidades())


# Se crea la instancia

m=Martillo("madera",20,"acero")
print("*"*30)
print(repr(m))
print(str(m))

# Se llaman distintos métodos

print("El peso del martillo es %f" %m.get_peso)

print("La cabeza es de %s" %m.get_cabeza)
print("El mango es de  %s y largo %s" %m.get_mango)


print("="*30)
print("Errores provocados:")
print("="*30)

m2=Martillo("plástico",20,"acero")
print(m2)
m3=Martillo("madera",60,"acero")
m4=Martillo("madera",20,"oro")

