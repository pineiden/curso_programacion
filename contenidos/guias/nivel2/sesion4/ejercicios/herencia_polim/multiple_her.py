class Cero:
    def __init__(self):
        print("Cero")
	
class Primera:
    def __init__(self):
        super(Primera, self).__init__()
        print("Primera")

class Segunda(Cero):
    def __init__(self):
        super(Segunda, self).__init__()
        print("Segunda")

class Tercera(Primera, Segunda):
    def __init__(self):
        super().__init__()
        print("Tercera")
        
# Ver Blog de Guido
# http://python-history.blogspot.com/2010/06/method-resolution-order.html        
        

