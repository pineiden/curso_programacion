class Mueble:
	def __init__(self, *args, **kwargs):				
				
		tipo=kwargs.get("tipo","mesa") 
		material=kwargs.get("material","madera") 
		origen=kwargs.get('origen',"Chile") 
		empresa=kwargs.get('empresa',"Retromadero") 
		codigo_id=kwargs.get('codigo_id',"ABC")
						
		self.tipo = tipo
		self.material = material
		self.origen = origen
		self.empresa = empresa
		self.codigo = codigo_id
		
class Mesa(Mueble):
	def __init__(self, *args, **kwargs):
		
		altura = kwargs.get('altura', .8)
		material = kwargs.get("material","Pino") 
		origen = kwargs.get("origen","Chile") 
		empresa=kwargs.get("empresa", None)
		codigo=kwargs.get("codigo","3M3") 
		tipo=kwargs.get("tipo","Comedor")
		patas=kwargs.get("patas",4)
		
		if empresa:
			data = dict(tipo="mesa", 
					material=material, 
					origen=origen, 
					empresa=empresa, 
					codigo_id=codigo)
		else:
			data = dict(tipo="mesa", 
					material=material, 
					origen=origen, 
					codigo_id=codigo)
		#llamamos init de Mueble
		super().__init__(*args, **data)
		self.altura = altura
		self.tipo = tipo
		self.patas=patas
		
class Silla(Mueble):
	def __init__(self, *args, **kwargs):
		
		altura = kwargs.get('altura', .6)
		material = kwargs.get("material","Pino") 
		origen = kwargs.get("origen","Chile") 
		empresa = kwargs.get("empresa", None)
		codigo = kwargs.get("codigo","3M3") 
		tipo = kwargs.get("tipo","Comedor")
		patas = kwargs.get("patas",4)
		
		data = object
		
		if empresa:
			data = dict(tipo="silla", 
					material=material, 
					origen=origen, 
					empresa=empresa, 
					codigo_id=codigo)
		else:
			data = dict(tipo="silla", 
					material=material, 
					origen=origen, 
					codigo_id=codigo)
		#llamamos init de Mueble	
		super().__init__(*args, **data)
		self.altura = altura
		self.tipo = tipo
		self.patas = patas
				
	
if __name__ == "__main__":
	
	#mesa
	data_mesa =dict(
			material="Roble",
			empresa="El buen mueble",
			origen="Wallmapu",
			altura=.8,
			patas=3,
			codigo="AM-01")
	
	mi_mesa = Mesa(**data_mesa)
	
	#silla
	
	data_silla = dict(altura=.6,codigo="AS-02")
	
	mi_silla = Silla(**data_silla)
	
	print("Origen Mesa %s, Silla %s" %(mi_mesa.origen, mi_silla.origen))
	print("Fabricante Mesa %s, Silla %s" %(mi_mesa.empresa, mi_silla.empresa))
	print("Código Mesa %s, Silla %s" %(mi_mesa.codigo, mi_silla.codigo))
	
