import sys
from termcolor import colored, cprint

class Normal:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		print(self.signo*self.cantidad)
		print(mensaje)
		print(self.signo*self.cantidad)
	
class Rojo:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'red')
		cprint(mensaje, 'red')
		cprint(self.signo*self.cantidad, 'red')
		
class Azul:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'blue')
		cprint(mensaje, 'blue')
		cprint(self.signo*self.cantidad, 'blue')
		
class Verde:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'green')
		cprint(mensaje, 'green')
		cprint(self.signo*self.cantidad, 'green')
		


if __name__ == "__main__":
	rojo = Rojo("=",20)
	azul = Azul("#",20)
	verde = Verde("&",20)
	default = Normal("°",30)
	colores = dict(rojo=rojo,verde=verde,azul=azul)
	while True:
		color = input("Selecciona Color {rojo, verde, azul}\n")
		mensaje = input("Ingresa un mensaje, termina con EOF\n")
		if mensaje == "EOF":
			break
		else:
			objeto = colores.get(color, default)
			objeto.imprimir(mensaje)
			
	rojo.imprimir("Muchas Gracias")
