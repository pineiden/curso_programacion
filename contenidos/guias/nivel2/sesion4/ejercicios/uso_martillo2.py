from ejemplo1 import Martillo

m1=Martillo("madera",20,"acero")
m2=Martillo("aluminio",25,"acero")

print("M2")
print(m2)
# equivalencia
# __eq__
print("Equivalencia")
print(m1 == m2)

# son diferentes?
# __ne__

print("Diferencias")
print(m1 != m2)

# menor estricto
# __lt__
print("Menor estricto")
print(m1 < m2)

# mayor estricto
# __gt__

print("Mayor estricto")
print(m1 > m2)

# menor o igual

print("Menor o igual")
print(m1 <= m2)

# mayor o igual

print("Mayor o igual")
print(m1 >= m2)

# Suma

print("Suma")
print(m1+m2)

# Resta

print("Resta")
print(m1-m2)
