class CarToy:
	'''EstableciendoComponentes Básicos'''
	color = None
	material = None
	size = None
	price = None
	model = None
	
	def __init__(self, color, material, size):
		self.color = color
		self.material = material
		self.size = size
		
	def retcolor(self):
		return self.color
	
	@property 
	def get_material(self):
		return self.material
	
	def __eq__(self, anothercar):
		a = self.model == anothercar.model
		b = self.color == anothercar.color
		return a and b
		
	
