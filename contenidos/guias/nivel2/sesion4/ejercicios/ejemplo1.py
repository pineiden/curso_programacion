import math
import simplejson as json

class Martillo:
    """
    Nos permite crear objetos martillo
    """
    densidades = {
        'madera': 1,
        'acero': 50,
        'aluminio':15,
        'goma': 20,
    }
    mango = "madera"
    largo = 25
    cabeza = "acero"
    def __init__(self, mango, largo, cabeza):
        """
        Es el método de inicialización
        """
        assert mango in self.densidades, "El material escogido para el mango no es valido"
        self.mango = mango

        assert largo>=20 and largo<=40, "El largo debe ser entre 20 y 40cm"
        self.largo = largo
        assert cabeza in self.densidades, "El material para la cabeza no es válido"
        self.cabeza = cabeza
        self.peso = self.calcula_peso()

    def __str__(self):
        """
        Retorna la representación en string
        del martillo
        """
        return "Mango de %s con cabeza de %s, con peso %s gr" %(self.mango,
                                             self.cabeza,
                                             self.peso)


    def __repr__(self):
        """
        Retorna la representación en string
        del martillo
        """
        result = dict(zip(("mango","cabeza","largo","peso"),(self.mango, self.cabeza, self.largo, self.peso)))
        return json.dumps(result)

    def calcula_peso(self):
        # gramos por cm cúbico
        # en cm cúbicos
        volumen_cabeza = 30
        peso_cabeza = volumen_cabeza*self.densidades.get(self.cabeza)
        # en cm
        radio = 2
        # en cm cúbicos
        volumen_mango = self.largo*math.pi*radio**2
        peso_mango = volumen_mango*self.densidades.get(self.mango)
        return peso_cabeza+peso_mango

    @property
    def get_peso(self):
        return self.peso

    @property
    def get_cabeza(self):
        return self.cabeza

    @property
    def get_mango(self):
        return (self.mango, self.largo)

    @classmethod
    def get_densidades(cls):
        return cls.densidades

    @classmethod
    def add_densidad(cls, material, densidad):
        # verificar que densidad sea número
        # verificar que material sea string
        types = [int, float]
        assert type(material)==str, "El material debe ser un nombre"
        assert type(densidad) in types,"La densidad debe ser un valor numérico"
        cls.densidades.update({material:densidad})
 

    def __eq__(self, otro_martillo):
        # activa ==
        misma_cabeza = self.cabeza == otro_martillo.get_cabeza
        mismo_mango = self.mango == otro_martillo.get_mango
        mismo_largo = self.largo == otro_martillo.largo
        return misma_cabeza and mismo_mango and mismo_largo

    def __ne__(self, otro_martillo):
        # activa not ==
        misma_cabeza = self.cabeza == otro_martillo.get_cabeza
        mismo_mango = self.mango == otro_martillo.get_mango
        mismo_largo = self.largo == otro_martillo.largo
        return not (misma_cabeza and mismo_mango and mismo_largo)

    def __lt__(self, otro_martillo):
        # activa <
        return self.peso < otro_martillo.get_peso

    def __gt__(self, otro_martillo):
        # activa >
        return self.peso > otro_martillo.get_peso

    def __le__(self, otro_martillo):
        # activa <
        return self.peso < otro_martillo.get_peso

    def __ge__(self, otro_martillo):
        # activa >
        return self.peso > otro_martillo.get_peso

    def __add__(self, otro_martillo):
        return self.peso + otro_martillo.get_peso

    def __sub__(self, otro_martillo):
        return self.peso - otro_martillo.get_peso

    def __hash__(self):
        return hash((self.mango, self.largo, self.cabeza))
