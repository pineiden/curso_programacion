* Polimorfismo

Es una de las características que se desprende de la /Programación Orientada a al
 Objeto/, en la cual se hace posible aumentar la flexibilidad de una operación o
 procedimiento que contemple el uso de diferentes objetos según el caso, pero la
 funcionalidad invocada sea la misma.

Es decir, tenemos las clases /Martillo/ y /Hoz/, ambas con el método
*calcular_peso*, si ocupamos indistintamente instancias de ambas clases y usamos
ese método, estamos frente a esta característica.  

Desde otro punto de vista, un conjunto de objetos que son capaces de responder
al mismo mensajes presentan *polimorfismo* frente al mensaje o acción.

En el caso de *Python* es muy útil definir los métodos integrando de listas
desempaquetadas **args* y diccionarios desempaquetados ***kwargs*.

#+BEGIN_SRC python
import sys
from termcolor import colored, cprint

class Normal:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		print(self.signo*self.cantidad)
		print(mensaje)
		print(self.signo*self.cantidad)
	
class Rojo:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'red')
		cprint(mensaje, 'red')
		cprint(self.signo*self.cantidad, 'red')
		
class Azul:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'blue')
		cprint(mensaje, 'blue')
		cprint(self.signo*self.cantidad, 'blue')
		
class Verde:
	def __init__(self, signo, cantidad):
		self.signo = signo
		self.cantidad = cantidad
		
	def imprimir(self, mensaje):
		cprint(self.signo*self.cantidad, 'green')
		cprint(mensaje, 'green')
		cprint(self.signo*self.cantidad, 'green')
#+END_SRC

Una vez implementadas las clases, las usamos en un pogramita sencillo.

#+BEGIN_SRC python
if __name__ == "__main__":
	rojo = Rojo("=",20)
	azul = Azul("#",20)
	verde = Verde("&",20)
	default = Normal("°",30)
	colores = dict(rojo=rojo,verde=verde,azul=azul)
	while True:
		color = input("Selecciona Color {rojo, verde, azul}\n")
		mensaje = input("Ingresa un mensaje, termina con EOF\n")
		if mensaje == "EOF":
			break
		else:
			objeto = colores.get(color, default)
			objeto.imprimir(mensaje)
	rojo.imprimir("Muchas Gracias")
#+END_SRC

* Herencia de Clase

Con el fin de lograr un gran factor de *reutilización* y *extensibilidad* esta
técnica permite abstraer elementos y funciones en común entre diferentes clases,
de manera tal que se crea una jerarquía de tipos relacionadas mediante herencia.

Esta jerarquía debe partir de lo más general y debe llegar a la definición 
de tipos específicos o casos bien definidos. De esta manera es posible
especializar el uso de las clases creadas.

** Un Caso Sencillo

Administramos la venta de muebles, tenemos a disposición *mesas* y *sillas*, nos
piden que administremos la información de cada uno de los objetos. Vemos que hay
elementos en común, por lo que decidimos escribir una *clase padre* llamada
*Mueble* que  contenga los elementos en común de interés.

#+BEGIN_SRC python
class Mueble:
	def __init__(self, *args, **kwargs):				
				
		tipo=kwargs.get("tipo","mesa") 
		material=kwargs.get("material","madera") 
		origen=kwargs.get('origen',"Chile") 
		empresa=kwargs.get('empresa',"Retromadero") 
		codigo_id=kwargs.get('codigo_id',"ABC")
						
		self.tipo = tipo
		self.material = material
		self.origen = origen
		self.empresa = empresa
		self.codigo = codigo_id
#+END_SRC

Luego, debido a que cada tipo de mueble tiene características específicas a su
diseño, decidimos escribir las clases *Mesa* y *Silla* para que podamos
gestionar la información en particular de ellas.

Importante a tener en cuenta el uso de *super()* que nos permite ejecutar
el método de la clase padre, de la que heredamos.

#+BEGIN_SRC python		
class Mesa(Mueble):
	def __init__(self, *args, **kwargs):
		
		altura = kwargs.get('altura', .8)
		material = kwargs.get("material","Pino") 
		origen = kwargs.get("origen","Chile") 
		empresa=kwargs.get("empresa", None)
		codigo=kwargs.get("codigo","3M3") 
		tipo=kwargs.get("tipo","Comedor")
		patas=kwargs.get("patas",4)
		
		if empresa:
			data = dict(tipo="mesa", 
					material=material, 
					origen=origen, 
					empresa=empresa, 
					codigo_id=codigo)
		else:
			data = dict(tipo="mesa", 
					material=material, 
					origen=origen, 
					codigo_id=codigo)
		# llamamos init de Mueble
		super().__init__(*args, **data)
		self.altura = altura
		self.tipo = tipo
		self.patas=patas
		
class Silla(Mueble):
	def __init__(self, *args, **kwargs):
		
		altura = kwargs.get('altura', .6)
		material = kwargs.get("material","Pino") 
		origen = kwargs.get("origen","Chile") 
		empresa = kwargs.get("empresa", None)
		codigo = kwargs.get("codigo","3M3") 
		tipo = kwargs.get("tipo","Comedor")
		patas = kwargs.get("patas",4)
		
		data = object
		
		if empresa:
			data = dict(tipo="silla", 
					material=material, 
					origen=origen, 
					empresa=empresa, 
					codigo_id=codigo)
		else:
			data = dict(tipo="silla", 
					material=material, 
					origen=origen, 
					codigo_id=codigo)
			
		super().__init__(*args, **data)
		self.altura = altura
		self.tipo = tipo
		self.patas = patas
#+END_SRC

Por último, creamos un script para probar la creación de muebles a manera de
verificar el correcto funcionamiento.

#+BEGIN_SRC python
if __name__ == "__main__":
	
	#mesa
	data_mesa =dict(
			material="Roble",
			empresa="El buen mueble",
			origen="Wallmapu",
			altura=.8,
			patas=3,
			codigo="AM-01")
	
	mi_mesa = Mesa(**data_mesa)
	
	#silla
	
	data_silla = dict(altura=.6,codigo="AS-02")
	
	mi_silla = Silla(**data_silla)
	
	print("Origen Mesa %s, Silla %s" %(mi_mesa.origen, mi_silla.origen))
	print("Fabricante Mesa %s, Silla %s" %(mi_mesa.empresa, mi_silla.empresa))
	print("Código Mesa %s, Silla %s" %(mi_mesa.codigo, mi_silla.codigo))

#+END_SRC

** Herencia Múltiple

Es una característica especial que permite heredar atributos y métodos de un
conjunto de clases diferentes.

A tener en cuenta:
- El orden en que se realiza la búsqueda sobre las clases padres para obtener un
  atributo o bien un método se llama el *Método de Resolución de Orden*  [MRO], 
- El método MRO de Python es mediante la búsqueda de último en una lista de
  izquierda a derecha.

El siguiente ejemplo debería ejecutar {Cero, Segunda, Primera, Tercera}

#+BEGIN_SRC python
class Cero:
    def __init__(self):
        super(Cero, self).__init__()
        print("Cero")
	
class Primera:
    def __init__(self):
        super(Primera, self).__init__()
        print("Primera")

class Segunda(Cero):
    def __init__(self):
        super(Segunda, self).__init__()
        print("Segunda")

class Tercera(Primera, Segunda):
    def __init__(self):
        super(Tercera, self).__init__()
        print("Tercera")
        
# Ver Blog de Guido
# http://python-history.blogspot.com/2010/06/method-resolution-order.html        
        
        
if __name__ == "__main__":
	t = Tercera()
#+END_SRC


** Tarea 

Poner en práctica el concepto de Herencia, utilizando su módulo de herramientas
que contiene *Martillo* y *Hoz*. Crea una clase padre que se defina como
*Herramienta* que contenga aquellos atributos y métodos comunes a nuestras
primeras dos clases.

Haz *commit* de las nuevas modificaciones y súbelas a tu repositorio *gitlab*.

Comparte en el chat el *link*.

* Ejercicio Grupal

Este ejercicio consiste en poner en práctica los conceptos de herencia mediante
la implementación de una jerarquía de clases que representen la *taxonomía del
reino animales*.

El orden o estructura debe llevar lo siguiente:

$Dominio > Reino > Filo o división > Clase > Orden > familia > Género > Especie$

Dada la imagen, nos ocuparemos de una rama reducida del árbol filogenético.

[[file:./filofgenetica.png]]

- Dominio :: Es la rama más alta de clasificación, se compone de tres grandes
             ramas {Aqueas, Bacterias, Eucariontes}}
- Reino :: Clasifica, dentro de la rama /Eucariontes/, al tipo de ser vivo
           {Animal, Fungi, Protista, Plantae}
- Filo :: Es una forma de agrupar según características generales en común, hay
          unas 40 filos 
- Clase :: Usa sufijos o prefijos para seres del reino animal, Las subclases
           llevan mombres de {Mammalia, Insecta, Cephalopoda, Becillo,
           Mollicutes}
- Orden :: Dependiendo del organismo, se emplean categorías intermedias. Lo
           mismo entre orden y familia. Determina características comunes entre
           seres vivos de una clase. 
- Familia :: Es una de las categorías más importantes en la clasificación,
             agrupa características similares dentro de algún orden.
- Género :: Un grupo de organismos divididos en una o varias especias,
            relacionados a través de la evolución.
- Especie :: agrupa a un conjunto de animales de características similares, como
             compartir un hábitat, reproducción entre ellos manteniendo mismas
             características.

** Ideas

- Tomar algunos animales específicos y describirlos.

- Algunos que tengan en común especie, otros género y así

- Implementar métodos de clase para definir la ruta de un animal.

** Referencias

- [[https://es.calameo.com/read/000694236fd9b6784590f][Taxonomía]]

- [[http://tolweb.org/tree/][Tree of Life Project]]

- [[https://en.wikipedia.org/wiki/Tree_of_life_(biology)][Wiki Tree of Life]]

-[[http://www.onezoom.org/life.html][ Treeof life Map]]

** Tarea

Hacer una lista de 100 animales diferentes y mapear sus rasgos utilizando
las clases construidas.
