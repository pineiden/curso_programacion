import socket
import pickle


bits = 3
buffersize = 2**8

timeout = 60

familia = socket.AF_NET
tipo = socket.SOCK_STREAM

cliente = socket.socket(familia, tipo)

direccion = (host, port)

# me conecto a una dirección
# que sabemos es un socket afín a lo que 
# necesito comunicar

conn = cliente.connect(direccion)

print(conn)

while True:
	mensaje = input("Envía un mensaje:\n")
	pm = pickle.dumps(mensaje)
	cliente.sendall(pm)
	rm = cliente.recv(buffersize)
	msj = pickle.loads(rm)
	print("Hemos recibido en cliente %s"%msj)
