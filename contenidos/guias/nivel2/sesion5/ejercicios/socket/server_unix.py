import socket
import pickle
import time

bits = 3
buffersize = 2**8

timeout = 60

familia = socket.AF_UNIX
tipo = socket.SOCK_STREAM

server = socket.socket(familia, tipo)

ruta = '/tmp/server.sock'

direccion = ruta

# ligar socket server a la direccion
server.bind(direccion)

# definir timeout
# en segundos
server.settimeout(timeout)

# escuchar por conexiones de nuevos clientes
server.listen()

# acepta conexión
(conn, address) = server.accept()

with conn:
	print('Connectado por', address)
	while True:
		data = conn.recv(buffersize)
		print(data)
		time.sleep(1)
		if data:
			msj = pickle.loads(data)
			print(msj)
			#if not data: break
			conn.sendall(pickle.dumps("Recibido %s" %msj))	
