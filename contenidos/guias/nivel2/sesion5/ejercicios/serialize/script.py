from objetos import Marraqueta

#Usando PICKLE

import pickle

def trocear(objeto, tamaño):
	u = pickle.dumps(objeto)
	d = tamaño
	result=[u[i*(d):d*(i+1)] for i in range(int(math.ceil(len(u)/d)))]
	return result
	
def reconstruir(mensaje_list):
	msj = b''.join(mensaje_list)
	m = pickle.loads(msj)
	return m

m=Marraqueta(1,.05,.5,3)

print(repr(m))

# Se serializa objeto directamente
mensaje_list = trocear(m, 5)

[print(trozo) for trozo in mensaje_list]

# Se deserializa

dm = reconstruir(mensaje_list)

print(dm)





