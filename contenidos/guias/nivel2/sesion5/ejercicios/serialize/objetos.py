import json

class Marraqueta:
	def __init__(self, harina, levadura, agua, tiempo_batido):
		self.harina = harina
		self.levadura = levadura
		self.agua = agua
		self.tiempo_batido = tiempo_batido
		
	def __repr__(self):
		return json.dumps(self.__dict__)
		 
#m=Marraqueta(1,.05,.5,3)
