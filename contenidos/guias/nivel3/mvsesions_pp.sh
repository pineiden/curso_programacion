n0=4
nf=12

for i in $(seq $n0 $nf); do
    command="mkdir sesion${i}"
    echo $command
    echo $command|bash
done

n0=5
for i in $(seq $nf -1 $n0); do
    pre=$(echo $i-1|bc)
    command_a="mv sesion${pre} sesion${i}"
    echo $command_a|bash
done
