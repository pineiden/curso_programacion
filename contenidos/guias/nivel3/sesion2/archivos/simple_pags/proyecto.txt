El Proyecto
===========

El proyecto consiste en un archivo repositorio con toda la información que
se pueda recopilar acerca de las *zonas de sacrificio*.

Está (estará) concebido para ser dinámico y que pueda generar un relato
visual de los efectos devastadores que tiene la actividad industrial.

También considera los principales actores que ejecutan o se ven afectados
en estas zonas.

Concebimos a un *neo juglar* que tenga la importante misión de recabar información y 
registrarla en la plataforma.

La información debe ser clara, precisa y con niveles de profundidad. Dependiendo 
del usuario hay distintos niveles de acceso.

Estamos en desarrollo
