(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted")
   (LaTeX-add-labels
    "sec:org1303a74"
    "sec:org81f75a9"
    "sec:orgb7eed1e"
    "sec:org1e866c2"
    "sec:org6968dff"
    "sec:orgabbf224"
    "fig:orgc61de0c"
    "fig:org0c8a5ab"
    "fig:orgc4a1978"
    "fig:org34540ff"
    "sec:org2e50976"
    "tab:org2c323d3"
    "sec:org980edd9"
    "sec:org1f077ed"
    "sec:orga243917"
    "sec:org4a7e4e1"
    "sec:org4e8c6bd"
    "sec:org4e83d57"
    "sec:orgcb4cf39"
    "sec:orgcda793f"
    "sec:orgcbfe393"
    "sec:org733f029"
    "sec:org119b1b4"
    "sec:orgd5b81ce"
    "fig:org638afa4"
    "sec:orgfc957d1"
    "fig:orgedd09e7"
    "sec:org4b5055a"
    "fig:org852d7c1"
    "sec:orgd68ec8a"
    "sec:orgfb6f2de"
    "sec:orgee55690"
    "fig:org8c2bd56"
    "sec:org709ea0d"
    "sec:org0cee314"
    "sec:orgc9707c4"
    "sec:orgbca409c"
    "sec:org0126a0f"
    "sec:orgd3680a3"
    "sec:org4facb13"
    "sec:org743021e"
    "sec:orgd66456f"
    "sec:org799c505"
    "sec:org8d4a820"
    "sec:org8995d87"))
 :latex)

