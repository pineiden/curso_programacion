(TeX-add-style-hook
 "guia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "tabularx")
   (LaTeX-add-labels
    "sec:org473efd9"
    "sec:org2d1437f"
    "sec:org6d3f02f"
    "sec:org891b742"
    "sec:org0a86679"
    "sec:orgb8b2ece"
    "sec:org7b63b5d"
    "sec:orgde9318c"
    "sec:org7308193"
    "sec:org4a36094"
    "sec:orgc230ca7"
    "sec:org8be0bc5"
    "sec:org80b2890"
    "sec:orgfd6e38b"
    "sec:org915378b"
    "sec:org9d178ca"
    "sec:orgbd02693"
    "sec:org1a59b35"
    "sec:org8766d77"
    "sec:orgcddcf95"
    "sec:orgab4c6b7"
    "sec:orgb053f71"
    "sec:orgedd8304"
    "sec:orgfbe15ee"
    "sec:orgbd9e0c9"
    "sec:orgbf50763"
    "sec:orgfba354e"
    "sec:orgbb1ae92"
    "sec:org8653a95"
    "sec:org5938733"
    "sec:orgcbfd238"
    "sec:org824ad22"
    "sec:org44f2c3a"
    "sec:org5e95fc5"
    "sec:org339fc60"
    "sec:orgc8110bf"
    "sec:org6cfd9e0"
    "sec:orgc77b7fe"
    "sec:org0b3f197"
    "sec:orgcaf6f32"
    "sec:org8453da0"
    "sec:org1ebf354"
    "sec:orgec89bba"
    "sec:org5e34456"
    "sec:org5c1bf01"
    "sec:org98d9421"
    "sec:org0248be4"
    "sec:org3ed38c2"
    "sec:org9148f92"
    "sec:org1999634"
    "sec:orgff4b340"
    "sec:org2f711f2"
    "sec:org8e9a754"
    "sec:org116c0b1"
    "sec:org42a1f98"
    "sec:org7cae6ef"
    "sec:org69d403e"
    "sec:org3693609"
    "sec:orgea4229a"
    "sec:orgd1d6ee3"
    "sec:orgd1cc02b"
    "sec:orgb1210b8"
    "sec:org96a96f5"
    "sec:org7fd846f"
    "sec:orgb6da559"
    "sec:orgc5dcfdd"))
 :latex)

